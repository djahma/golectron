package golectron

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func (g *Golectron) setRoutes() {
	g.router.GET("/electronReady", g.electronReady)
	g.router.POST("/event/menuitem/:id/:eventName", g.menuitemEvent)
	g.router.POST("/event/browserwindow/:id/:eventName", g.browserwindowEvent)
	g.router.POST("/event/app/:eventName", g.appEvent)
	g.router.POST("/event/globalShortcut", g.globalShortcutEvent)
	g.router.POST("/event/WebContents/:id/:eventName", g.webContentsEvent)
	g.router.POST("/event/session/:id/:eventName", g.sessionEvent)
	g.router.POST("/event/cookies/:id/:eventName", g.cookiesEvent)
	g.router.POST("/event/webRequest/:id/:eventName", g.webRequestEvent)
}

//------------------------
// List of Routes
//---------------------------
func (g *Golectron) electronReady(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("electron is ready", r.FormValue("listeningPort"))
	if r.FormValue("listeningPort") != "" {
		g.ElectronServerPort = r.FormValue("listeningPort")
		g.golecReadyCh <- 1
	}
}

func (g *Golectron) menuitemEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in menuitem, id=", p.ByName("id"), "event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	mi := g.GetMenuItemByID(i)
	if mi == nil {
		fmt.Println("ERROR in menuitemEvent, couldn't find menuitem id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		mi.Emit(p.ByName("eventName"), rbody)
		w.WriteHeader(200)
	}
}

func (g *Golectron) browserwindowEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in browserwindow, id=", p.ByName("id"), "event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	win := g.GetWindowByID(i)
	if win == nil {
		fmt.Println("ERROR in browserwindowEvent, couldn't find window id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		win.Emit(p.ByName("eventName"), rbody)
		w.WriteHeader(200)
	}
}

func (g *Golectron) appEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron app, event=", p.ByName("eventName"))
	rbody, _ := ioutil.ReadAll(r.Body)
	g.Emit(p.ByName("eventName"), rbody)
	w.WriteHeader(200)
}

func (g *Golectron) globalShortcutEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron globalShortcutEvent")
	data := struct {
		Accelerator string `json:"accelerator"`
	}{}
	rbody, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	fmt.Println(" globalShortcutEvent rbody", string(rbody))
	err := json.Unmarshal(rbody, &data)
	if err != nil {
		fmt.Println("ERROR Unmarshalling globalShortcutEvent:", err)
	}
	fmt.Println("globalShortcut is", string(rbody), data)

	globalShortcuts[data.Accelerator]()
	w.WriteHeader(200)
}

func (g *Golectron) webContentsEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron webContentsEvent, event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	wb := g.GetWebContentsByID(i)
	if wb == nil {
		fmt.Println("ERROR in webContentsEvent, couldn't find webcontent id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		wb.Emit(p.ByName("eventName"), rbody)
		w.WriteHeader(200)
	}
	w.WriteHeader(200)
}

func (g *Golectron) sessionEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron sessionEvent, event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	sess := g.GetSessionByID(i)
	if sess == nil {
		fmt.Println("ERROR in sessionEvent, couldn't find session id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		resp := sess.Emit(p.ByName("eventName"), rbody)
		w.Write([]byte(resp.([]byte)))
		w.WriteHeader(200)
	}
}
func (g *Golectron) cookiesEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron cookiesEvent, event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	sess := g.GetSessionByID(i)
	if sess == nil {
		fmt.Println("ERROR in cookiesEvent, couldn't find session id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		resp := sess.Cookies.Emit(p.ByName("eventName"), rbody)
		w.Write([]byte(resp.([]byte)))
		w.WriteHeader(200)
	}
}
func (g *Golectron) webRequestEvent(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	fmt.Println("Event received in golectron webRequestEvent, event=", p.ByName("eventName"))
	i, _ := strconv.Atoi(p.ByName("id"))
	sess := g.GetSessionByID(i)
	if sess == nil {
		fmt.Println("ERROR in webRequestEvent, couldn't find session id=", p.ByName("id"))
		w.WriteHeader(400)
	} else {
		rbody, _ := ioutil.ReadAll(r.Body)
		resp := sess.WebRequest.Emit(p.ByName("eventName"), rbody)
		w.Write([]byte(resp.([]byte)))
		w.WriteHeader(200)
	}
}
