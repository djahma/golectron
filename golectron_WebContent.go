package golectron

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// WebContents is an EventEmitter. It is responsible for rendering and controlling a web page and is a property of the BrowserWindow object
type WebContents struct {
	// Private
	elecClient *http.Client
	gelec      *Golectron
	eventMap   map[string]func(...interface{})
	//Private electron properties
	session Session
	// PUblic
	Id int `json:"id"` // The unique ID of this WebContents.
	// Session             Session      `json:"session"`             // DO NOT USE! Session used by this webContents. Use GetSession() instead to ensure syncing between Go and Electron
	HostWebContents     *WebContents `json:"hostWebContents"`     //A WebContents that might own this WebContents.
	DevToolsWebContents *WebContents `json:"devToolsWebContents"` //A WebContents of DevTools for this WebContents.
	// Debugger            Debugger     `json:"debugger"`            //A Debugger instance for this webContents.
}

//----------------------------------------------------------------------------
// Static Methods
//
//----------------------------------------------------------------------------

// WebContentsGetAllWebContents returns an array of all WebContents instances. This will contain web contents for all windows, webviews, opened devtools, and devtools extension background pages.
func (g *Golectron) WebContentsGetAllWebContents() []*WebContents {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/getAllWebContents", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call getAllWebContents function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		WebContentss []*WebContents `json:"webContents"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR WebContents failed to Unmarshal getAllWebContents:", err)
	}
	fmt.Println("WebContents getAllWebContents response=", resp.Status, string(r))
	for _, val := range data.WebContentss {
		g.addWebContents(val)
	}
	g.purgeWebContents(data.WebContentss)
	return g.webContents
}

// WebContentsGetFocusedWebContents returns the web contents that is focused in this application, otherwise returns null.
func (g *Golectron) WebContentsGetFocusedWebContents() *WebContents {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/getFocusedWebContents", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call getFocusedWebContents function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		WebContentss *WebContents `json:"webContents"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR WebContents failed to Unmarshal getFocusedWebContents:", err)
	}
	fmt.Println("WebContents getFocusedWebContents response=", resp.Status, string(r))
	g.addWebContents(data.WebContentss)
	g.WebContentsGetAllWebContents() //just to purge destroyed webcontents
	return g.GetWebContentsByID(data.WebContentss.Id)
}

//WebContentsFromId returns a WebContents instance with the given ID.
func (g *Golectron) WebContentsFromId(id int) *WebContents {
	data := struct {
		ID           int          `json:"id"`
		WebContentss *WebContents `json:"webContents"`
	}{ID: id}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.WebContentsFromId:", err)
	} else {
		fmt.Println("marshalling WebContents.WebContentsFromId worked:", string(jsData))
	}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/fromId", bytes.NewBuffer(jsData))
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call WebContentsFromId function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR WebContents failed to Unmarshal WebContentsFromId:", err)
	}
	fmt.Println("WebContents WebContentsFromId response=", resp.Status, string(r))
	g.addWebContents(data.WebContentss)
	g.WebContentsGetAllWebContents() //just to purge destroyed webContents
	return g.GetWebContentsByID(data.WebContentss.Id)
}

func (g *Golectron) addWebContents(wc *WebContents) *WebContents {
	// add wc if not already in g.webContents registry
	isThere := false
	for _, val := range g.webContents {
		if val.Id == wc.Id {
			isThere = true
			wc = val
			break
		}
	}
	wc.elecClient = g.elecClient
	wc.gelec = g
	if !isThere {
		g.webContents = append(g.webContents, wc)
	}
	return wc
}
func (g *Golectron) purgeWebContents(wcs []*WebContents) {
	toBeDeleted := []int{}
	for _, val := range g.webContents {
		isthere := false
		for ind := 0; ind < len(wcs); ind++ {
			if val.Id == wcs[ind].Id {
				isthere = true
				break
			}
		}
		if !isthere {
			toBeDeleted = append(toBeDeleted, val.Id)
		}
	}
	for i := len(toBeDeleted) - 1; i >= 0; i-- {
		g.RemoveWebContentsByID(toBeDeleted[i])
	}
}
func (g *Golectron) newBlankWebContents() *WebContents {
	wc := &WebContents{}
	wc.gelec = g
	wc.elecClient = g.elecClient
	wc.eventMap = make(map[string]func(...interface{}))
	return wc
}

//--------------------------------------------------------
// WebContents Instance Events
// Render and control the contents of a BrowserWindow instance.
//--------------------------------------------------------

/* WebContents has the following list of events:
did-finish-load
did-fail-load returns event Event,errorCode int,errorDescription string, validatedURL string, isMainFrame bool
did-frame-finish-load returns event Event, isMainFrame bool
did-start-loading
did-stop-loading
did-get-response-details returns event Event, status bool,newURL string, originalURL string, httpResponseCode int, requestMethod string,referrer string, headers Object, resourceType string
did-get-redirect-request returns event Event, oldURL string,newURL string, isMainFrame bool,httpResponseCode int, requestMethod string,referrer string, headers Object
dom-ready returns event Event
page-favicon-updated returns event Event,favicons string[] - Array of URLs
new-window returns event Event,url string,frameName string,disposition string, options Object, additionalFeatures Array
will-navigate returns event Event, url string
did-navigate returns event Event,url string
did-navigate-in-page returns event Event,url string, isMainFrame bool
crashed returns event Event, killed bool
plugin-crashed returns event Event,name string, version string
destroyed
devtools-opened
devtools-closed
devtools-focused
certificate-error returns event Event, url URL,error string,certificate {data string, issuerName string, subjectName string,serialNumber string, validStart int,validExpiry int, fingerprint string}, callback Function
select-client-certificate returns event Event, url URL,[]certificateList {data string, issuerName string, subjectName string,serialNumber string, validStart int,validExpiry int, fingerprint string}, callback Function
login returns event Event,request {method string,url URL,referrer URL}, authInfo{isProxy bool, scheme string,host string,port int,realm string}, callback Function
found-in-page returns event Event, result {requestId int, activeMatchOrdinal int, matches int, selectionArea Object}
media-started-playing
media-paused
did-change-theme-color
update-target-url returns event Event, url string
cursor-changed returns event Event,type string, image NativeImage, scale Float, size {width int,height int},hotspot{x int,y int}
context-menu returns event Event, params{x int,y int,linkURL string,linkText string,pageURL string,frameURL string,srcURL string, mediaType string, hasImageContents bool,isEditable bool,selectionText string, titleText string, misspelledWord string, frameCharset string, inputFieldType string, menuSourceType string,mediaFlags{inError bool, isPaused bool, isMuted bool, hasAudio bool, isLooping bool, isControlsVisible bool,canToggleControls bool, canRotate bool}, editFlags{canUndo bool, canRedo bool,canCut bool,canCopy bool,canPaste bool,canDelete bool,canSelectAll bool}}
select-bluetooth-device returns event Event, []devices{deviceName string, deviceId string},callback Function(deviceId string)
paint returns event Event,dirtyRect{x int,y int,width int,height int}, image NativeImage
*/

// On registers eventName event with fn func
func (web *WebContents) On(eventName string, fn func(...interface{})) {
	fmt.Println("Adding event", eventName, "to webcontent")
	req, err := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/listen4event/"+eventName, nil)
	if err != nil {
		fmt.Println("ERROR WebContents failed generate On request:", err)
	}
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR WebContents failed to Unmarshal Listen4Event error message:", err)
		}
		fmt.Println("ERROR WebContents couldn't listen to event", eventName, ":", data.Error)
	} else {
		web.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (web *WebContents) RemoveAllListeners(eventName string) {
	req, err := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/unlisten4event/"+eventName, nil)
	if err != nil {
		fmt.Println("ERROR WebContents failed generate RemoveAllListeners request:", err)
	}
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Unlisten4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR WebContents failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR WebContents couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(web.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (web *WebContents) Emit(eventName string, data ...interface{}) {
	fmt.Println("Emitting webcontent event", eventName, " eventmap=", web.eventMap)
	if val, ok := web.eventMap[eventName]; ok {
		val(data)
	} else {
		fmt.Println("ERROR: Received event in WebContents is out of sync with Electron. EventName=", eventName)
	}
}

// EventList returns an array of event names currently registered.
func (web *WebContents) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(web.eventMap))
	for key := range web.eventMap {
		evList = append(evList, key)
	}
	return evList
}

// Certificate holds the data passed by events about certificates; e.g.: certificate-error
type Certificate struct {
	Data         string `json:"data"`
	IssuerName   string `json:"IssuerName"`
	SubjectName  string `json:"subjectName"`
	SerialNumber string `json:"serialNumber"`
	ValidStart   int    `json:"validStart"`
	ValidExpiry  int    `json:"validExpiry"`
	Fingerprint  string `json:"fingerprint"`
}

// AuthInfo holds the data passed by 'login' event
type AuthInfo struct {
	IsProxy bool   `json:"isProxy"`
	Scheme  string `json:"scheme"`
	Host    string `json:"host"`
	Port    int    `json:"port"`
	Realm   string `json:"realm"`
}

// FoundInPageResult holds parameter from found-in-page event
type FoundInPageResult struct {
	RequestId          int  `json:"requestId"`
	ActiveMatchOrdinal int  `json:"activeMatchOrdinal"`
	Matches            int  `json:"matches"`
	SelectionArea      Rect `json:"selectionArea"`
}

// Rect represent a rectangle coordinates
type Rect struct {
	Width  int `json:"width"`
	Height int `json:"height"`
	X      int `json:"x"`
	Y      int `json:"y"`
}

type ContextMenuParam struct {
	Rect
	LinkURL          string     `json:"linkURL"`
	LinkText         string     `json:"linkText"`
	PageURL          string     `json:"pageURL"`
	FrameURL         string     `json:"frameURL"`
	SrcURL           string     `json:"srcURL"`
	MediaType        string     `json:"mediaType"`
	HasImageContents bool       `json:"hasImageContents"`
	IsEditable       bool       `json:"isEditable"`
	SelectionText    string     `json:"selectionText"`
	TitleText        string     `json:"titleText"`
	MisspelledWord   string     `json:"misspelledWord"`
	FrameCharset     string     `json:"frameCharset"`
	InputFieldType   string     `json:"inputFieldType"`
	MenuSourceType   string     `json:"menuSourceType"`
	MediaFlags       MediaFlags `json:"mediaFlags"`
	EditFlags        EditFlags  `json:"editFlags"`
}
type MediaFlags struct {
	InError           bool `json:"inError"`
	IsPaused          bool `json:"isPaused"`
	IsMuted           bool `json:"isMuted"`
	HasAudio          bool `json:"hasAudio"`
	IsLooping         bool `json:"isLooping"`
	IsControlsVisible bool `json:"isControlsVisible"`
	CanToggleControls bool `json:"canToggleControls"`
	CanRotate         bool `json:"canRotate"`
}
type EditFlags struct {
	CanUndo      bool `json:"canUndo"`
	CanRedo      bool `json:"canRedo"`
	CanCut       bool `json:"canCut"`
	CanCopy      bool `json:"canCopy"`
	CanPaste     bool `json:"canPaste"`
	CanDelete    bool `json:"canDelete"`
	CanSelectAll bool `json:"canSelectAll"`
}
type Device struct {
	DeviceName string `json:"deviceName"`
	DeviceId   string `json:"deviceId"`
}

//----------------------------------------------------------------------------
// WebContents Instance Methods
//
//----------------------------------------------------------------------------

// GetSession retrieves the session used by this webContents, ensuring Go and Electron sides remain in sync. DO NOT USE Session Field from webContents: I left it there because I don't know how to Unmarshal json without leaving it Public, but the "Id" field won't be set, and functions won't work.
func (web *WebContents) GetSession() *Session {
	data := struct {
		Session Session `json:"session"`
		Err     string  `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getSession", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetSession function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	fmt.Println("WebContents GetSession response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetSession:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetSession:", resp.StatusCode, data.Err)
		return nil
	}
	web.gelec.addSession(&data.Session)
	return &data.Session
}

// LoadURL loads the url in the window.
func (web *WebContents) LoadURL(url, httpReferrer, userAgent, extraHeaders string) {
	data := struct {
		Url          string `json:"url"`
		HttpReferrer string `json:"httpReferrer"`
		UserAgent    string `json:"userAgent"`
		ExtraHeaders string `json:"extraHeaders"`
	}{url, httpReferrer, userAgent, extraHeaders}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.LoadURL:", err)
	} else {
		fmt.Println("marshalling WebContents.LoadURL worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/loadURL", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call LoadURL function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("WebContents LoadURL response=", resp.Status, string(r))
}

// DownloadURL initiates a download of the resource at url without navigating. The will-download event of session will be triggered.
func (web *WebContents) DownloadURL(url string) {
	data := struct {
		Url string `json:"url"`
	}{url}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.DownloadURL:", err)
	} else {
		fmt.Println("marshalling WebContents.DownloadURL worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/downloadURL", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call DownloadURL function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("WebContents DownloadURL response=", resp.Status, string(r))
}

// GetURL returns string - The URL of the current web page.
func (web *WebContents) GetURL() string {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getURL", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetURL function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Url string `json:"url"`
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetURL:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetURL:", resp.StatusCode, data.Err)
		return ""
	}
	return data.Url
}

// GetTitle returns string - The title of the current web page.
func (web *WebContents) GetTitle() string {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getTitle", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetTitle function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Title string `json:"title"`
		Err   string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetTitle:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetTitle:", resp.StatusCode, data.Err)
		return ""
	}
	return data.Title
}

// IsDestroyed returns bool - Whether the web page is destroyed.
func (web *WebContents) IsDestroyed() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isDestroyed", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsDestroyed function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsDestroyed bool   `json:"isDestroyed"`
		Err         string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsDestroyed:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsDestroyed:", resp.StatusCode, data.Err)
	}
	return data.IsDestroyed
}

// IsFocused returns bool - Whether the web page is focused.
func (web *WebContents) IsFocused() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isFocused", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsFocused function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsFocused bool   `json:"isFocused"`
		Err       string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsFocused:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsFocused:", resp.StatusCode, data.Err)
	}
	return data.IsFocused
}

// IsLoading returns bool - Whether web page is still loading resources.
func (web *WebContents) IsLoading() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isLoading", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsLoading function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsLoading bool   `json:"isLoading"`
		Err       string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsLoading:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsLoading:", resp.StatusCode, data.Err)
	}
	return data.IsLoading
}

// IsLoadingMainFrame returns bool - Whether the main frame (and not just iframes or frames within it) is still loading.
func (web *WebContents) IsLoadingMainFrame() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isLoadingMainFrame", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsLoadingMainFrame function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsLoadingMainFrame bool   `json:"isLoadingMainFrame"`
		Err                string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsLoadingMainFrame:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsLoadingMainFrame:", resp.StatusCode, data.Err)
	}
	return data.IsLoadingMainFrame
}

// IsWaitingForResponse returns bool - Whether the web page is waiting for a first-response from the main resource of the page.
func (web *WebContents) IsWaitingForResponse() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isWaitingForResponse", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsWaitingForResponse function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsWaitingForResponse bool   `json:"isWaitingForResponse"`
		Err                  string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsWaitingForResponse:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsWaitingForResponse:", resp.StatusCode, data.Err)
	}
	return data.IsWaitingForResponse
}

// Stop stops any pending navigation.
func (web *WebContents) Stop() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/stop", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Stop function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Stop:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Stop:", resp.StatusCode, data.Err)
	}
}

// Reload the current web page.
func (web *WebContents) Reload() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/reload", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Reload function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Reload:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Reload:", resp.StatusCode, data.Err)
	}
}

// ReloadIgnoringCache Reloads current page and ignores cache.
func (web *WebContents) ReloadIgnoringCache() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/reloadIgnoringCache", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call ReloadIgnoringCache function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.ReloadIgnoringCache:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.ReloadIgnoringCache:", resp.StatusCode, data.Err)
	}
}

// CanGoBack Returns bool - Whether the browser can go back to previous web page.
func (web *WebContents) CanGoBack() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/canGoBack", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call CanGoBack function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		CanGoBack bool   `json:"canGoBack"`
		Err       string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.CanGoBack:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.CanGoBack:", resp.StatusCode, data.Err)
	}
	return data.CanGoBack
}

// CanGoForward Returns bool - Whether the browser can go forward to next web page.
func (web *WebContents) CanGoForward() bool {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/canGoForward", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call CanGoForward function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		CanGoForward bool   `json:"canGoForward"`
		Err          string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.CanGoForward:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.CanGoForward:", resp.StatusCode, data.Err)
	}
	return data.CanGoForward
}

// CanGoToOffset Returns bool - Whether the web page can go to offset.
func (web *WebContents) CanGoToOffset(offset int) bool {
	data := struct {
		Offset        int    `json:"offset"`
		CanGoToOffset bool   `json:"canGoToOffset"`
		Err           string `json:"error"`
	}{Offset: offset}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.CanGoToOffset:", err)
	} else {
		fmt.Println("marshalling WebContents.CanGoToOffset worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/canGoToOffset", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call CanGoToOffset function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.CanGoToOffset:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.CanGoToOffset:", resp.StatusCode, data.Err)
	}
	return data.CanGoToOffset
}

// ClearHistory Clears the navigation history.
func (web *WebContents) ClearHistory() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/clearHistory", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call clearHistory function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.clearHistory:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.clearHistory:", resp.StatusCode, data.Err)
	}
}

// GoBack Makes the browser go back a web page.
func (web *WebContents) GoBack() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/goBack", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GoBack function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GoBack:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GoBack:", resp.StatusCode, data.Err)
	}
}

// GoForward Makes the browser go forward a web page.
func (web *WebContents) GoForward() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/goForward", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GoForward function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GoForward:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GoForward:", resp.StatusCode, data.Err)
	}
}

// GoToIndex Navigates browser to the specified absolute web page index.
func (web *WebContents) GoToIndex(index int) {
	data := struct {
		Index int    `json:"index"`
		Err   string `json:"error"`
	}{Index: index}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.GoToIndex:", err)
	} else {
		fmt.Println("marshalling WebContents.GoToIndex worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/goToIndex", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GoToIndex function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GoToIndex:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GoToIndex:", resp.StatusCode, data.Err)
	}
}

// GoToOffset Navigates to the specified offset from the “current entry”.
func (web *WebContents) GoToOffset(offset int) {
	data := struct {
		Offset int    `json:"offset"`
		Err    string `json:"error"`
	}{Offset: offset}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.GoToOffset:", err)
	} else {
		fmt.Println("marshalling WebContents.GoToOffset worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/goToOffset", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GoToOffset function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GoToOffset:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GoToOffset:", resp.StatusCode, data.Err)
	}
}

// IsCrashed Returns Boolean - Whether the renderer process has crashed.
func (web *WebContents) IsCrashed() bool {
	data := struct {
		IsCrashed bool   `json:"isCrashed"`
		Err       string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isCrashed", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsCrashed function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsCrashed:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsCrashed:", resp.StatusCode, data.Err)
	}
	return data.IsCrashed
}

// SetUserAgent Overrides the user agent for this web page.
func (web *WebContents) SetUserAgent(userAgent string) {
	data := struct {
		UserAgent string `json:"userAgent"`
		Err       string `json:"error"`
	}{UserAgent: userAgent}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetUserAgent:", err)
	} else {
		fmt.Println("marshalling WebContents.SetUserAgent worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setUserAgent", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetUserAgent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetUserAgent:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetUserAgent:", resp.StatusCode, data.Err)
	}
}

// GetUserAgent Returns String - The user agent for this web page.
func (web *WebContents) GetUserAgent() string {
	data := struct {
		UserAgent string `json:"userAgent"`
		Err       string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getUserAgent", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetUserAgent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetUserAgent:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetUserAgent:", resp.StatusCode, data.Err)
	}
	return data.UserAgent
}

// InsertCSS Injects CSS into the current web page.
func (web *WebContents) InsertCSS(css string) {
	data := struct {
		CSS string `json:"css"`
		Err string `json:"error"`
	}{CSS: css}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.InsertCSS:", err)
	} else {
		fmt.Println("marshalling WebContents.InsertCSS worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/insertCSS", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call InsertCSS function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.InsertCSS:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.InsertCSS:", resp.StatusCode, data.Err)
	}
}

// ExecuteJavaScript Evaluates code in page.
func (web *WebContents) ExecuteJavaScript(code string, userGesture bool) {
	data := struct {
		Code        string `json:"code"`
		UserGesture bool   `json:"userGesture"`
		Err         string `json:"error"`
	}{Code: code, UserGesture: userGesture}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.ExecuteJavaScript:", err)
	} else {
		fmt.Println("marshalling WebContents.ExecuteJavaScript worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/executeJavaScript", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call ExecuteJavaScript function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.ExecuteJavaScript:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.ExecuteJavaScript:", resp.StatusCode, data.Err)
	}
}

// SetAudioMuted Mute the audio on the current web page.
func (web *WebContents) SetAudioMuted(muted bool) {
	data := struct {
		Muted bool   `json:"muted"`
		Err   string `json:"error"`
	}{Muted: muted}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetAudioMuted:", err)
	} else {
		fmt.Println("marshalling WebContents.SetAudioMuted worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setAudioMuted", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetAudioMuted function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetAudioMuted:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetAudioMuted:", resp.StatusCode, data.Err)
	}
}

// IsAudioMuted Returns Boolean - Whether this page has been muted.
func (web *WebContents) IsAudioMuted() bool {
	data := struct {
		IsAudioMuted bool   `json:"isAudioMuted"`
		Err          string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isAudioMuted", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsAudioMuted function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsAudioMuted:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsAudioMuted:", resp.StatusCode, data.Err)
	}
	return data.IsAudioMuted
}

// SetZoomFactor Changes the zoom factor to the specified factor. Zoom factor is zoom percent divided by 100, so 300% = 3.0.
func (web *WebContents) SetZoomFactor(factor float64) {
	data := struct {
		Factor float64 `json:"factor"`
		Err    string  `json:"error"`
	}{Factor: factor}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetZoomFactor:", err)
	} else {
		fmt.Println("marshalling WebContents.SetZoomFactor worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setZoomFactor", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetZoomFactor function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetZoomFactor:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetZoomFactor:", resp.StatusCode, data.Err)
	}
}

// GetZoomFactor Returns float64 - Get current zoom factor
func (web *WebContents) GetZoomFactor() float64 {
	data := struct {
		Factor float64 `json:"factor"`
		Err    string  `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getZoomFactor", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetZoomFactor function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetZoomFactor:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetZoomFactor:", resp.StatusCode, data.Err)
	}
	return data.Factor
}

// SetZoomLevel Changes the zoom level to the specified level. The original size is 0 and each increment above or below represents zooming 20% larger or smaller to default limits of 300% and 50% of original size, respectively.
func (web *WebContents) SetZoomLevel(level float64) {
	data := struct {
		Level float64 `json:"level"`
		Err   string  `json:"error"`
	}{Level: level}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetZoomLevel:", err)
	} else {
		fmt.Println("marshalling WebContents.SetZoomLevel worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setZoomLevel", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetZoomLevel function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetZoomLevel:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetZoomLevel:", resp.StatusCode, data.Err)
	}
}

// GetZoomLevel Sends a request to get current zoom level
func (web *WebContents) GetZoomLevel() float64 {
	data := struct {
		Level float64 `json:"level"`
		Err   string  `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getZoomLevel", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetZoomLevel function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetZoomLevel:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetZoomLevel:", resp.StatusCode, data.Err)
	}
	return data.Level
}

// SetZoomLevelLimits Sets the maximum and minimum zoom level.
func (web *WebContents) SetZoomLevelLimits(minimumLevel, maximumLevel float64) {
	data := struct {
		MinimumLevel float64 `json:"minimumLevel"`
		MaximumLevel float64 `json:"maximumLevel"`
		Err          string  `json:"error"`
	}{MinimumLevel: minimumLevel, MaximumLevel: maximumLevel}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetZoomLevelLimits:", err)
	} else {
		fmt.Println("marshalling WebContents.SetZoomLevelLimits worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setZoomLevelLimits", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetZoomLevelLimits function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetZoomLevelLimits:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetZoomLevelLimits:", resp.StatusCode, data.Err)
	}
}

// Undo Executes the editing command undo in web page.
func (web *WebContents) Undo() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/undo", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Undo function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Undo:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Undo:", resp.StatusCode, data.Err)
	}
}

// Redo Executes the editing command redo in web page.
func (web *WebContents) Redo() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/redo", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Redo function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Redo:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Redo:", resp.StatusCode, data.Err)
	}
}

// Cut Executes the editing command cut in web page.
func (web *WebContents) Cut() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/cut", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Cut function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Cut:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Cut:", resp.StatusCode, data.Err)
	}
}

// Copy Executes the editing command copy in web page.
func (web *WebContents) Copy() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/copy", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Copy function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Copy:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Copy:", resp.StatusCode, data.Err)
	}
}

// CopyImageAt Copy the image at the given position to the clipboard.
func (web *WebContents) CopyImageAt(x, y int) {
	data := struct {
		X   int    `json:"x"`
		Y   int    `json:"y"`
		Err string `json:"error"`
	}{X: x, Y: y}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.CopyImageAt:", err)
	} else {
		fmt.Println("marshalling WebContents.CopyImageAt worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/copyImageAt", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call CopyImageAt function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.CopyImageAt:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.CopyImageAt:", resp.StatusCode, data.Err)
	}
}

// Paste Executes the editing command paste in web page.
func (web *WebContents) Paste() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/paste", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Paste function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Paste:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Paste:", resp.StatusCode, data.Err)
	}
}

// PasteAndMatchStyle Executes the editing command pasteAndMatchStyle in web page.
func (web *WebContents) PasteAndMatchStyle() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/pasteAndMatchStyle", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call PasteAndMatchStyle function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.PasteAndMatchStyle:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.PasteAndMatchStyle:", resp.StatusCode, data.Err)
	}
}

// Delete Executes the editing command delete in web page.
func (web *WebContents) Delete() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/delete", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Delete function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Delete:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Delete:", resp.StatusCode, data.Err)
	}
}

// SelectAll Executes the editing command selectAll in web page.
func (web *WebContents) SelectAll() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/selectAll", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SelectAll function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SelectAll:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SelectAll:", resp.StatusCode, data.Err)
	}
}

// Unselect Executes the editing command unselect in web page.
func (web *WebContents) Unselect() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/unselect", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Unselect function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Unselect:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Unselect:", resp.StatusCode, data.Err)
	}
}

// Replace Executes the editing command replace in web page.
func (web *WebContents) Replace(text string) {
	data := struct {
		Text string `json:"text"`
		Err  string `json:"error"`
	}{Text: text}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.Replace:", err)
	} else {
		fmt.Println("marshalling WebContents.Replace worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/replace", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Replace function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Replace:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Replace:", resp.StatusCode, data.Err)
	}
}

// ReplaceMisspelling Executes the editing command replaceMisspelling in web page.
func (web *WebContents) ReplaceMisspelling(text string) {
	data := struct {
		Text string `json:"text"`
		Err  string `json:"error"`
	}{Text: text}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.ReplaceMisspelling:", err)
	} else {
		fmt.Println("marshalling WebContents.ReplaceMisspelling worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/replaceMisspelling", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call ReplaceMisspelling function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.ReplaceMisspelling:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.ReplaceMisspelling:", resp.StatusCode, data.Err)
	}
}

// InsertText Inserts text to the focused element.
func (web *WebContents) InsertText(text string) {
	data := struct {
		Text string `json:"text"`
		Err  string `json:"error"`
	}{Text: text}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.InsertText:", err)
	} else {
		fmt.Println("marshalling WebContents.InsertText worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/insertText", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call InsertText function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.InsertText:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.InsertText:", resp.StatusCode, data.Err)
	}
}

// FindInPage Starts a request to find all matches for the text in the web page and returns an Integer representing the request id used for the request. The result of the request can be obtained by subscribing to found-in-page event.
func (web *WebContents) FindInPage(text string, options FindInPageOptions) int {
	data := struct {
		Text    string            `json:"text"`
		Options FindInPageOptions `json:"options"`
		RId     int               `json:"rid"`
		Err     string            `json:"error"`
	}{Text: text, Options: options}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.FindInPage:", err)
	} else {
		fmt.Println("marshalling WebContents.FindInPage worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/findInPage", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call FindInPage function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.FindInPage:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.FindInPage:", resp.StatusCode, data.Err)
	}
	return data.RId
}

// FindInPageOptions holds options for the findinpage func
type FindInPageOptions struct {
	Forward                  bool `json:"forward"`
	FindNext                 bool `json:"findNext"`
	MatchCase                bool `json:"matchCase"`
	WordStart                bool `json:"wordStart"`
	MedialCapitalAsWordStart bool `json:"medialCapitalAsWordStart"`
}

// StopFindInPage Stops any findInPage request for the webContents with the provided action.
// action can be: clearSelection keepSelection activateSelection
func (web *WebContents) StopFindInPage(action string) {
	data := struct {
		Action string `json:"action"`
		Err    string `json:"error"`
	}{Action: action}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.StopFindInPage:", err)
	} else {
		fmt.Println("marshalling WebContents.StopFindInPage worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/stopFindInPage", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call StopFindInPage function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.StopFindInPage:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.StopFindInPage:", resp.StatusCode, data.Err)
	}
}

/* TODO: NativeImage
contents.capturePage([rect, ]callback)
rect Object (optional) - The area of the page to be captured
x Integer
y Integer
width Integer
height Integer
callback Function
Captures a snapshot of the page within rect. Upon completion callback will be called with callback(image). The image is an instance of NativeImage that stores data of the snapshot. Omitting rect will capture the whole visible page.
*/

// HasServiceWorker Checks if any ServiceWorker is registered
func (web *WebContents) HasServiceWorker() bool {
	data := struct {
		HasServiceWorker bool   `json:"hasServiceWorker"`
		Err              string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/hasServiceWorker", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call HasServiceWorker function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.HasServiceWorker:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.HasServiceWorker:", resp.StatusCode, data.Err)
	}
	return data.HasServiceWorker
}

// UnregisterServiceWorker Unregisters any ServiceWorker if present and returns a boolean as response when the JS promise is fulfilled or false when the JS promise is rejected.
func (web *WebContents) UnregisterServiceWorker() bool {
	data := struct {
		Success bool   `json:"success"`
		Err     string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/unregisterServiceWorker", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call UnregisterServiceWorker function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.UnregisterServiceWorker:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.UnregisterServiceWorker:", resp.StatusCode, data.Err)
	}
	return data.Success
}

// Print Prints window’s web page. When silent is set to true, Electron will pick up system’s default printer and default settings for printing.
func (web *WebContents) Print(silent, printbackground bool) {
	data := struct {
		Silent          bool   `json:"silent"`
		PrintBackground bool   `json:"printBackground"`
		Err             string `json:"error"`
	}{Silent: silent, PrintBackground: printbackground}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.Print:", err)
	} else {
		fmt.Println("marshalling WebContents.Print worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/print", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Print function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Print:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Print:", resp.StatusCode, data.Err)
	}
}

// PrintToPDF Prints window’s web page as PDF with Chromium’s preview printing custom settings.
func (web *WebContents) PrintToPDF(marginsType int, pageSize string, printbackground, printSelectionOnly, landscape bool) []byte {
	data := struct {
		MarginsType        int    `json:"marginsType"`
		PageSize           string `json:"pageSize"`
		PrintBackground    bool   `json:"printBackground"`
		PrintSelectionOnly bool   `json:"printSelectionOnly"`
		Landscape          bool   `json:"landscape"`
		Err                string `json:"error"`
		Data               string `json:"d"`
	}{marginsType, pageSize, printbackground, printSelectionOnly, landscape, "", ""}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.PrintToPDF:", err)
	} else {
		fmt.Println("marshalling WebContents.PrintToPDF worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/printToPDF", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call PrintToPDF function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.PrintToPDF:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.PrintToPDF:", resp.StatusCode, data.Err)
	}
	pdfData, err := hex.DecodeString(data.Data)
	if err != nil {
		fmt.Println("ERROR in WebContents.PrintToPDF hex.DecodeString(data.Data):", err)
	}
	return pdfData
}

// AddWorkSpace Adds the specified path to DevTools workspace. Must be used after DevTools creation:
func (web *WebContents) AddWorkSpace(path string) {
	data := struct {
		Path string `json:"path"`
		Err  string `json:"error"`
	}{Path: path}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.AddWorkSpace:", err)
	} else {
		fmt.Println("marshalling WebContents.AddWorkSpace worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/addWorkSpace", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call AddWorkSpace function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.AddWorkSpace:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.AddWorkSpace:", resp.StatusCode, data.Err)
	}
}

// RemoveWorkSpace Removes the specified path from DevTools workspace.
func (web *WebContents) RemoveWorkSpace(path string) {
	data := struct {
		Path string `json:"path"`
		Err  string `json:"error"`
	}{Path: path}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.RemoveWorkSpace:", err)
	} else {
		fmt.Println("marshalling WebContents.RemoveWorkSpace worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/removeWorkSpace", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call RemoveWorkSpace function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.RemoveWorkSpace:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.RemoveWorkSpace:", resp.StatusCode, data.Err)
	}
}

// OpenDevTools Opens the devtools.
// mode String - Opens the devtools with specified dock state, can be right, bottom, undocked, detach. Defaults to last used dock state. In undocked mode it’s possible to dock back. In detach mode it’s not.
func (web *WebContents) OpenDevTools(mode string) {
	data := struct {
		Mode string `json:"mode"`
		Err  string `json:"error"`
	}{Mode: mode}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.OpenDevTools:", err)
	} else {
		fmt.Println("marshalling WebContents.OpenDevTools worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/openDevTools", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call OpenDevTools function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.OpenDevTools:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.OpenDevTools:", resp.StatusCode, data.Err)
	}
}

// CloseDevTools Closes the devtools.
func (web *WebContents) CloseDevTools() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/closeDevTools", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call CloseDevTools function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.CloseDevTools:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.CloseDevTools:", resp.StatusCode, data.Err)
	}
}

// IsDevToolsOpened Returns Whether the devtools is opened.
func (web *WebContents) IsDevToolsOpened() bool {
	data := struct {
		IsDevToolsOpened bool   `json:"isDevToolsOpened"`
		Err              string `json:"error"`
	}{}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.IsDevToolsOpened:", err)
	} else {
		fmt.Println("marshalling WebContents.IsDevToolsOpened worked:", string(jsData))
	}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isDevToolsOpened", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsDevToolsOpened function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsDevToolsOpened:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsDevToolsOpened:", resp.StatusCode, data.Err)
	}
	return data.IsDevToolsOpened
}

// IsDevToolsFocused Returns Whether the devtools view is focused
func (web *WebContents) IsDevToolsFocused() bool {
	data := struct {
		IsDevToolsFocused bool   `json:"isDevToolsFocused"`
		Err               string `json:"error"`
	}{}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.IsDevToolsFocused:", err)
	} else {
		fmt.Println("marshalling WebContents.IsDevToolsFocused worked:", string(jsData))
	}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isDevToolsFocused", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsDevToolsFocused function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsDevToolsFocused:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsDevToolsFocused:", resp.StatusCode, data.Err)
	}
	return data.IsDevToolsFocused
}

// ToggleDevTools Toggles the developer tools.
func (web *WebContents) ToggleDevTools() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/toggleDevTools", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call ToggleDevTools function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.ToggleDevTools:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.ToggleDevTools:", resp.StatusCode, data.Err)
	}
}

// InspectElement Starts inspecting element at position (x, y).
func (web *WebContents) InspectElement(x, y int) {
	data := struct {
		X   int    `json:"x"`
		Y   int    `json:"y"`
		Err string `json:"error"`
	}{X: x, Y: y}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.InspectElement:", err)
	} else {
		fmt.Println("marshalling WebContents.InspectElement worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/inspectElement", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call InspectElement function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.InspectElement:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.InspectElement:", resp.StatusCode, data.Err)
	}
}

// InspectServiceWorker Opens the developer tools for the service worker context.
func (web *WebContents) InspectServiceWorker() {
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/inspectServiceWorker", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call InspectServiceWorker function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Err string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.InspectServiceWorker:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.InspectServiceWorker:", resp.StatusCode, data.Err)
	}
}

// Send an asynchronous message to renderer process via channel, you can also send arbitrary arguments. Arguments will be serialized in JSON internally and hence no functions or prototype chain will be included.
func (web *WebContents) Send(channel string, args []string) {
	data := struct {
		Channel string   `json:"channel"`
		Args    []string `json:"args"`
		Err     string   `json:"error"`
	}{Channel: channel, Args: args}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.Send:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/send", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Send function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR unmarshalling WebContents.Send:", err)
		}
		fmt.Println("ERROR in WebContents.Send:", resp.StatusCode, data.Err)
	}
}

// EnableDeviceEmulation Enable device emulation with the given parameters.
// screenPosition string - Specify the screen type to emulate (desktop|mobile)
// screen Rect - Set the emulated screen size and view(default{x: 0, y: 0})
// deviceScaleFactor int - Set the device scale factor (if zero defaults to original device scale factor)
// view Rect - Set the view size and offset. Values of -1 indicates no override
// fitToView bool - Whether view should be scaled down if necessary to fit into available space
// scale float64 - Scale of view inside available space: 1.0 is no scaling
func (web *WebContents) EnableDeviceEmulation(screenPosition string, screen Rect, deviceScaleFactor int, view Rect, fitToView bool, scale float64) {
	data := struct {
		ScreenPosition    string  `json:"screenPosition"`
		Screen            Rect    `json:"screen"`
		DeviceScaleFactor int     `json:"deviceScaleFactor"`
		View              Rect    `json:"view"`
		FitToView         bool    `json:"fitToView"`
		Scale             float64 `json:"scale"`
		Err               string  `json:"error"`
	}{ScreenPosition: screenPosition, Screen: screen}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.EnableDeviceEmulation:", err)
	} else {
		fmt.Println("marshalling WebContents.EnableDeviceEmulation worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/enableDeviceEmulation", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call EnableDeviceEmulation function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.EnableDeviceEmulation:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.EnableDeviceEmulation:", resp.StatusCode, data.Err)
	}
}

// DisableDeviceEmulation Disable device emulation enabled by webContents.enableDeviceEmulation.
func (web *WebContents) DisableDeviceEmulation() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/disableDeviceEmulation", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call DisableDeviceEmulation function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.DisableDeviceEmulation:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.DisableDeviceEmulation:", resp.StatusCode, data.Err)
	}
}

// SendInputEvent Sends an input event to the page.
// eventName string - Can be mouseDown, mouseUp, mouseEnter, mouseLeave, contextMenu, mouseWheel, mouseMove, keyDown, keyUp, char.
// modifiers []string - Can include shift, control, alt, meta, isKeypad, isAutoRepeat, leftButtonDown, middleButtonDown, rightButtonDown, capsLock, numLock, left, right.
// Keyboard events must set keyCode *string (non nil) - The character that will be sent as the keyboard event. Should only use the valid key codes in Accelerator.
// Mouse events must set mouseEvent to a non nil MouseEvent, nil otherwise.
func (web *WebContents) SendInputEvent(eventName string, modifiers []string, keyCode *string, mouseEvent *MouseEvent, mouseWheelEvent *MouseWheelEvent) {
	data := struct {
		EventName       string          `json:"eventName"`
		Modifiers       []string        `json:"modifiers"`
		KeyCode         string          `json:"keyCode"`
		MouseEvent      MouseEvent      `json:"mouseEvent"`
		MouseWheelEvent MouseWheelEvent `json:"mouseWheelEvent"`
		Err             string          `json:"error"`
	}{EventName: eventName, Modifiers: modifiers}
	if keyCode != nil {
		data.KeyCode = *keyCode
	}
	if mouseEvent != nil {
		data.MouseEvent = *mouseEvent
	}
	if mouseWheelEvent != nil {
		data.MouseWheelEvent = *mouseWheelEvent
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SendInputEvent:", err)
	} else {
		fmt.Println("marshalling WebContents.SendInputEvent worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/sendInputEvent", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SendInputEvent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SendInputEvent:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SendInputEvent:", resp.StatusCode, data.Err)
	}
}

// MouseEvent stores data for mouse events
type MouseEvent struct {
	X          int    `json:"x"`
	Y          int    `json:"y"`
	Button     string `json:"button"` // - The button pressed, can be left, middle, right
	GlobalX    int    `json:"globalX"`
	GlobalY    int    `json:"globalY"`
	MovementX  int    `json:"movementX"`
	MovementY  int    `json:"movementY"`
	ClickCount int    `json:"clickCount"`
}

// MouseWheelEvent stores data for mouse wheel events
type MouseWheelEvent struct {
	DeltaX                    int  `json:"deltaX"`
	DeltaY                    int  `json:"deltaY"`
	WheelTicksX               int  `json:"wheelTicksX"`
	WheelTicksY               int  `json:"wheelTicksY"`
	AccelerationRatioX        int  `json:"accelerationRatioX"`
	AccelerationRatioY        int  `json:"accelerationRatioY"`
	HasPreciseScrollingDeltas bool `json:"hasPreciseScrollingDeltas"`
	CanScroll                 bool `json:"canScroll"`
}

// TODO:contents.beginFrameSubscription([onlyDirty ,]callback)
// onlyDirty Boolean (optional) - Defaults to false
// callback Function
// Begin subscribing for presentation events and captured frames, the callback will be called with callback(frameBuffer, dirtyRect) when there is a presentation event.
//
// The frameBuffer is a Buffer that contains raw pixel data. On most machines, the pixel data is effectively stored in 32bit BGRA format, but the actual representation depends on the endianness of the processor (most modern processors are little-endian, on machines with big-endian processors the data is in 32bit ARGB format).
//
// The dirtyRect is an object with x, y, width, height properties that describes which part of the page was repainted. If onlyDirty is set to true, frameBuffer will only contain the repainted area. onlyDirty defaults to false.
// func (web *WebContents) BeginFrameSubscription(onlyDirty bool) {
// 	data := struct {
// 		OnlyDirty bool   `json:"onlyDirty"`
// 		Err       string `json:"error"`
// 	}{OnlyDirty: onlyDirty}
// 	jsData, err := json.Marshal(data)
// 	if err != nil {
// 		fmt.Println("ERROR marshalling WebContents.BeginFrameSubscription:", err)
// 	} else {
// 		fmt.Println("marshalling WebContents.BeginFrameSubscription worked:", string(jsData))
// 	}
// 	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/beginFrameSubscription", bytes.NewBuffer(jsData))
// 	resp, err := web.elecClient.Do(req)
// 	if err != nil {
// 		fmt.Println("ERROR WebContents failed to call BeginFrameSubscription function:", err)
// 	}
// 	r, _ := ioutil.ReadAll(resp.Body)
// 	err = json.Unmarshal(r, &data)
// 	if err != nil {
// 		fmt.Println("ERROR unmarshalling WebContents.BeginFrameSubscription:", err)
// 	}
// 	if resp.StatusCode != 200 {
// 		fmt.Println("ERROR in WebContents.BeginFrameSubscription:", resp.StatusCode, data.Err)
// 	}
// }

// contents.endFrameSubscription()
// End subscribing for frame presentation events.

// TODO: contents.startDrag(item)
// item object
// file String
// icon NativeImage
// Sets the item as dragging item for current drag-drop operation, file is the absolute path of the file to be dragged, and icon is the image showing under the cursor when dragging.

// SavePage saves webContents' content to file.
// SaveType can be HTMLOnly, HTMLComplete or MHTML
func (web *WebContents) SavePage(savePath, saveType string) error {
	data := struct {
		SavePath string `json:"fullPath"`
		SaveType string `json:"saveType"`
		Err      string `json:"error"`
	}{SavePath: savePath, SaveType: saveType}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SavePage:", err)
	} else {
		fmt.Println("marshalling WebContents.SavePage worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/savePage", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SavePage function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SavePage:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SavePage:", resp.StatusCode, data.Err)
	}
	return errors.New(data.Err)
}

// ShowDefinitionForSelection macOS - Shows pop-up dictionary that searches the selected word on the page.
func (web *WebContents) ShowDefinitionForSelection(savePath, saveType string) {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/showDefinitionForSelection", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call ShowDefinitionForSelection function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.ShowDefinitionForSelection:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.ShowDefinitionForSelection:", resp.StatusCode, data.Err)
	}
}

// IsOffscreen () bool - Indicates whether offscreen rendering is enabled.
func (web *WebContents) IsOffscreen() bool {
	data := struct {
		IsOffscreen bool   `json:"isOffscreen"`
		Err         string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isOffscreen", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsOffscreen function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsOffscreen:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsOffscreen:", resp.StatusCode, data.Err)
	}
	return data.IsOffscreen
}

// StartPainting If offscreen rendering is enabled and not painting
func (web *WebContents) StartPainting() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/startPainting", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call StartPainting function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.StartPainting:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.StartPainting:", resp.StatusCode, data.Err)
	}
}

// StopPainting If offscreen rendering is enabled and painting, stop painting.
func (web *WebContents) StopPainting() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/stopPainting", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call StopPainting function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.StopPainting:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.StopPainting:", resp.StatusCode, data.Err)
	}
}

// IsPainting bool - If offscreen rendering is enabled returns whether it is currently painting.
func (web *WebContents) IsPainting() bool {
	data := struct {
		IsPainting bool   `json:"isPainting"`
		Err        string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/isPainting", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call IsPainting function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.IsPainting:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.IsPainting:", resp.StatusCode, data.Err)
	}
	return data.IsPainting
}

// SetFrameRate (fps int) If offscreen rendering is enabled sets the frame rate to the specified number. Only values between 1 and 60 are accepted.
func (web *WebContents) SetFrameRate(fps int) {
	data := struct {
		FPS int    `json:"fps"`
		Err string `json:"error"`
	}{FPS: fps}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebContents.SetFrameRate:", err)
	} else {
		fmt.Println("marshalling WebContents.SetFrameRate worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/setFrameRate", bytes.NewBuffer(jsData))
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call SetFrameRate function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.SetFrameRate:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.SetFrameRate:", resp.StatusCode, data.Err)
	}
}

// GetFrameRate () int - If offscreen rendering is enabled returns the current frame rate.
func (web *WebContents) GetFrameRate() int {
	data := struct {
		FPS int    `json:"fps"`
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/getFrameRate", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call GetFrameRate function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.GetFrameRate:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.GetFrameRate:", resp.StatusCode, data.Err)
	}
	return data.FPS
}

// Invalidate - If offscreen rendering is enabled invalidates the frame and generates a new one through the 'paint' event.
func (web *WebContents) Invalidate() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/WebContents/"+strconv.Itoa(web.Id)+"/invalidate", nil)
	resp, err := web.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR WebContents failed to call Invalidate function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebContents.Invalidate:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebContents.Invalidate:", resp.StatusCode, data.Err)
	}
}

// TODO: Debugger is an alternate transport for Chrome’s remote debugging protocol. Chrome Developer Tools has a special binding available at JavaScript runtime that allows interacting with pages and instrumenting them
// type Debugger struct {
// }
