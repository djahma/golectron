# Documentation

## Architecture overview
This wrapper is built around two servers. One on Go side, the other on the javascript side. When calling a Go func, a request is sent to the javascript side for Electron to do the equivalent function call, then return the values to the Go side which marshalls the answer into a Go struct.  
The key here is to maintain both sides in sync, especially true with "objects" such as BrowserWindow, MenuItem or Events. When destroyed on one side, an object must be destroyed on the other. That's why most instantiable objects have a destroy() function. Dereferencing an object on one side will cause a memory leak.

This wrapper was built on a Linux amd64 platform and was not tested elsewhere, therefore it may fail for your particular platform.

## Initialisation

One initialises golectron calling `NewGolectron()` func.  
Golectron first crawls the folder hierarchy for the paths to _itself_, _golectronApp_, _Electron_ install. Once it found itself, it searches for Electron install first in its own directory, then in the `$PATH`. The *electron* binary must be contained in a folder whose name starts by 'electron' to be found.

The `config.json` file is read before launching Go server. This is where you can choose specific ports for both Go and JavaScript servers. A **0** value means 'let golectron choose random ports'.  
An `httprouter` is instantiated and Go server started.

Now that everything is set on **Go** side, **Electron** can be started. It is given the path to *golectronApp*, the server port to use and the client port as arguments.  
This app basically launches an **express** router with all the routes corresponding to the implemented **Electron** API set. It also make use of a `goelec` object to store objects to stay in sync with Go.  
When done, it sends a *ready* signal as a request to Go server.

Once **Go** receives the signal, it can create its client to the JavaScript server. The `NewGolectron()` func returns.

## Communication scheme
When the user calls a golectron func, a request is sent to the *golectronApp* **express** middleware. The **Go** arguments are serialized into `json`. Returned values are also in `json` and marshalled into **Go** struct.

**Beware: Not all routes have been debugged!!!**

### Object maps
When the user calls a 'New' struct func from Go, an 'object' is created and stored in a map.  
Take the `NewBrowserWindow()` func as an example. When called, a request is sent on the route `http://localhost/create/browserWindow`. Electron will route that to the `r_create(...)` function in *golectronApp/routes.js* file. Then call `new BrowserWindow()` from Electron API and store the returned window into `goelec.windows[]` array. This new window will have an *id* which will be returned to **Go**. golectron will create a `&BrowserWindow{}` in its turn, and store it in `golectron.windows` map. Because a BrowserWindow support *Events*, its `eventMap` is initialised before the `NewBrowserWindow()` func returns the window.

I'm guessing you see the pitfall of not having a garbage collector in this wrapper: If you dereference the window struct of our example in Go, it would still live on the JavaScript side, and memory is leaked.  
The user must use the `Destroy()` func on the BrowserWindow receiver to avoid this issue in our example.
