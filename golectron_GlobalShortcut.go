package golectron

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

var globalShortcuts map[string]func()

// Register registers a global shortcut of accelerator. An event is sent to /app/event/globalShortcut with the accelerator pressed by the user.
// When the accelerator is already taken by other applications, this call will silently fail. This behavior is intended by operating systems, since they don’t want applications to fight for global shortcuts.
func (g *Golectron) Register(accelerator string, handler func()) {
	data := struct {
		Accelerator string `json:"accelerator"`
		Error       string `json:"error"`
	}{Accelerator: accelerator}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling GlobalShortcut.Register:", err)
	} else {
		fmt.Println("marshalling GlobalShortcut.Register worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/GlobalShortcut/register", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR /GlobalShortcut/register:", err)
		return
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR GlobalShortcut failed to Unmarshal Register error message:", err)
			return
		}
		fmt.Println("ERROR GlobalShortcut.Register failed:", data.Error)
		return
	}
	if globalShortcuts == nil {
		globalShortcuts = make(map[string]func())
	}
	globalShortcuts[data.Accelerator] = handler
}

// IsRegistered returns whether this application has registered accelerator.
// When the accelerator is already taken by other applications, this call will still return false. This behavior is intended by operating systems, since they don’t want applications to fight for global shortcuts.
func (g *Golectron) IsRegistered(accelerator string) (bool, error) {
	data := struct {
		Accelerator string `json:"accelerator"`
		Error       string `json:"error"`
		IsR         bool   `json:"isRegistered"`
	}{Accelerator: accelerator}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling GlobalShortcut.IsRegistered:", err)
	} else {
		fmt.Println("marshalling GlobalShortcut.IsRegistered worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/GlobalShortcut/isRegistered", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR @ /GlobalShortcut/isRegistered:", err)
		return false, err
	}
	r, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	fmt.Println("/GlobalShortcut/isRegistered body is:", string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR GlobalShortcut failed to Unmarshal Register error message:", err)
		return false, err
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR GlobalShortcut.Register failed:", data.Error)
		return false, errors.New(data.Error)
	}
	return data.IsR, nil
}

// UnRegister unregisters the global shortcut of accelerator.
func (g *Golectron) UnRegister(accelerator string) {
	data := struct {
		Accelerator string `json:"accelerator"`
		Error       string `json:"error"`
	}{Accelerator: accelerator}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling GlobalShortcut.UnRegister:", err)
	} else {
		fmt.Println("marshalling GlobalShortcut.UnRegister worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/GlobalShortcut/unregister", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR /GlobalShortcut/unregister:", err)
		return
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR GlobalShortcut failed to Unmarshal UnRegister error message:", err, "Response Body is:", string(r))
		} else {
			fmt.Println("ERROR GlobalShortcut.UnRegister failed:", data.Error)
		}
	}
}

// UnRegisterAll unregisters all of the global shortcuts.
func (g *Golectron) UnRegisterAll() {
	req, _ := http.NewRequest("GET", "http://localhost/GlobalShortcut/unregisterAll", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR /GlobalShortcut/unregisterAll:", err)
		return
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR GlobalShortcut.unregisterAll failed:", resp.StatusCode, resp.StatusCode)
	}
}
