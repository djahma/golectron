package golectron

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

//----------------------------------------------------------------------------
// Session Definition
//
//----------------------------------------------------------------------------

// Session manages browser sessions, cookies, cache, proxy settings, etc.
type Session struct {
	// Private
	elecClient   *http.Client
	GolectronCtx *Golectron
	eventMap     map[string]func(...interface{}) interface{}
	// Public
	Id         int         `json:"session_id"`
	Cookies    *Cookies    `json:"cookies"`
	WebRequest *WebRequest `json:"webRequest"`
	Protocol   *Protocol   `json:"protocol"`
}

// SessionFromPartition returns existing *Session or create a new one
func (g *Golectron) SessionFromPartition(partition string, cache bool) *Session {
	data := struct {
		Partition string `json:"partition"`
		Cache     bool   `json:"cache"`
	}{Partition: partition, Cache: cache}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling SessionFromPartition:", err)
	} else {
		fmt.Println("marshalling SessionFromPartition worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/fromPartition", bytes.NewBuffer(jsData))
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR SessionFromPartition call failed:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	s := Session{}
	err = json.Unmarshal(r, &s)
	if err != nil {
		fmt.Println("ERROR SessionFromPartition failed to Unmarshal:", err)
	}
	fmt.Println("SessionFromPartition response=", resp.Status, string(r))
	s.GolectronCtx = g
	s.elecClient = g.elecClient
	s.eventMap = make(map[string]func(...interface{}) interface{})
	g.sessions = append(g.sessions, &s)
	return &s
}

// SessionGetDefaultSession returns the default session object of the app.
func (g *Golectron) SessionGetDefaultSession() *Session {
	data := struct {
		Session Session `json:"sess"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/getDefaultSession", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR SessionFromPartition call failed:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR SessionFromPartition failed to Unmarshal:", err)
	}
	fmt.Println("SessionFromPartition response=", resp.Status, string(r))
	s := &data.Session
	s.elecClient = g.elecClient
	s.GolectronCtx = g
	s.eventMap = make(map[string]func(...interface{}) interface{})
	g.sessions = append(g.sessions, s)
	return s
}

func (g *Golectron) addSession(s *Session) {
	// add s if not already in g.sessions registry
	isThere := false
	for _, val := range g.sessions {
		if val.Id == s.Id {
			isThere = true
			break
		}
	}
	s.elecClient = g.elecClient
	s.GolectronCtx = g
	if isThere {
		return
	}
	g.sessions = append(g.sessions, s)
}

//----------------------------------------------------------------------------
// Session Instance Events
//
//----------------------------------------------------------------------------

/* Session has the following list of events:
will-download
*/

// On registers eventName event with fn func
func (s *Session) On(eventName string, fn func(...interface{}) interface{}) {
	req, err := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/listen4event/"+eventName, nil)
	if err != nil {
		fmt.Println("ERROR Session failed generate On request:", err)
	}
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR Session failed to Unmarshal Listen4Event error message:", err)
		}
		fmt.Println("ERROR Session couldn't listen to event", eventName, ":", data.Error)
	} else {
		s.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (s *Session) RemoveAllListeners(eventName string) {
	req, err := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/unlisten4event/"+eventName, nil)
	if err != nil {
		fmt.Println("ERROR Session failed generate RemoveAllListeners request:", err)
	}
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call RemoveAllListeners function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR Session failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR Session couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(s.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (s *Session) Emit(eventName string, data ...interface{}) interface{} {
	if val, ok := s.eventMap[eventName]; ok {
		res := val(data)
		return res
	}
	fmt.Println("ERROR: Received event in Session is out of sync with Electron. EventName=", eventName)
	return nil
}

// EventList returns an array of event names currently registered.
func (s *Session) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(s.eventMap))
	for key := range s.eventMap {
		evList = append(evList, key)
	}
	return evList
}

//----------------------------------------------------------------------------
// WebContents Instance Methods
//
//----------------------------------------------------------------------------

// GetCookies returns the Cookies property of s *Session, in sync between Go and JS
func (s *Session) GetCookies() *Cookies {
	data := struct {
		Cookies []Cookie `json:"cookies"`
		Err     string   `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getCookies", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetCookies function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetCookies response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetCookies:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetCookies:", resp.StatusCode, data.Err)
		return nil
	}
	c := &Cookies{SessionID: s.Id, Cookies: data.Cookies}
	c.GolectronCtx = s.GolectronCtx
	c.eventMap = make(map[string]func(...interface{}) interface{})
	s.Cookies = c
	return c
}

// GetWebRequest returns the WebRequest property of s *Session, in sync between Go and JS
func (s *Session) GetWebRequest() *WebRequest {
	data := struct {
		WebRequest *WebRequest `json:"webRequest"`
		Err        string      `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getWebRequest", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetWebRequest function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetWebRequest response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetWebRequest:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetWebRequest:", resp.StatusCode, data.Err)
		return nil
	}
	wr := data.WebRequest
	wr.GolectronCtx = s.GolectronCtx
	wr.SessionID = s.Id
	wr.eventMap = make(map[string]func(...interface{}) interface{})
	s.WebRequest = wr
	return wr
}

// GetProtocol returns the Protocol property of s *Session, in sync between Go and JS
func (s *Session) GetProtocol() *Protocol {
	data := struct {
		Protocol *Protocol `json:"protocol"`
		Err      string    `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getGetProtocol", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetProtocol:", resp.StatusCode, data.Err)
		return nil
	}
	p := data.Protocol
	p.golectronCtx = s.GolectronCtx
	p.SessionID = s.Id
	p.eventMap = make(map[string]func(...interface{}) interface{})
	s.Protocol = p
	return p
}

// GetCacheSize Returns the session’s current cache size.
func (s *Session) GetCacheSize() int {
	data := struct {
		CacheSize int    `json:"cacheSize"`
		Err       string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getCacheSize", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetCacheSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetCacheSize response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetCacheSize:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetCacheSize:", resp.StatusCode, data.Err)
		return -1
	}
	return data.CacheSize
}

// ClearCache Clears the session’s HTTP cache.
func (s *Session) ClearCache() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/clearCache", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call ClearCache function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session ClearCache response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.ClearCache:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.ClearCache:", resp.StatusCode, data.Err)
	}
}

// ClearStorageData Clears the data of web storages.
// origin string - Should follow window.location.origin’s representation scheme://host:port.
// storages []string - The types of storages to clear, can contain: appcache, cookies, filesystem, indexdb, local storage, shadercache, websql, serviceworkers
// quotas []string - The types of quotas to clear, can contain: temporary, persistent, syncable.
func (s *Session) ClearStorageData(origin string, storages, quotas []string) {
	data := struct {
		Origin   string   `json:"origin"`
		Storages []string `json:"storages"`
		Quotas   []string `json:"quotas"`
		Err      string   `json:"error"`
	}{Origin: origin, Storages: storages, Quotas: quotas}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.ClearStorageData:", err)
	} else {
		fmt.Println("marshalling Session.ClearStorageData worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/clearStorageData", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call ClearStorageData function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session ClearStorageData response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.ClearStorageData:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.ClearStorageData:", resp.StatusCode, data.Err)
	}
}

// FlushStorageData Writes any unwritten DOMStorage data to disk.
func (s *Session) FlushStorageData() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/flushStorageData", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call FlushStorageData function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session FlushStorageData response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.FlushStorageData:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.FlushStorageData:", resp.StatusCode, data.Err)
	}
}

// SetProxy Sets the proxy settings.
// arguments can be nil
// When pacScript and proxyRules are provided together, proxyRules is ignored and pacScript configuration is applied.
/*	The proxyRules has to follow the rules below:
	proxyRules = schemeProxies[";"<schemeProxies>]
	schemeProxies = [<urlScheme>"="]<proxyURIList>
	urlScheme = "http" | "https" | "ftp" | "socks"
	proxyURIList = <proxyURL>[","<proxyURIList>]
	proxyURL = [<proxyScheme>"://"]<proxyHost>[":"<proxyPort>]
*/
// check http://electron.atom.io/docs/api/session/ for more details
func (s *Session) SetProxy(pacScript, proxyRules, proxyBypassRules *string) {
	data := struct {
		PacScript        *string `json:"pacScript"`
		ProxyRules       *string `json:"proxyRules"`
		ProxyBypassRules *string `json:"proxyBypassRules"`
		Err              string  `json:"error"`
	}{}
	if pacScript != nil {
		data.PacScript = pacScript
	}
	if proxyRules != nil {
		data.ProxyRules = proxyRules
	}
	if proxyBypassRules != nil {
		data.ProxyBypassRules = proxyBypassRules
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.SetProxy:", err)
	} else {
		fmt.Println("marshalling Session.SetProxy worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/setProxy", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	fmt.Println("This is how SetProxy request looks:", req)
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call SetProxy function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session SetProxy response=", resp.Status, string(r))
	defer resp.Body.Close()
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.SetProxy:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.SetProxy:", resp.StatusCode, data.Err)
	}
}

// ResolveProxy Resolves the proxy information for url.
func (s *Session) ResolveProxy(url string) string {
	data := struct {
		URL   string `json:"url"`
		Proxy string `json:"proxy"`
		Err   string `json:"error"`
	}{}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.ResolveProxy:", err)
	} else {
		fmt.Println("marshalling Session.ResolveProxy worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/resolveProxy", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call ResolveProxy function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session ResolveProxy response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.ResolveProxy:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.ResolveProxy:", resp.StatusCode, data.Err)
	}
	return data.Proxy
}

// SetDownloadPath Sets download saving directory. By default, the download directory will be the Downloads under the respective app folder.
func (s *Session) SetDownloadPath(path string) {
	data := struct {
		Path string `json:"path"`
		Err  string `json:"error"`
	}{}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.SetDownloadPath:", err)
	} else {
		fmt.Println("marshalling Session.SetDownloadPath worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/setDownloadPath", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call SetDownloadPath function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session SetDownloadPath response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.SetDownloadPath:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.SetDownloadPath:", resp.StatusCode, data.Err)
	}
}

// EnableNetworkEmulation Emulates network with the given configuration for the session.
// offline bool Whether to emulate network outage.
// latency float64 - RTT in ms.
// downloadThroughput float64 - Download rate in Bps.
// uploadThroughput float64 - Upload rate in Bps.
func (s *Session) EnableNetworkEmulation(offline *bool, latency, downloadThroughput, uploadThroughput *float64) {
	data := struct {
		Offline            *bool    `json:"offline"`
		Latency            *float64 `json:"latency"`
		DownloadThroughput *float64 `json:"downloadThroughput"`
		UploadThroughput   *float64 `json:"uploadThroughput"`
		Err                string   `json:"error"`
	}{}
	if offline != nil {
		data.Offline = offline
	}
	if latency != nil {
		data.Latency = latency
	}
	if downloadThroughput != nil {
		data.DownloadThroughput = downloadThroughput
	}
	if uploadThroughput != nil {
		data.UploadThroughput = uploadThroughput
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.EnableNetworkEmulation:", err)
	} else {
		fmt.Println("marshalling Session.EnableNetworkEmulation worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/enableNetworkEmulation", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call EnableNetworkEmulation function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session EnableNetworkEmulation response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.EnableNetworkEmulation:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.EnableNetworkEmulation:", resp.StatusCode, data.Err)
	}
}

// DisableNetworkEmulation Disables any network emulation already active for the session. Resets to the original network configuration.
func (s *Session) DisableNetworkEmulation() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/disableNetworkEmulation", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call DisableNetworkEmulation function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session DisableNetworkEmulation response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.DisableNetworkEmulation:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.DisableNetworkEmulation:", resp.StatusCode, data.Err)
	}
}

// SetCertificateVerifyProc Sets the certificate verify proc for session, the proc will be called with proc(hostname, certificate, callback) whenever a server certificate verification is requested. Calling callback(true) accepts the certificate, calling callback(false) rejects it.
func (s *Session) SetCertificateVerifyProc(fn func(...interface{}) interface{}) {
	s.eventMap["certificateVerifyProc"] = fn
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/setCertificateVerifyProc", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call SetCertificateVerifyProc function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session SetCertificateVerifyProc response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.SetCertificateVerifyProc:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.SetCertificateVerifyProc:", resp.StatusCode, data.Err)
	}
}

// SetPermissionRequestHandler Sets the handler which can be used to respond to permission requests for the session. Handler will be passed json {id: webContents.id, permission: perm} with id the webContents' id requesting the permission perm. Handler must return bool: true is acceptance, false rejection
// permission string is any of ‘media’, ‘geolocation’, ‘notifications’, ‘midiSysex’, ‘pointerLock’, ‘fullscreen’, ‘openExternal’.
func (s *Session) SetPermissionRequestHandler(fn func(...interface{}) interface{}) {
	s.eventMap["permissionRequest"] = fn
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/setPermissionRequestHandler", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call SetPermissionRequestHandler function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session SetPermissionRequestHandler response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.SetPermissionRequestHandler:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.SetPermissionRequestHandler:", resp.StatusCode, data.Err)
	}
}

// ClearHostResolverCache Clears the host resolver cache.
func (s *Session) ClearHostResolverCache() {
	data := struct {
		Err string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/clearHostResolverCache", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call ClearHostResolverCache function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session ClearHostResolverCache response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.ClearHostResolverCache:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.ClearHostResolverCache:", resp.StatusCode, data.Err)
	}
}

// AllowNTLMCredentialsForDomains Dynamically sets whether to always send credentials for HTTP NTLM or Negotiate authentication.
// domains String - A comma-seperated list of servers for which integrated authentication is enabled.
func (s *Session) AllowNTLMCredentialsForDomains(domains string) {
	data := struct {
		Domains string `json:"domains"`
		Err     string `json:"error"`
	}{Domains: domains}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.AllowNTLMCredentialsForDomains:", err)
	} else {
		fmt.Println("marshalling Session.AllowNTLMCredentialsForDomains worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/allowNTLMCredentialsForDomains", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call AllowNTLMCredentialsForDomains function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session AllowNTLMCredentialsForDomains response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.AllowNTLMCredentialsForDomains:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.AllowNTLMCredentialsForDomains:", resp.StatusCode, data.Err)
	}
}

// SetUserAgent Overrides the userAgent and acceptLanguages for this session.
// The acceptLanguages must be a comma separated ordered list of language codes, for example "en-US,fr,de,ko,zh-CN,ja".
// This doesn’t affect existing WebContents, and each WebContents can use webContents.setUserAgent to override the session-wide user agent.
func (s *Session) SetUserAgent(userAgent string, acceptedLanguages *string) {
	data := struct {
		UserAgent         string  `json:"userAgent"`
		AcceptedLanguages *string `json:"acceptedLanguages"`
		Err               string  `json:"error"`
	}{UserAgent: userAgent}
	if acceptedLanguages != nil {
		data.AcceptedLanguages = acceptedLanguages
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.SetUserAgent:", err)
	} else {
		fmt.Println("marshalling Session.SetUserAgent worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/setUserAgent", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call SetUserAgent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session SetUserAgent response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.SetUserAgent:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.SetUserAgent:", resp.StatusCode, data.Err)
	}
}

// GetUserAgent Returns string - The user agent for this session.
func (s *Session) GetUserAgent() string {
	data := struct {
		UserAgent string `json:"userAgent"`
		Err       string `json:"error"`
	}{}
	req, _ := http.NewRequest("GET", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getUserAgent", nil)
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetUserAgent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetUserAgent response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetUserAgent:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetUserAgent:", resp.StatusCode, data.Err)
	}
	return data.UserAgent
}

// GetBlobData Returns Blob - The blob data associated with the identifier.
func (s *Session) GetBlobData(identifier string) []byte {
	data := struct {
		Identifier string `json:"identifier"`
		Blob       []byte `json:"blob"`
		Err        string `json:"error"`
	}{Identifier: identifier}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Session.GetBlobData:", err)
	} else {
		fmt.Println("marshalling Session.GetBlobData worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(s.Id)+"/getBlobData", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := s.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call GetBlobData function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session GetBlobData response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Session.GetBlobData:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Session.GetBlobData:", resp.StatusCode, data.Err)
	}
	return data.Blob
}

//----------------------------------------------------------------------------
// Cookies Definition
//
//----------------------------------------------------------------------------

// Cookies is an Array of cookie objects.
type Cookies struct {
	eventMap map[string]func(...interface{}) interface{}

	Cookies      []Cookie
	GolectronCtx *Golectron
	SessionID    int //the Session instance these cookies belong to
}

// Cookie stores cookie data
type Cookie struct {
	Name           string  `json:"name"`           //The name of the cookie.
	Value          string  `json:"value"`          //The value of the cookie.
	Domain         string  `json:"domain"`         //The domain of the cookie.
	HostOnly       string  `json:"hostOnly"`       //Whether the cookie is a host-only cookie.
	Path           string  `json:"path"`           //The path of the cookie.
	Secure         bool    `json:"secure"`         //Whether the cookie is marked as secure.
	HttpOnly       bool    `json:"httpOnly"`       //Whether the cookie is marked as HTTP only.
	Session        bool    `json:"session"`        //Whether the cookie is a session cookie or a persistent cookie with an expiration date.
	ExpirationDate float64 `json:"expirationDate"` // The expiration date of the cookie as the number of seconds since the UNIX epoch. Not provided for session cookies.
}

// CookieFilter is employed in Cookies.Get() func to narrow the search
type CookieFilter struct {
	Url     string `json:"url"`     //Retrieves cookies which are associated with url. Empty implies retrieving cookies of all urls.
	Name    string `json:"name"`    //Filters cookies by name.
	Domain  string `json:"domain"`  //Retrieves cookies whose domains match or are subdomains of domains
	Path    string `json:"path"`    //Retrieves cookies whose path matches path.
	Secure  bool   `json:"secure"`  //Filters cookies by their Secure property.
	Session bool   `json:"session"` //Filters out session or persistent cookies.
}

// Get Sends a request to get all cookies matching details
//, callback will be called with callback(error, cookies) on complete.
func (c *Cookies) Get(cf CookieFilter) (*Cookies, error) {
	data := struct {
		CookiesFilter CookieFilter `json:"cookiesFilter"`
		Cookies       []Cookie     `json:"cookies"`
		Err           string       `json:"error"`
	}{CookiesFilter: cf}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Cookies.Get:", err)
	} else {
		fmt.Println("marshalling Cookies.Get worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(c.SessionID)+"/cookies/get", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call Cookies.Get function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session Cookies.Get response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Cookies.Get:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Cookies.Get:", resp.StatusCode, data.Err)
		return nil, errors.New("ERROR in Cookies.Get:" + data.Err)
	}
	cooks := c
	cooks.Cookies = data.Cookies
	return cooks, nil
}

// Set sets a cookie for the url with Cookie details
func (c *Cookies) Set(url string, cf Cookie) {
	data := struct {
		Url    string `json:"url"`
		Cookie Cookie `json:"cookie"`
		Err    string `json:"error"`
	}{Url: url, Cookie: cf}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Cookies.Set:", err)
	} else {
		fmt.Println("marshalling Cookies.Set worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(c.SessionID)+"/cookies/set", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call Cookies.Set function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session Cookies.Set response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Cookies.Set:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Cookies.Set:", resp.StatusCode, data.Err)
	}
}

// Remove Removes the cookies matching url and name
// url string - The URL associated with the cookie.
// name string - The name of cookie to remove.
func (c *Cookies) Remove(url, name string) {
	data := struct {
		Url  string `json:"url"`
		Name string `json:"name"`
		Err  string `json:"error"`
	}{Url: url, Name: name}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Cookies.Remove:", err)
	} else {
		fmt.Println("marshalling Cookies.Remove worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(c.SessionID)+"/cookies/remove", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := c.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call Cookies.Remove function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session Cookies.Remove response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Cookies.Remove:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Cookies.Remove:", resp.StatusCode, data.Err)
	}
}

//----------------------------------------------------------------------------
// Cookies Instance Events
//
//----------------------------------------------------------------------------

/* Cookies has the following list of events:
changed called with event Event, cookie Object - The cookie that was changed, cause string - The cause of the change with one of the following values: explicit - The cookie was changed directly by a consumer’s action. overwrite - The cookie was automatically removed due to an insert operation that overwrote it. expired - The cookie was automatically removed as it expired. evicted - The cookie was automatically evicted during garbage collection. expired-overwrite - The cookie was overwritten with an already-expired expiration date., removed Boolean - true if the cookie was removed, false otherwise.
*/

// On registers eventName event with fn func
func (c *Cookies) On(eventName string, fn func(...interface{}) interface{}) {
	resp, err := http.Get("http://localhost/Session/" + strconv.Itoa(c.SessionID) + "/cookies/listen4event/" + eventName)
	if err != nil {
		fmt.Println("ERROR Cookies failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR Cookies failed to Unmarshal Listen4Event error message:", err)
		}
		fmt.Println("ERROR Cookies couldn't listen to event", eventName, ":", data.Error)
	} else {
		c.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (c *Cookies) RemoveAllListeners(eventName string) {
	resp, err := http.Get("http://localhost/Session/" + strconv.Itoa(c.SessionID) + "/cookies/unlisten4event/" + eventName)
	if err != nil {
		fmt.Println("ERROR Cookies failed to call Unlisten4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR Cookies failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR Cookies couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(c.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (c *Cookies) Emit(eventName string, data ...interface{}) interface{} {
	if val, ok := c.eventMap[eventName]; ok {
		res := val(data)
		return res
	}
	fmt.Println("ERROR: Received event in Cookies is out of sync with Electron. EventName=", eventName)
	return nil
}

// EventList returns an array of event names currently registered.
func (c *Cookies) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(c.eventMap))
	for key := range c.eventMap {
		evList = append(evList, key)
	}
	return evList
}

//----------------------------------------------------------------------------
// WebRequest Definition
// Check http://electron.atom.io/docs/api/session/ for details
//----------------------------------------------------------------------------

// WebRequest is accessed by Session.GetWebRequest()
type WebRequest struct {
	eventMap map[string]func(...interface{}) interface{}

	GolectronCtx *Golectron
	SessionID    int //the Session instance these cookies belong to
}

// WebRequestDetails stores WebRequest data
type WebRequestDetails struct {
	Id             int               `json:"id"`
	Url            string            `json:"url"`
	Method         string            `json:"method"`
	ResourceType   string            `json:"resourceType"`
	Timestamp      float64           `json:"timestamp"`
	UploadData     []UploadDataStore `json:"uploadData"`
	StatusLine     string            `json:"statusLine"`
	StatusCode     int               `json:"statusCode"`
	RequestHeaders interface{}       `json:"requestHeaders"`
	FromCache      bool              `json:"fromCache"`
	RedirectURL    string            `json:"redirectURL"`
	Ip             string            `json:"ip"`
	Error          string            `json:"error"`
}

// UploadDataStore stores WebRequest's uploadData
type UploadDataStore struct {
	Bytes    []byte `json:"bytes"`
	File     string `json:"file"`
	BlobUUID string `json:"blobUUID"`
}

//----------------------------------------------------------------------------
// WebRequest Instance Methods
//
//----------------------------------------------------------------------------

// OnBeforeRequest sets the listener that will be called with listener(details, callback) when a request is about to occur.
// The filter object an Array of URL patterns that will be used to filter out the requests that do not match the URL patterns. If the filter is empty then all requests will be matched.
func (w *WebRequest) OnBeforeRequest(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onBeforeRequest"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnBeforeRequest:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnBeforeRequest worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onBeforeRequest", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnBeforeRequest function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnBeforeRequest response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnBeforeRequest:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnBeforeRequest:", resp.StatusCode, data.Err)
	}
}

// OnBeforeSendHeaders - The listener will be called with listener(details, callback) before sending an HTTP request, once the request headers are available. This may occur after a TCP connection is made to the server, but before any http data is sent.
func (w *WebRequest) OnBeforeSendHeaders(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onBeforeSendHeaders"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnBeforeSendHeaders:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnBeforeSendHeaders worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onBeforeSendHeaders", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnBeforeSendHeaders function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnBeforeSendHeaders response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnBeforeSendHeaders:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnBeforeSendHeaders:", resp.StatusCode, data.Err)
	}
}

// OnSendHeaders - The listener will be called with listener(details) just before a request is going to be sent to the server, modifications of previous onBeforeSendHeaders response are visible by the time this listener is fired.
func (w *WebRequest) OnSendHeaders(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onSendHeaders"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnSendHeaders:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnSendHeaders worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onSendHeaders", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnSendHeaders function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnSendHeaders response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnSendHeaders:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnSendHeaders:", resp.StatusCode, data.Err)
	}
}

// OnHeadersReceived - The listener will be called with listener(details, callback) when HTTP response headers of a request have been received.
func (w *WebRequest) OnHeadersReceived(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onHeadersReceived"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnHeadersReceived:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnHeadersReceived worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onHeadersReceived", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnHeadersReceived function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnHeadersReceived response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnHeadersReceived:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnHeadersReceived:", resp.StatusCode, data.Err)
	}
}

// OnResponseStarted - The listener will be called with listener(details) when first byte of the response body is received. For HTTP requests, this means that the status line and response headers are available.
func (w *WebRequest) OnResponseStarted(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onResponseStarted"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnResponseStarted:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnResponseStarted worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onResponseStarted", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnResponseStarted function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnResponseStarted response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnResponseStarted:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnResponseStarted:", resp.StatusCode, data.Err)
	}
}

// OnBeforeRedirect - The listener will be called with listener(details) when a server initiated redirect is about to occur.
func (w *WebRequest) OnBeforeRedirect(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onBeforeRedirect"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnBeforeRedirect:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnBeforeRedirect worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onBeforeRedirect", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnBeforeRedirect function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnBeforeRedirect response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnBeforeRedirect:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnBeforeRedirect:", resp.StatusCode, data.Err)
	}
}

// OnCompleted - The listener will be called with listener(details) when a request is completed.
func (w *WebRequest) OnCompleted(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onCompleted"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnCompleted:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnCompleted worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onCompleted", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnCompleted function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnCompleted response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnCompleted:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnCompleted:", resp.StatusCode, data.Err)
	}
}
func (w *WebRequest) OnErrorOccurred(filter []string, listener func(...interface{}) interface{}) {
	w.eventMap["onErrorOccurred"] = listener
	data := struct {
		Urls []string `json:"filter"`
		Err  string   `json:"error"`
	}{Urls: filter}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling WebRequest.OnErrorOccurred:", err)
	} else {
		fmt.Println("marshalling WebRequest.OnErrorOccurred worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(w.SessionID)+"/webRequest/onErrorOccurred", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := w.GolectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Session failed to call WebRequest.OnErrorOccurred function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Session WebRequest.OnErrorOccurred response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling WebRequest.OnErrorOccurred:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in WebRequest.OnErrorOccurred:", resp.StatusCode, data.Err)
	}
}

//----------------------------------------------------------------------------
// WebRequest Instance Events
//
//----------------------------------------------------------------------------

// On registers eventName event with fn func
func (w *WebRequest) On(eventName string, fn func(...interface{}) interface{}) {
	resp, err := http.Get("http://localhost/Session/" + strconv.Itoa(w.SessionID) + "/webRequest/listen4event/" + eventName)
	if err != nil {
		fmt.Println("ERROR WebRequest failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR WebRequest failed to Unmarshal Listen4Event error message:", err)
		}
		fmt.Println("ERROR WebRequest couldn't listen to event", eventName, ":", data.Error)
	} else {
		w.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (w *WebRequest) RemoveAllListeners(eventName string) {
	resp, err := http.Get("http://localhost/Session/" + strconv.Itoa(w.SessionID) + "/webRequest/unlisten4event/" + eventName)
	if err != nil {
		fmt.Println("ERROR WebRequest failed to call Unlisten4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR WebRequest failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR WebRequest couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(w.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (w *WebRequest) Emit(eventName string, data ...interface{}) interface{} {
	if val, ok := w.eventMap[eventName]; ok {
		res := val(data)
		return res
	}
	fmt.Println("ERROR: Received event in WebRequest is out of sync with Electron. EventName=", eventName)
	return nil
}

// EventList returns an array of event names currently registered.
func (w *WebRequest) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(w.eventMap))
	for key := range w.eventMap {
		evList = append(evList, key)
	}
	return evList
}
