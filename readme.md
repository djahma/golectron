# Concept
Golectron is a Go wrapper to [Electron](https://electronjs.org/docs) API. Pretty experimental, you can write a Go back end, using Web/Javascript technologies for the front end. I'm not planning to further this project, feel free to fork.

# Installation

Import the *golectron* package in your project, and make sure an electron install is available in either of the following paths:
1. your project root Folder
2. system wide in the `$PATH`

*electron* folder must begin by "electron" to be found.  
Golectron depends on [`github.com/julienschmidt/httprouter`](https://github.com/julienschmidt/httprouter) to work.

Also install the following node modules in *golectron/golectronApp*:
- less
- express
- body-parser

## Installation recap
```bash
$ mkdir myapp
$ cd myapp
$ git clone https://github.com/djahma/golectron
$ go get github.com/julienschmidt/httprouter
```
Find your electron release at https://github.com/electron/electron/releases  
For me it's version 1.7.5 for Linux.
```shell
$ wget https://github.com/electron/electron/releases/download/v1.7.5/electron-v1.7.5-linux-x64.zip
$ mkdir golectron/electron
$ unzip electron-v1.7.5-linux-x64.zip -d golectron/electron/
$ rm electron-v1.7.5-linux-x64.zip
$ cd golectron/golectronApp/
$ npm install less
$ npm install express
$ npm install body-parser

```
Voilà! you may now return in `myapp` folder and paste the below usage code into your `myapp.go` file, run, and see it load your electron app in go:-)

# Usage

```Go
package main

import (
	"fmt"
	"golectron"
)

func main() {
	quitCh := make(chan int, 1)
	// create golectron context
	g, err := golectron.NewGolectron()
	if err != nil {
		fmt.Println("golectron ERROR:", err)
		return
	}
	// generate standard window options
	winOpts := g.NewBrowserWindowOptions()
	// create a standard window
	w := g.NewBrowserWindow(winOpts)
	// attach listener on "close" event
	w.On("close", false, func(...interface{}) {
		fmt.Println("window closed, exiting...")
		quitCh <- 1
	})
	w.LoadURL("https://www.github.com", "", "", "")
	// block execution till window is closed
	<-quitCh
}
```
More details on golectron inner workings in the [docs](./doc/index.md)
