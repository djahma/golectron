/* This file cover the globalShortcut API
It's made of a 'router' that route every absolute routes starting by '/globalShortcut' to the corresponding function
*/
const {app, globalShortcut} = require('electron')
// var GlobalShortcut = require('electron').globalShortcut
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	router.use(bodyParser.raw({ type: 'application/javascript' }));
	//routes come here
	router.post("/register", register)
	router.post("/isRegistered", isRegistered)
	router.post("/unregister", unregister)
	router.get("/unregisterAll", unregisterAll)

	return router
}

// register Registers a global shortcut of accelerator. An event is sent to /event/globalShortcut with the accelerator pressed when the registered shortcut is pressed by the user.
// When the accelerator is already taken by other applications, this call will silently fail. This behavior is intended by operating systems, since they don’t want applications to fight for global shortcuts.
function register(req, res, next){
	console.log("router_GlobalShortcut.register function",req.body.accelerator)
	var accel = ""
	if(req.body.accelerator && req.body.accelerator!="")accel=req.body.accelerator;
	console.log("router_GlobalShortcut.register function, accel=",accel)
	if (accel!="") {
		globalShortcut.register(accel,function(){
			console.log("sending globalShortcut",accel);
			glec.send2go('/event/globalShortcut', "POST", {accelerator: accel})
		})
		res.status(200).end()
	}else {
		res.status(400).json({ error: "router_GlobalShortcut.register invalid accelerator:"+accel}).end()
	}
}

// Returns Boolean - Whether this application has registered accelerator.
// When the accelerator is already taken by other applications, this call will still return false. This behavior is intended by operating systems, since they don’t want applications to fight for global shortcuts.
function isRegistered(req, res, next){
	console.log("router_GlobalShortcut.isRegistered function")
	var accel = ""
	if(req.body.accelerator && req.body.accelerator!="")accel=req.body.accelerator;
	if (accel!="") {
		isR = globalShortcut.isRegistered(accel)
		res.status(200).json({'isRegistered':isR}).end()
	}else{
		res.status(400).json({ error: "router_GlobalShortcut.isRegistered invalid accelerator:"+accel}).end()
	}
}

// Unregisters the global shortcut of accelerator.
function unregister(req, res, next){
	console.log("router_GlobalShortcut.unregister function")
	var accel = ""
	if(req.body.accelerator && req.body.accelerator!="")accel=req.body.accelerator;
	if (accel!="") {
		globalShortcut.unregister(accel)
		res.status(200).end()
	}else {
		res.status(400).json({ error: "router_GlobalShortcut.unregister invalid accelerator:"+accel}).end()
	}
}

// Unregisters all of the global shortcuts.
function unregisterAll(req, res, next){
	console.log("router_GlobalShortcut.unregisterAll function")
	globalShortcut.unregisterAll()
	res.status(200).end()
}
