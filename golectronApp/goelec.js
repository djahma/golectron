var http = require('http');
var bodyParser = require('body-parser')
var Menu = require('electron').Menu
var MenuItem = require('electron').MenuItem
var BrowserWindow = require('electron').BrowserWindow

module.exports = Goelec

function Goelec(listeningPort, goPort) {
	this.listeningPort = listeningPort
	this.goPort = goPort
	this.windows = []
	this.menus = []
	this.menuitems = []
	this.sessions = []
	this.test2 = "you found me"
}

Goelec.prototype.sayhello=function(){
	return "I'm saying hello"
}
Goelec.prototype.__addWindow = function(win){
	var that = this
	this.windows.push(win)
	win.on("close",function(event){
		that.__removeWindow(win.id)
	})
}
Goelec.prototype.__removeWindow = function(id){
	w = this.getWindowById(id)
	if(w!==null){
		ind = this.windows.indexOf(w)
		this.windows.splice(ind, 1)
	}
}
// Adds the menu object to the registry and assign it a unique id
Goelec.prototype.__addMenu = function(menu){
	x = this.menus.length
	for(menu.id=0; menu.id<=x; menu.id++){
		isTaken = false
		for(i=0; i<x; i++){
			if (this.menus[i].id==menu.id) {
				isTaken = true
				break
			}
		}
		if (!isTaken) {
			break
		}
	}
	this.menus.push(menu)
}
Goelec.prototype.__removeMenu = function(id){
	m = this.getMenuById(id)
	if(m!==null){
		ind = this.menus.indexOf(m)
		this.menus.splice(ind, 1)
	}
}
// Adds the menuitem object to the registry and assign it a unique id
Goelec.prototype.__addMenuItem = function(menuitem){
	x = this.menuitems.length
	for(menuitem.menuitemID=0; menuitem.menuitemID<=x; menuitem.menuitemID++){
		isTaken = false
		for(i=0; i<x; i++){
			if (this.menuitems[i].menuitemID==menuitem.menuitemID) {
				isTaken = true
				break
			}
		}
		if (!isTaken) {
			break
		}
	}
	this.menuitems.push(menuitem)
}
Goelec.prototype.__removeMenuItem = function(id){
	mi = this.getMenuItemById(id)
	if(mi!==null){
		ind = this.menuitems.indexOf(mi)
		this.menuitems.splice(ind, 1)
	}
}
// Adds the menuitem object to the registry and assign it a unique id
Goelec.prototype.__addSession = function(sess){
	x = this.sessions.length
	for(sess.session_id=0; sess.session_id<=x; sess.session_id++){
		isTaken = false
		for(i=0; i<x; i++){
			if (this.sessions[i].session_id==sess.session_id) {
				isTaken = true
				break
			}
		}
		if (!isTaken) {
			break
		}
	}
	this.sessions.push(sess)
}
Goelec.prototype.__removeSession = function(id){
	s = this.getSessionById(id)
	if(s!==null){
		ind = this.sessions.indexOf(s)
		this.sessions.splice(ind, 1)
	}
}

Goelec.prototype.send2go=function(path, method, dataObj, respMssg){
	console.log("send2Go is called with data=", dataObj,require('util').format("%j",dataObj));
	var querystring = require('querystring');
	var options = {
		protocol: "http:",
		hostname:"localhost",
		port: this.goPort,
		method: method,
		path : path,
		timeout: 10
	}
	if (method=="GET") {
		var urlsearch = querystring.stringify(dataObj)
		options.path += "?" + urlsearch.toString()
	}
	var req = http.request(options)
	if (method=="POST") {
		postData = require('util').format("%j",dataObj)
		req.headers={
			"Content-Type": "application/json",
			'Content-Length': Buffer.byteLength(postData)
		}
		req.write(postData)
	}
	req.end()
	if (respMssg) {
		req.on('response', respMssg(mssg))
	}
	console.log("send2go after respMssg", req);
	// req.connection.unref()
	return
}

Goelec.prototype.getWindowById = function(idNumber){
	for (var i = this.windows.length - 1; i >= 0; i--) {
		if(this.windows[i].id==idNumber){
			return this.windows[i]
		}
	}
	return null
}
Goelec.prototype.getMenuById = function(idNumber){
	for (var i = this.menus.length - 1; i >= 0; i--) {
		if(this.menus[i].id==idNumber){
			return this.menus[i]
		}
	}
	return null
}
Goelec.prototype.getMenuItemById = function(idNumber){
	for (var i = this.menuitems.length - 1; i >= 0; i--) {
		if(this.menuitems[i].menuitemID==idNumber){
			return this.menuitems[i]
		}
	}
	return null
}
Goelec.prototype.getSessionById = function(idNumber){
	for (var i = this.sessions.length - 1; i >= 0; i--) {
		if(this.sessions[i].session_id==idNumber){
			return this.sessions[i]
		}
	}
	return null
}

Goelec.prototype.destroyMenu = function (id) {
	ids = {}
	ids.menus=[]
	ids.menuitems=[]
	m = this.getMenuById(id)
	for (var i = 0; i < m.items.length; i++) {
		ind = this.destroyMenuItem(m.items[i].id)
		ids.menus.push(ind.menus)
		ids.menuitems.push(ind.menuitems)
	}
	ids.menus.push(id)
	this.__removeMenu(id)
	return ids
};
Goelec.prototype.destroyMenuItem = function (id) {
	ids = {}
	ids.menus=[]
	ids.menuitems=[]
	mi = this.getMenuItemById(id)
	if (typeof mi.submenu !== 'undefined') {
		ind = this.destroyMenu(mi.items[i].id)
		ids.menus.push(ind.menus)
		ids.menuitems.push(ind.menuitems)
	}
	ids.menuitems.push(id)
	this.__removeMenuItem(id)
	return ids
};
