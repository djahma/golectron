/* This file cover the Session API
It's made of a 'router' that route every absolute routes starting by '/Session' to the corresponding function
*/
const {session} = require('electron')
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	// /Session routes come here
	router.get("/:idnum", testID)
	router.post("/fromPartition", fromPartition)
	router.get("/getDefaultSession", getDefaultSession)
	router.get("/:idnum/getCookies", getCookies)
	router.get("/:idnum/getWebRequest", getWebRequest)
	router.get("/:idnum/getCacheSize", getCacheSize)
	router.get("/:idnum/clearCache", clearCache)
	router.post("/:idnum/clearStorageData", clearStorageData)
	router.get("/:idnum/flushStorageData", flushStorageData)
	router.post("/:idnum/setProxy", setProxy)
	router.post("/:idnum/resolveProxy", resolveProxy)
	router.post("/:idnum/setDownloadPath", setDownloadPath)
	router.post("/:idnum/enableNetworkEmulation", enableNetworkEmulation)
	router.get("/:idnum/disableNetworkEmulation", disableNetworkEmulation)
	router.get("/:idnum/setCertificateVerifyProc", setCertificateVerifyProc)
	router.get("/:idnum/setPermissionRequestHandler", setPermissionRequestHandler)
	router.get("/:idnum/clearHostResolverCache", clearHostResolverCache)
	router.post("/:idnum/allowNTLMCredentialsForDomains", allowNTLMCredentialsForDomains)
	router.post("/:idnum/setUserAgent", setUserAgent)
	router.get("/:idnum/getUserAgent", getUserAgent)
	router.post("/:idnum/getBlobData", getBlobData)
	// Cookies routes come here
	router.post("/:idnum/cookies/get", cookiesGet)
	router.post("/:idnum/cookies/set", cookiesSet)
	router.post("/:idnum/cookies/remove", cookiesRemove)
	// WebRequest routes come here
	router.post("/:idnum/webRequest/onBeforeRequest", webRequestOnBeforeRequest)
	router.post("/:idnum/webRequest/onBeforeSendHeaders", webRequestOnBeforeSendHeaders)
	router.post("/:idnum/webRequest/onSendHeaders", webRequestOnSendHeaders)
	router.post("/:idnum/webRequest/onHeadersReceived", webRequestOnHeadersReceived)
	router.post("/:idnum/webRequest/onResponseStarted", webRequestOnResponseStarted)
	router.post("/:idnum/webRequest/onBeforeRedirect", webRequestOnBeforeRedirect)
	router.post("/:idnum/webRequest/onCompleted", webRequestOnCompleted)
	router.post("/:idnum/webRequest/onErrorOccurred", webRequestOnErrorOccurred)
	// Protocol routes come here
	router.post("/:idnum/protocol/registerStandardSchemes", protocolRegisterStandardSchemes)
	router.post("/:idnum/protocol/registerServiceWorkerSchemes", protocolRegisterServiceWorkerSchemes)
	router.post("/:idnum/protocol/registerFileProtocol", protocolRegisterFileProtocol)
	router.post("/:idnum/protocol/registerBufferProtocol", protocolRegisterBufferProtocol)
	router.post("/:idnum/protocol/registerStringProtocol", protocolRegisterStringProtocol)
	router.post("/:idnum/protocol/registerHttpProtocol", protocolRegisterHttpProtocol)
	router.post("/:idnum/protocol/unregisterProtocol", protocolUnregisterProtocol)
	router.post("/:idnum/protocol/isProtocolHandled", protocolIsProtocolHandled)



	// // Event routes
	router.get("/:idnum/listen4event/:eventName", listen4event)
	router.get("/:idnum/unlisten4event/:eventName", unlisten4event)
	router.get("/:idnum/cookies/listen4event/:eventName", listen4event_cookies)
	router.get("/:idnum/cookies/unlisten4event/:eventName", unlisten4event_cookies)
	return router
}

function testID(req, res, next){
	console.log("router_Session.testID function. id=", req.params.idnum)
	var w = glec.getSessionById(req.params.idnum)
	if(w!==null){
		console.log("router found session id", w.session_id)
	}else{
		console.log("router couldnt find session id", req.params.idnum)
	}
	res.status(200).end()
}

// SessionFromPartition returns existing *Session or create a new one
function fromPartition(req, res, next){
	console.log("router_Session.fromPartition function")
	s = session.fromPartition(req.body.partion, req.body.cache)
	if (!s.session_id) {
		glec.__addSession(s)
	}
	res.status(200).json({session:s}).end()
}
// getDefaultSession returns the default session object of the app.
function getDefaultSession(req, res, next){
	console.log("router_Session.getDefaultSession function")
	s = session.defaultSession
	if (!s.session_id) {
		glec.__addSession(s)
	}
	res.status(200).json({sess:s}).end()
}

//----------------------------------------------------------------------------
// WebContents Instance Methods
//
//----------------------------------------------------------------------------

// getCookies returns a Cookies object.
function getCookies(req, res, next){
	console.log("router_Session.getCookies function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		res.status(200).json({cookies:s.cookies})
	}else{
		res.status(400).json({ error: "router_Session.getCookies couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
// GetWebRequest returns the WebRequest property of s *Session, in sync between Go and JS
function getWebRequest(req, res, next){
	console.log("router_Session.getWebRequest function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		res.status(200).json({webRequest:s.webRequest})
	}else{
		res.status(400).json({ error: "router_Session.getWebRequest couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
// GetCacheSize Returns the session’s current cache size.
function getCacheSize(req, res, next){
	console.log("router_Session.getCacheSize function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.getCacheSize(function(size){
			res.status(200).json({cacheSize:size})
		})
	}else{
		res.status(400).json({ error: "router_Session.getCacheSize couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
// ClearCache Clears the session’s HTTP cache.
function clearCache(req, res, next){
	console.log("router_Session.clearCache function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.clearCache(function(){
			res.status(200).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.clearCache couldnt find Session id:"+req.params.idnum }).end()
	}
}
// ClearStorageData Clears the data of web storages.
function clearStorageData(req, res, next){
	console.log("router_Session.clearStorageData function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		parameters = {}
		if(req.body.origin){parameters.origin=req.body.origin}
		if(req.body.storages){parameters.storages=req.body.storages}
		if(req.body.quotas){parameters.quotas=req.body.quotas}
		s.clearStorageData(parameters, function(){
			res.status(200).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.clearStorageData couldnt find Session id:"+req.params.idnum }).end()
	}
}
// FlushStorageData Writes any unwritten DOMStorage data to disk.
function flushStorageData(req, res, next){
	console.log("router_Session.flushStorageData function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.flushStorageData()
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.flushStorageData couldnt find Session id:"+req.params.idnum }).end()
	}
}
// SetProxy Sets the proxy settings.
function setProxy(req, res, next){
	console.log("router_Session.setProxy function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		parameters = {}
		if(req.body.pacScript){parameters.pacScript=req.body.pacScript}
		if(req.body.proxyRules){parameters.proxyRules=req.body.proxyRules}
		if(req.body.proxyBypassRules){parameters.proxyBypassRules=req.body.proxyBypassRules}
		s.setProxy(parameters, function(){
			res.status(200).json({}).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.setProxy couldnt find Session id:"+req.params.idnum }).end()
	}
}
// ResolveProxy Resolves the proxy information for url.
function resolveProxy(req, res, next){
	console.log("router_Session.resolveProxy function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		u = url.parse(req.body.url)
		s.resolveProxy(u, function(p){
			res.status(200).json({proxy:p}).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.resolveProxy couldnt find Session id:"+req.params.idnum }).end()
	}
}
// SetDownloadPath Sets download saving directory. By default, the download directory will be the Downloads under the respective app folder.
function setDownloadPath(req, res, next){
	console.log("router_Session.setDownloadPath function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.setDownloadPath(req.body.path)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.setDownloadPath couldnt find Session id:"+req.params.idnum }).end()
	}
}
// ClearStorageData Clears the data of web storages.
function enableNetworkEmulation(req, res, next){
	console.log("router_Session.enableNetworkEmulation function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		parameters = {}
		if(req.body.offline){parameters.offline=req.body.offline}
		if(req.body.latency){parameters.latency=req.body.latency}
		if(req.body.downloadThroughput){parameters.downloadThroughput=req.body.downloadThroughput}
		if(req.body.uploadThroughput){parameters.uploadThroughput=req.body.uploadThroughput}
		s.enableNetworkEmulation(parameters)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.enableNetworkEmulation couldnt find Session id:"+req.params.idnum }).end()
	}
}
// DisableNetworkEmulation Disables any network emulation already active for the session. Resets to the original network configuration.
function disableNetworkEmulation(req, res, next){
	console.log("router_Session.disableNetworkEmulation function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.disableNetworkEmulation()
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.disableNetworkEmulation couldnt find Session id:"+req.params.idnum }).end()
	}
}
// SetCertificateVerifyProc Sets the certificate verify proc for session, the proc will be called with proc(hostname, certificate, callback) whenever a server certificate verification is requested. Calling callback(true) accepts the certificate, calling callback(false) rejects it.
function setCertificateVerifyProc(req, res, next){
	console.log("router_Session.setCertificateVerifyProc function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.setCertificateVerifyProc(function(hn, cert, cb){
			glec.send2go('/event/session/'+req.params.idnum+'/certificateVerifyProc', "POST", {hostname: hn, certificate: cert}, function(mssg){
				mssg.on('data', function(chunk){
					cb(chunk)
				})
			})
		})
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.setCertificateVerifyProc couldnt find Session id:"+req.params.idnum }).end()
	}
}
// SetPermissionRequestHandler Sets the handler which can be used to respond to permission requests for the session. Calling callback(true) will allow the permission and callback(false) will reject it.
function setPermissionRequestHandler(req, res, next){
	console.log("router_Session.setPermissionRequestHandler function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.setPermissionRequestHandler(function(wc, perm, cb){
			glec.send2go('/event/session/'+req.params.idnum+'/permissionRequest', "POST", {id: wc.id, permission: perm}, function(mssg){
				mssg.on('data', function(chunk){
					cb(chunk)
				})
			})
		})
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.setPermissionRequestHandler couldnt find Session id:"+req.params.idnum }).end()
	}
}
// ClearHostResolverCache Clears the host resolver cache.
function clearHostResolverCache(req, res, next){
	console.log("router_Session.clearHostResolverCache function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.clearHostResolverCache(function(){
			res.status(200).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.clearHostResolverCache couldnt find Session id:"+req.params.idnum }).end()
	}
}
// AllowNTLMCredentialsForDomains Dynamically sets whether to always send credentials for HTTP NTLM or Negotiate authentication.
function allowNTLMCredentialsForDomains(req, res, next){
	console.log("router_Session.allowNTLMCredentialsForDomains function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.allowNTLMCredentialsForDomains(req.body.domains)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.allowNTLMCredentialsForDomains couldnt find Session id:"+req.params.idnum }).end()
	}
}
function setUserAgent(req, res, next){
	console.log("router_Session.setUserAgent function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		if (req.body.acceptedLanguages) {
			s.setUserAgent(req.body.userAgent, req.body.acceptedLanguages)
		}else{
			s.setUserAgent(req.body.userAgent)
		}
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.setUserAgent couldnt find Session id:"+req.params.idnum }).end()
	}
}
// GetUserAgent Returns the user agent for this session.
function getUserAgent(req, res, next){
	console.log("router_Session.getUserAgent function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		u =s.getUserAgent()
		res.status(200).json({userAgent:u}).end()
	}else{
		res.status(400).json({ error: "router_Session.getUserAgent couldnt find Session id:"+req.params.idnum }).end()
	}
}
// GetUserAgent Returns the user agent for this session.
function getBlobData(req, res, next){
	console.log("router_Session.getUserAgent function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.getBlobData(function(result){
			res.status(200).json({blob:result}).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.getUserAgent couldnt find Session id:"+req.params.idnum }).end()
	}
}
//----------------------------------------------------------------------------
// Cookies Methods
//
//----------------------------------------------------------------------------

// cookiesGet Sends a request to get all cookies matching details
function cookiesGet(req, res, next){
	console.log("router_Session.cookiesGet function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.cookies.get(req.body.cookiesFilter, function(err,cs){
			if (error) {
				res.status(400).json({error:err}).end()
			}else{
				res.status(200).json({cookies: cs}).end()
			}
		})
	}else{
		res.status(400).json({ error: "router_Session.cookiesGet couldnt find Session id:"+req.params.idnum }).end()
	}
}
// cookiesSet sets a cookie for the url with Cookie details
function cookiesSet(req, res, next){
	console.log("router_Session.cookiesSet function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		details = req.body.cookie
		details.url = req.body.url
		s.cookies.set(details, function(err){
			if(err){
				res.status(400).json({error: err}).end()
			}else{
				res.status(200).end()
			}
		})
	}else{
		res.status(400).json({ error: "router_Session.cookiesSet couldnt find Session id:"+req.params.idnum }).end()
	}
}
// Remove Removes the cookies matching url and name
function cookiesRemove(req, res, next){
	console.log("router_Session.cookiesRemove function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		details = req.body.cookie
		details.url = req.body.url
		s.cookies.remove(req.body.url, req.body.name, function(){
			res.status(200).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.cookiesRemove couldnt find Session id:"+req.params.idnum }).end()
	}
}

//----------------------------------------------------------------------------
// webRequest Methods
//
//----------------------------------------------------------------------------

// onBeforeRequest sets the listener that will be called with listener(details, callback) when a request is about to occur.
function webRequestOnBeforeRequest(req, res, next){
	console.log("router_Session.webRequestOnBeforeRequest function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onBeforeRequest({urls:req.body.filter}, function(d, cb){
			glec.send2go('/event/webRequest/'+w.session_id+"/onBeforeRequest", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnBeforeRequest couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnBeforeSendHeaders(req, res, next){
	console.log("router_Session.webRequestOnBeforeSendHeaders function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onBeforeSendHeaders({urls:req.body.filter}, function(d, cb){
			glec.send2go('/event/webRequest/'+w.session_id+"/onBeforeSendHeaders", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnBeforeSendHeaders couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnSendHeaders(req, res, next){
	console.log("router_Session.webRequestOnSendHeaders function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onSendHeaders({urls:req.body.filter}, function(d){
			glec.send2go('/event/webRequest/'+w.session_id+"/onSendHeaders", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				// response = {cancel:false, redirectURL:""}
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnSendHeaders couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnHeadersReceived(req, res, next){
	console.log("router_Session.webRequestOnHeadersReceived function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onHeadersReceived({urls:req.body.filter}, function(d, cb){
			glec.send2go('/event/webRequest/'+w.session_id+"/onHeadersReceived", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnHeadersReceived couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnResponseStarted(req, res, next){
	console.log("router_Session.webRequestOnResponseStarted function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onResponseStarted({urls:req.body.filter}, function(d){
			glec.send2go('/event/webRequest/'+w.session_id+"/onResponseStarted", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				// cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnResponseStarted couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnBeforeRedirect(req, res, next){
	console.log("router_Session.webRequestOnBeforeRedirect function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onBeforeRedirect({urls:req.body.filter}, function(d){
			glec.send2go('/event/webRequest/'+w.session_id+"/onBeforeRedirect", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				// cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnBeforeRedirect couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnCompleted(req, res, next){
	console.log("router_Session.webRequestOnCompleted function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onCompleted({urls:req.body.filter}, function(d){
			glec.send2go('/event/webRequest/'+w.session_id+"/onCompleted", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				// cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnCompleted couldnt find Session id:"+req.params.idnum }).end()
	}
}
function webRequestOnErrorOccurred(req, res, next){
	console.log("router_Session.webRequestOnErrorOccurred function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.webRequest.onErrorOccurred({urls:req.body.filter}, function(d){
			glec.send2go('/event/webRequest/'+w.session_id+"/onErrorOccurred", "POST", {details: d},function(inMssg){
				// TODO: implement callback and response obj properly, from answer contained in inMssg
				response = {cancel:false, redirectURL:""}
				// cb(response)
			})
		})
	}else{
		res.status(400).json({ error: "router_Session.webRequestOnErrorOccurred couldnt find Session id:"+req.params.idnum }).end()
	}
}
//----------------------------------------------------------------------------
// Protocol Methods
//
//----------------------------------------------------------------------------

// RegisterStandardSchemes Registering a scheme as standard will allow access to files through the FileSystem API. Otherwise the renderer will throw a security error for the scheme.
// Note: This method can only be used before the ready event of the app module gets emitted.
function protocolRegisterStandardSchemes(req, res, next){
	console.log("router_Session.protocolRegisterStandardSchemes function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerStandardSchemes(req.body.schemes)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterStandardSchemes couldnt find Session id:"+req.params.idnum }).end()
	}
}
// RegisterServiceWorkerSchemes - Custom schemes to be registered to handle service workers.
function protocolRegisterServiceWorkerSchemes(req, res, next){
	console.log("router_Session.protocolRegisterServiceWorkerSchemes function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerServiceWorkerSchemes(req.body.schemes)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterServiceWorkerSchemes couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolRegisterFileProtocol(req, res, next){
	console.log("router_Session.protocolRegisterFileProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerFileProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {path:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterFileProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolRegisterBufferProtocol(req, res, next){
	console.log("router_Session.protocolRegisterBufferProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerBufferProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterBufferProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolRegisterStringProtocol(req, res, next){
	console.log("router_Session.protocolRegisterStringProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerStringProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterStringProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolRegisterHttpProtocol(req, res, next){
	console.log("router_Session.protocolRegisterHttpProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.registerHttpProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolRegisterHttpProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
// UnregisterProtocol Unregisters the custom protocol of scheme.
function protocolUnregisterProtocol(req, res, next){
	console.log("router_Session.protocolUnregisterProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.unregisterProtocol(req.body.scheme)
		res.status(200).end()
	}else{
		res.status(400).json({ error: "router_Session.protocolUnregisterProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
// IsProtocolHandled indicates whether there is already a handler for scheme.
function protocolIsProtocolHandled(req, res, next){
	console.log("router_Session.protocolIsProtocolHandled function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.isProtocolHandled(req.body.scheme, function(result){
			res.status(200).json({resp: result}).end()
		})
	}else{
		res.status(400).json({ error: "router_Session.protocolIsProtocolHandled couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolInterceptFileProtocol(req, res, next){
	console.log("router_Session.protocolInterceptFileProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.interceptFileProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {path:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolInterceptFileProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolInterceptBufferProtocol(req, res, next){
	console.log("router_Session.protocolInterceptBufferProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.interceptFileProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolInterceptBufferProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolInterceptStringProtocol(req, res, next){
	console.log("router_Session.protocolInterceptStringProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.interceptStringProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolInterceptStringProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}
function protocolInterceptHttpProtocol(req, res, next){
	console.log("router_Session.protocolInterceptHttpProtocol function. id=", req.params.idnum)
	var s = glec.getSessionById(req.params.idnum)
	if(s!==null){
		console.log("router found Session id", s.session_id)
		s.protocol.interceptHttpProtocol(
			req.body.scheme,
			function(r, cb){
				glec.send2go(
					'/event/protocol/'+s.session_id+"/scheme/"+req.body.scheme,
					"POST",
					{request: r},
					function(inMssg){
					// TODO: implement callback and response obj properly, from answer contained in inMssg
					response = {data:"", mimeType:"", charset:""}
					cb(response)

				})
			},
			function(result){
				if(result===null){ res.status(200).end()}
				else res.status(400).json({error: result}).end()
			})
	}else{
		res.status(400).json({ error: "router_Session.protocolInterceptHttpProtocol couldnt find Session id:"+req.params.idnum }).end()
	}
}






//----------------------------------------------------------------------------
// Session Instance Events
//
//----------------------------------------------------------------------------

// Registers an eventName to be listenned for
function listen4event(req, res, next){
	console.log("router_Session.listen4event function. id=", req.params.idnum)
	var w = glec.getSessionById(req.params.idnum)
	if(w!==null){
		if (req.params.eventName == "will-download") {
			w.on(req.params.eventName, function(e,i, wb){
				glec.send2go('/event/session/'+w.session_id+"/"+req.params.eventName, "POST", {event: e, item: i, webContents: wb})
			})
		}else{
			w.on(req.params.eventName, function(e){
				glec.send2go('/event/Session/'+w.session_id+"/"+req.params.eventName, "POST", {event: e})
			})
		}
		res.status(200)
	}else{
		res.status(400).json({ error: "router_Session.listen4event couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
function unlisten4event(req, res, next){
	console.log("router_Session.unlisten4event function. id=", req.params.idnum)
	var w = glec.getSessionById(req.params.idnum)
	if(w!==null){
		w.removeAllListeners(req.params.eventName)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_Session.unlisten4event couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}

//----------------------------------------------------------------------------
// Cookies Instance Event
//
//----------------------------------------------------------------------------

// Registers an eventName to be listenned for
function listen4event_cookies(req, res, next){
	console.log("router_Session.listen4event_cookies function. id=", req.params.idnum)
	var w = glec.getSessionById(req.params.idnum)
	if(w!==null){
		if (req.params.eventName == "changed") {
			w.on(req.params.eventName, function(e,cook, ca, rm){
				glec.send2go('/event/cookies/'+w.session_id+"/"+req.params.eventName, "POST", {event: e, cookie: cook, cause: ca, removed:rm})
			})
		}else{
			w.on(req.params.eventName, function(e){
				glec.send2go('/event/cookies/'+w.session_id+"/"+req.params.eventName, "POST", {event: e})
			})
		}
		res.status(200)
	}else{
		res.status(400).json({ error: "router_Session.listen4event_cookies couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
function unlisten4event_cookies(req, res, next){
	console.log("router_Session.listen4event_cookies function. id=", req.params.idnum)
	var w = glec.getSessionById(req.params.idnum)
	if(w!==null){
		w.cookies.removeAllListeners(req.params.eventName)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_Session.listen4event_cookies couldnt find Session id:"+req.params.idnum })
	}
	res.end()
}
