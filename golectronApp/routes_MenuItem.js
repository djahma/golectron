/* This file cover the MenuItem API
It's made of a 'router' that route every absolute routes starting by '/MenuItem' to the corresponding function
*/
var MenuItem = require('electron').MenuItem
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	router.use(bodyParser.raw({ type: 'application/javascript' }));
	//routes come here
	router.post("/buildFromTemplate", buildFromTemplate)
	router.get("/:idnum", testID)
	router.get("/:idnum/destroy", destroy)
	router.get("/:idnum/getMenuItemOptions", getMenuItemOptions)
	router.post("/:idnum/setMenuItemOptions", setMenuItemOptions)
	router.get("/:idnum/getSubmenu", getSubmenu)

	return router
}

function testID(req, res, next){
	console.log("router_MenuItem.testID function. id=", req.params.idnum)
	var m = glec.getMenuItemById(req.params.idnum)
	if(m!==null){
		console.log("router found MenuItem id", m.id)
		res.status(200)
	}else{
		console.log("router couldnt find MenuItem id", req.params.idnum)
		res.status(404)
	}
	res.end()
}
// Destroy the menuitem, and its submenu too.
function destroy(req, res, next){
	console.log("router_MenuItem.destroy function. id=", req.params.idnum)
	var response = glec.destroyMenuItem(req.params.idnum)
	res.status(200).json(response)
	res.end()
}
function getMenuItemOptions(req, res, next){
	console.log("router_MenuItem.getMenuItemOptions function. id=", req.params.idnum)
	var mi = glec.getMenuItemById(req.params.idnum)
	if (mi) {
		opts={}
		if(mi.role && mi.role!="")opts.role=mi.role;
		if(mi.type && mi.type!="")opts.type=mi.type;
		if(mi.label && mi.label!="")opts.label=mi.label;
		if(mi.sublabel && mi.sublabel!="")opts.sublabel=mi.sublabel;
		if(mi.enabled)opts.enabled=mi.enabled;
		if(mi.visible)opts.visible=mi.visible;
		if(mi.checked)opts.checked=mi.checked;
		if(mi.id)opts.id=mi.id;
		if(mi.position)opts.position=mi.position;
		res.status(200).json({options:opts})
	}else{
		err="menuitem "+req.params.idnum+" not found"
		res.status(400).json({error:err})
	}
	res.end()
}
function setMenuItemOptions(req, res, next){
	console.log("router_MenuItem.setMenuItemOptions function. id=", req.params.idnum)
	var mi = glec.getMenuItemById(req.params.idnum)
	if (mi) {
		if(req.body.role && req.body.role!="")mi.role=req.body.role;
		else delete mi.role;
		mi.type=req.body.type
		mi.label=req.body.label
		mi.sublabel=req.body.sublabel
		mi.enabled=req.body.enabled
		mi.visible=req.body.visible
		mi.checked=req.body.checked
		mi.id=req.body.id
		mi.position=req.body.positions
		res.status(200)
	}else{
		err="menuitem "+req.params.idnum+" not found"
		res.status(400).json({error:err})
	}
	res.end()
}
function getSubmenu(req, res, next){
	console.log("router_MenuItem.getSubmenu function. id=", req.params.idnum)
	var mi = glec.getMenuItemById(req.params.idnum)
	if (mi) {
		if (mi.submenu) {
			m = glec.getMenuById(mi.submenu.id)
			if (m!=null) {
				res.status(200).json({submenuID:m.id})
			}else{
				res.status(400).json({error:"submenu out of sync"})
			}
		}else{
			res.status(400).json({error:"menuitem does not sport any submenu"})
		}
	}else{
		err="menuitem "+req.params.idnum+" not found"
		res.status(400).json({error:err})
	}
	res.end()
}

//MenuItem.buildFromTemplate(template)
function buildFromTemplate(req, res, next){
	console.log("router_MenuItem.buildFromTemplate function")
	try {
		console.log(eval(req.body.toString()))
		m = new MenuItem(eval(req.body.toString()))
		glec.__addMenuItem(m)
		res.status(200).json({'menuitemID':m.menuitemID})
	} catch (e) {
		res.status(400).json({error:e.message})
	} finally {
		res.end()
	}
}
