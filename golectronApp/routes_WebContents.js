/* This file cover the WebContents API
It's made of a 'router' that route every absolute routes starting by '/WebContents' to the corresponding function
*/

const {webContents} = require('electron')
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	// /WebContents routes come here
	router.get("/:idnum", testID)
	router.get("/getAllWebContents", getAllWebContents)
	router.get("/getFocusedWebContents", getFocusedWebContents)
	router.get("/fromId", fromId)
	router.get("/:idnum/getSession", getSession)
	router.post("/:idnum/loadURL", loadURL)
	router.post("/:idnum/downloadURL", downloadURL)
	router.get("/:idnum/getURL", getURL)
	router.get("/:idnum/getTitle", getTitle)
	router.get("/:idnum/isDestroyed", isDestroyed)
	router.get("/:idnum/isFocused", isFocused)
	router.get("/:idnum/isLoading", isLoading)
	router.get("/:idnum/isLoadingMainFrame", isLoadingMainFrame)
	router.get("/:idnum/isWaitingForResponse", isWaitingForResponse)
	router.get("/:idnum/stop", stop)
	router.get("/:idnum/reload", reload)
	router.get("/:idnum/reloadIgnoringCache", reloadIgnoringCache)
	router.get("/:idnum/canGoBack", canGoBack)
	router.get("/:idnum/canGoForward", canGoForward)
	router.post("/:idnum/canGoToOffset", canGoToOffset)
	router.get("/:idnum/clearHistory", clearHistory)
	router.get("/:idnum/goBack", goBack)
	router.get("/:idnum/goForward", goForward)
	router.post("/:idnum/goToIndex", goToIndex)
	router.post("/:idnum/goToOffset", goToOffset)
	router.get("/:idnum/isCrashed", isCrashed)
	router.post("/:idnum/setUserAgent", setUserAgent)
	router.get("/:idnum/getUserAgent", getUserAgent)
	router.post("/:idnum/insertCSS", insertCSS)
	router.post("/:idnum/executeJavaScript", executeJavaScript)
	router.post("/:idnum/setAudioMuted", setAudioMuted)
	router.get("/:idnum/isAudioMuted", isAudioMuted)
	router.post("/:idnum/setZoomFactor", setZoomFactor)
	router.get("/:idnum/getZoomFactor", getZoomFactor)
	router.post("/:idnum/setZoomLevel", setZoomLevel)
	router.get("/:idnum/getZoomLevel", getZoomLevel)
	router.post("/:idnum/setZoomLevelLimits", setZoomLevelLimits)
	router.get("/:idnum/undo", undo)
	router.get("/:idnum/redo", redo)
	router.get("/:idnum/cut", cut)
	router.get("/:idnum/copy", copy)
	router.post("/:idnum/copyImageAt", copyImageAt)
	router.get("/:idnum/paste", paste)
	router.get("/:idnum/pasteAndMatchStyle", pasteAndMatchStyle)
	router.get("/:idnum/delete", dilite)
	router.get("/:idnum/selectAll", selectAll)
	router.get("/:idnum/unselect", unselect)
	router.post("/:idnum/replace", replace)
	router.post("/:idnum/replaceMisspelling", replaceMisspelling)
	router.post("/:idnum/insertText", insertText)
	router.post("/:idnum/findInPage", findInPage)
	router.post("/:idnum/stopFindInPage", stopFindInPage)
	router.get("/:idnum/hasServiceWorker", hasServiceWorker)
	router.get("/:idnum/unregisterServiceWorker", unregisterServiceWorker)
	router.post("/:idnum/print", print)
	router.post("/:idnum/printToPDF", printToPDF)
	router.post("/:idnum/addWorkSpace", addWorkSpace)
	router.post("/:idnum/removeWorkSpace", removeWorkSpace)
	router.post("/:idnum/openDevTools", openDevTools)
	router.get("/:idnum/closeDevTools", closeDevTools)
	router.get("/:idnum/isDevToolsOpened", isDevToolsOpened)
	router.get("/:idnum/isDevToolsFocused", isDevToolsFocused)
	router.get("/:idnum/toggleDevTools", toggleDevTools)
	router.post("/:idnum/inspectElement", inspectElement)
	router.get("/:idnum/inspectServiceWorker", inspectServiceWorker)
	router.post("/:idnum/send", send)
	router.post("/:idnum/enableDeviceEmulation", enableDeviceEmulation)
	router.get("/:idnum/disableDeviceEmulation", disableDeviceEmulation)
	router.post("/:idnum/sendInputEvent", sendInputEvent)
	router.post("/:idnum/savePage", savePage)
	router.get("/:idnum/showDefinitionForSelection", showDefinitionForSelection)
	router.get("/:idnum/isOffscreen", isOffscreen)
	router.get("/:idnum/startPainting", startPainting)
	router.get("/:idnum/stopPainting", stopPainting)
	router.get("/:idnum/isPainting", isPainting)
	router.post("/:idnum/setFrameRate", setFrameRate)
	router.get("/:idnum/getFrameRate", getFrameRate)
	router.get("/:idnum/invalidate", invalidate)

	// // Event routes
	router.get("/:idnum/listen4event/:eventName", listen4event)
	router.get("/:idnum/unlisten4event/:eventName", unlisten4event)
	return router
}

function testID(req, res, next){
	console.log("router_WebContents.testID function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found webContents id", w.id)
	}else{
		console.log("router couldnt find webContents id", req.params.idnum)
	}
	res.status(200).end()
}

//----------------------------------------------------------------
// Static Methods
//
//----------------------------------------------------------------

// getAllWebContents returns an array of all WebContents instances. This will contain web contents for all windows, webviews, opened devtools, and devtools extension background pages.
function getAllWebContents(req, res, next){
	console.log("router_WebContents.getAllWebContents function")
	c = webContents.getAllWebContents()
	res.status(200).json({webContents:c}).end()
}
// WebContentsGetFocusedWebContents returns the web contents that is focused in this application, otherwise returns null.
function getFocusedWebContents(req, res, next){
	console.log("router_WebContents.getFocusedWebContents function")
	c = webContents.getFocusedWebContents()
	res.status(200).json({webContents:c}).end()
}
//fromId returns a WebContents instance with the given ID.
function fromId(req, res, next){
	console.log("router_WebContents.fromId function")
	c = webContents.fromId(req.body.id)
	res.status(200).json({webContents:c}).end()
}

//----------------------------------------------------------------
// WebContents Instance Methods
//
//----------------------------------------------------------------

// GetSession retrieves the session used by this webContents, ensuring Go and Electron sides remain in sync.
function getSession(req, res, next){
	console.log("router_WebContents.getSession function. id=", req.params.idnum)
	var i = parseInt(req.params.idnum , 10 )
	var w = webContents.fromId(i)
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		s = w.session
		if (!s.session_id) {
			glec.__addSession(s)
		}
		res.status(200).json({session: s})
	}else{
		res.status(400).json({ error: "router_WebContents.getSession couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// LoadURL loads the url in the window.
function loadURL(req, res, next){
	console.log("router_WebContents.loadURL function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w)
		console.log("router body=", req.body)
		// w.loadURL('http://www.google.fr')
		w.loadURL(req.body.url, req.body)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.loadURL couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// DownloadURL initiates a download of the resource at url without navigating. The will-download event of session will be triggered.
function downloadURL(req, res, next){
	console.log("router_WebContents.downloadURL function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		console.log("router body=", req.body)
		w.downloadURL(req.body.url)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.downloadURL couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GetURL returns string - The URL of the current web page.
function getURL(req, res, next){
	console.log("router_WebContents.getURL function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.getURL()
		res.status(200).json({url:u})
	}else{
		res.status(400).json({ error: "router_WebContents.getURL couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GetTitle returns string - The title of the current web page.
function getTitle(req, res, next){
	console.log("router_WebContents.getTitle function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.getTitle()
		res.status(200).json({title:u})
	}else{
		res.status(400).json({ error: "router_WebContents.getTitle couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsDestroyed returns bool - Whether the web page is destroyed.
function isDestroyed(req, res, next){
	console.log("router_WebContents.isDestroyed function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isDestroyed()
		res.status(200).json({isDestroyed:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isDestroyed couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsFocused returns bool - Whether the web page is focused.
function isFocused(req, res, next){
	console.log("router_WebContents.isFocused function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isFocused()
		res.status(200).json({isFocused:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isFocused couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsLoading returns bool - Whether web page is still loading resources.
function isLoading(req, res, next){
	console.log("router_WebContents.isLoading function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isLoading()
		res.status(200).json({isLoading:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isLoading couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsLoadingMainFrame returns bool - Whether the main frame (and not just iframes or frames within it) is still loading.
function isLoadingMainFrame(req, res, next){
	console.log("router_WebContents.isLoadingMainFrame function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isLoadingMainFrame()
		res.status(200).json({isLoadingMainFrame:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isLoadingMainFrame couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsLoadingMainFrame returns bool - Whether the main frame (and not just iframes or frames within it) is still loading.
function isWaitingForResponse(req, res, next){
	console.log("router_WebContents.isWaitingForResponse function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isWaitingForResponse()
		res.status(200).json({isWaitingForResponse:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isWaitingForResponse couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Stop stops any pending navigation.
function stop(req, res, next){
	console.log("router_WebContents.stop function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.stop()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.stop couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Reload the current web page.
function reload(req, res, next){
	console.log("router_WebContents.reload function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.reload()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.reload couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// ReloadIgnoringCache Reloads current page and ignores cache.
function reloadIgnoringCache(req, res, next){
	console.log("router_WebContents.reloadIgnoringCache function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.reloadIgnoringCache()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.reloadIgnoringCache couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// CanGoBack Returns Boolean - Whether the browser can go back to previous web page.
function canGoBack(req, res, next){
	console.log("router_WebContents.canGoBack function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		c = w.canGoBack()
		res.status(200).json({canGoBack:c})
	}else{
		res.status(400).json({ error: "router_WebContents.canGoBack couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// CanGoForward Returns bool - Whether the browser can go forward to next web page.
function canGoForward(req, res, next){
	console.log("router_WebContents.canGoForward function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		c = w.canGoForward()
		res.status(200).json({canGoForward:c})
	}else{
		res.status(400).json({ error: "router_WebContents.canGoForward couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// CanGoForward Returns bool - Whether the browser can go forward to next web page.
function canGoToOffset(req, res, next){
	console.log("router_WebContents.canGoToOffset function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		c = w.canGoToOffset(req.body.offset)
		res.status(200).json({canGoToOffset:c})
	}else{
		res.status(400).json({ error: "router_WebContents.canGoToOffset couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// ClearHistory Clears the navigation history.
function clearHistory(req, res, next){
	console.log("router_WebContents.clearHistory function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.clearHistory()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.clearHistory couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GoBack Makes the browser go back a web page.
function goBack(req, res, next){
	console.log("router_WebContents.goBack function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.goBack()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.goBack couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GoForward Makes the browser go forward a web page.
function goForward(req, res, next){
	console.log("router_WebContents.goForward function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.goForward()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.goForward couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GoToIndex Navigates browser to the specified absolute web page index.
function goToIndex(req, res, next){
	console.log("router_WebContents.goToIndex function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.goToIndex(req.body.index)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.goToIndex couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GoToOffset Navigates to the specified offset from the “current entry”.
function goToOffset(req, res, next){
	console.log("router_WebContents.goToOffset function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.goToOffset(req.body.offset)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.goToOffset couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsCrashed Returns Boolean - Whether the renderer process has crashed.
function isCrashed(req, res, next){
	console.log("router_WebContents.isCrashed function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		c = w.isCrashed()
		res.status(200).json({isCrashed:c})
	}else{
		res.status(400).json({ error: "router_WebContents.isCrashed couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SetUserAgent Overrides the user agent for this web page.
function setUserAgent(req, res, next){
	console.log("router_WebContents.setUserAgent function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setUserAgent(req.body.userAgent)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setUserAgent couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// UserAgent Overrides the user agent for this web page.
function getUserAgent(req, res, next){
	console.log("router_WebContents.getUserAgent function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u=w.getUserAgent()
		res.status(200).json({ userAgent: u })
	}else{
		res.status(400).json({ error: "router_WebContents.getUserAgent couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// InsertCSS Injects CSS into the current web page.
function insertCSS(req, res, next){
	console.log("router_WebContents.insertCSS function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.insertCSS(req.body.css)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.insertCSS couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// ExecuteJavaScript Evaluates code in page.
function executeJavaScript(req, res, next){
	console.log("router_WebContents.executeJavaScript function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.executeJavaScript(req.body.code, req.body.userGesture)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.executeJavaScript couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SetAudioMuted Mute the audio on the current web page.
function setAudioMuted(req, res, next){
	console.log("router_WebContents.setAudioMuted function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setAudioMuted(req.body.muted)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setAudioMuted couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsAudioMuted Returns Boolean - Whether this page has been muted.
function isAudioMuted(req, res, next){
	console.log("router_WebContents.isAudioMuted function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isAudioMuted()
		res.status(200).json({ isAudioMuted: u })
	}else{
		res.status(400).json({ error: "router_WebContents.isAudioMuted couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SetZoomFactor Changes the zoom factor to the specified factor. Zoom factor is zoom percent divided by 100, so 300% = 3.0.
function setZoomFactor(req, res, next){
	console.log("router_WebContents.setZoomFactor function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setZoomFactor(req.body.factor)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setZoomFactor couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GetZoomFactor Returns float64 - Get current zoom factor
function getZoomFactor(req, res, next){
	console.log("router_WebContents.getZoomFactor function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.getZoomFactor(function(f){
			res.status(200).json({factor: f}).end()
		})
	}else{
		res.status(400).json({ error: "router_WebContents.getZoomFactor couldnt find WebContents id:"+req.params.idnum }).end()
	}
}
// SetZoomLevel Changes the zoom level to the specified level. The original size is 0 and each increment above or below represents zooming 20% larger or smaller to default limits of 300% and 50% of original size, respectively.
function setZoomLevel(req, res, next){
	console.log("router_WebContents.setZoomLevel function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setZoomLevel(req.body.level)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setZoomLevel couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// GetZoomLevel Sends a request to get current zoom level
function getZoomLevel(req, res, next){
	console.log("router_WebContents.getZoomLevel function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.getZoomLevel(function(f){
			res.status(200).json({level: f}).end()
		})
	}else{
		res.status(400).json({ error: "router_WebContents.getZoomLevel couldnt find WebContents id:"+req.params.idnum }).end()
	}
}
// SetZoomLevelLimits Sets the maximum and minimum zoom level.
function setZoomLevelLimits(req, res, next){
	console.log("router_WebContents.setZoomLevelLimits function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setZoomLevelLimits(req.body.minimumLevel,req.body.maximumLevel)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setZoomLevelLimits couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Undo Executes the editing command undo in web page.
function undo(req, res, next){
	console.log("router_WebContents.undo function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.undo()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.undo couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Redo Executes the editing command redo in web page.
function redo(req, res, next){
	console.log("router_WebContents.redo function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.redo()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.redo couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Cut Executes the editing command cut in web page.
function cut(req, res, next){
	console.log("router_WebContents.cut function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.cut()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.cut couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Copy Executes the editing command copy in web page.
function copy(req, res, next){
	console.log("router_WebContents.copy function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.copy()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.copy couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// CopyImageAt Copy the image at the given position to the clipboard.
function copyImageAt(req, res, next){
	console.log("router_WebContents.copyImageAt function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.copyImageAt(req.body.x,req.body.y)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.copyImageAt couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Paste Executes the editing command paste in web page.
function paste(req, res, next){
	console.log("router_WebContents.paste function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.paste()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.paste couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// PasteAndMatchStyle Executes the editing command pasteAndMatchStyle in web page.
function pasteAndMatchStyle(req, res, next){
	console.log("router_WebContents.pasteAndMatchStyle function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.pasteAndMatchStyle()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.pasteAndMatchStyle couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// dilite Executes the editing command delete in web page.
function dilite(req, res, next){
	console.log("router_WebContents.delete function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.delete()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.delete couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SelectAll Executes the editing command selectAll in web page.
function selectAll(req, res, next){
	console.log("router_WebContents.selectAll function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.selectAll()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.selectAll couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Unselect Executes the editing command unselect in web page.
function unselect(req, res, next){
	console.log("router_WebContents.unselect function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.unselectunselect()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.unselect couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Replace Executes the editing command replace in web page.
function replace(req, res, next){
	console.log("router_WebContents.replace function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.replace(req.body.text)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.replace couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// ReplaceMisspelling Executes the editing command replaceMisspelling in web page.
function replaceMisspelling(req, res, next){
	console.log("router_WebContents.replaceMisspelling function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.replaceMisspelling(req.body.text)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.replaceMisspelling couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// InsertText Inserts text to the focused element.
function insertText(req, res, next){
	console.log("router_WebContents.insertText function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.insertText(req.body.text)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.insertText couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// FindInPage Starts a request to find all matches for the text in the web page and returns an Integer representing the request id used for the request. The result of the request can be obtained by subscribing to found-in-page event.
function findInPage(req, res, next){
	console.log("router_WebContents.findInPage function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		r = w.findInPage(req.body.text, req.body.options)
		res.status(200).json({ rid: r })
	}else{
		res.status(400).json({ error: "router_WebContents.findInPage couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// StopFindInPage Stops any findInPage request for the webContents with the provided action.
function stopFindInPage(req, res, next){
	console.log("router_WebContents.stopFindInPage function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.stopFindInPage(req.body.action)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.stopFindInPage couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// HasServiceWorker Checks if any ServiceWorker is registered and returns a boolean as response to callback.
function hasServiceWorker(req, res, next){
	console.log("router_WebContents.hasServiceWorker function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.hasServiceWorker(function(result){
			res.status(200).json({hasServiceWorker:result}).end()
		})
	}else{
		res.status(400).json({ error: "router_WebContents.hasServiceWorker couldnt find WebContents id:"+req.params.idnum }).end()
	}
}
// UnregisterServiceWorker Unregisters any ServiceWorker if present and returns a boolean as response to callback when the JS promise is fulfilled or false when the JS promise is rejected.
function unregisterServiceWorker(req, res, next){
	console.log("router_WebContents.unregisterServiceWorker function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.unregisterServiceWorker(function(result){
			res.status(200).json({success:result}).end()
		})
	}else{
		res.status(400).json({ error: "router_WebContents.unregisterServiceWorker couldnt find WebContents id:"+req.params.idnum }).end()
	}
}
// Print Prints window’s web page. When silent is set to true, Electron will pick up system’s default printer and default settings for printing.
function print(req, res, next){
	console.log("router_WebContents.print function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.print({
			silent: req.body.silent,
			printBackground: req.body.printBackground
		})
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.print couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// PrintToPDF Prints window’s web page as PDF with Chromium’s preview printing custom settings.
function printToPDF(req, res, next){
	console.log("router_WebContents.printToPDF function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.printToPDF({
			marginsType: req.body.marginsType,
			pageSize: req.body.pageSize,
			printBackground: req.body.printBackground,
			printSelectionOnly: req.body.printSelectionOnly,
			landscape: req.body.landscape
		},
		function(error, data){
			if (error) {
				res.status(400).json({ error: "router_WebContents.printToPDF error: "+error }).end()
			}else{
				res.status(200).json({d: data.toString('hex')}).end()
			}
		})
	}else{
		res.status(400).json({ error: "router_WebContents.printToPDF couldnt find WebContents id:"+req.params.idnum }).end()
	}
}
// AddWorkSpace Adds the specified path to DevTools workspace. Must be used after DevTools creation:
function addWorkSpace(req, res, next){
	console.log("router_WebContents.addWorkSpace function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.addWorkSpace(req.body.path)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.addWorkSpace couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// RemoveWorkSpace Removes the specified path from DevTools workspace.
function removeWorkSpace(req, res, next){
	console.log("router_WebContents.removeWorkSpace function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.removeWorkSpace(req.body.path)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.removeWorkSpace couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// OpenDevTools Opens the devtools.
function openDevTools(req, res, next){
	console.log("router_WebContents.openDevTools function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.openDevTools({mode: req.body.mode})
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.openDevTools couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// CloseDevTools Closes the devtools.
function closeDevTools(req, res, next){
	console.log("router_WebContents.closeDevTools function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.closeDevTools()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.closeDevTools couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsDevToolsOpened Returns whether the devtools is opened.
function isDevToolsOpened(req, res, next){
	console.log("router_WebContents.isDevToolsOpened function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isDevToolsOpened()
		res.status(200).json({isDevToolsOpened:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isDevToolsOpened couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// IsDevToolsFocused Returns Whether the devtools view is focused
function isDevToolsFocused(req, res, next){
	console.log("router_WebContents.isDevToolsFocused function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		u = w.isDevToolsFocused()
		res.status(200).json({isDevToolsFocused:u})
	}else{
		res.status(400).json({ error: "router_WebContents.isDevToolsFocused couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// ToggleDevTools Toggles the developer tools.
function toggleDevTools(req, res, next){
	console.log("router_WebContents.toggleDevTools function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.toggleDevTools()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.toggleDevTools couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// InspectElement Starts inspecting element at position (x, y).
function inspectElement(req, res, next){
	console.log("router_WebContents.inspectElement function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.inspectElement(req.body.x,req.body.y)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.inspectElement couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// InspectServiceWorker Opens the developer tools for the service worker context.
function inspectServiceWorker(req, res, next){
	console.log("router_WebContents.inspectServiceWorker function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.inspectServiceWorker()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.inspectServiceWorker couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Send an asynchronous message to renderer process via channel, you can also send arbitrary arguments. Arguments will be serialized in JSON internally and hence no functions or prototype chain will be included.
function send(req, res, next){
	console.log("router_WebContents.send function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.send(req.body.channel, req.body.args)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.send couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// enableDeviceEmulation Enable device emulation with the given parameters.
function enableDeviceEmulation(req, res, next){
	console.log("router_WebContents.enableDeviceEmulation function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		parameters = {
			screenPosition: req.body.screenPosition,
			screenSize: {
				width: req.body.screen.width,
				height: req.body.screen.height
			},
			viewPosition:{
				x: req.body.screen.x,
				y: req.body.screen.y
			},
			deviceScaleFactor: req.body.deviceScaleFactor,
			viewSize: {
				width: req.body.view.width,
				height: req.body.view.height
			},
			fitToView: req.body.fitToView,
			offset:{
				x: req.body.view.x/100,
				y: req.body.view.y/100,
			},
			scale: req.body.scale
		}
		w.enableDeviceEmulation(parameters)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.enableDeviceEmulation couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// DisableDeviceEmulation Disable device emulation enabled by webContents.enableDeviceEmulation.
function disableDeviceEmulation(req, res, next){
	console.log("router_WebContents.disableDeviceEmulation function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.disableDeviceEmulation()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.disableDeviceEmulation couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SendInputEvent Sends an input event to the page.
function sendInputEvent(req, res, next){
	console.log("router_WebContents.sendInputEvent function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		parameters = {
			eventName: req.body.eventName,
			modifiers: req.body.modifiers
		}
		if (req.body.keyCode) {
			parameters.keyCode = req.body.keyCode
		}
		if (req.body.mouseEvent) {
			for (var prop in req.body.mouseEvent) {
				if (req.body.mouseEvent.hasOwnProperty(prop)) {
					parameters[prop]=req.body.mouseEvent[prop]
				}
			}
		}
		if (req.body.mouseWheelEvent) {
			for (var prop in req.body.mouseWheelEvent) {
				if (req.body.mouseWheelEvent.hasOwnProperty(prop)) {
					parameters[prop]=req.body.mouseWheelEvent[prop]
				}
			}
		}
		w.sendInputEvent(parameters)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.sendInputEvent couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// SavePage saves webContents' content to file.
function savePage(req, res, next){
	console.log("router_WebContents.savePage function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		result = w.savePage(req.body.fullPath, req.body.saveType, function(err){
			if (!err) res.status(200).end()
			else res.status(400).json({ error: "router_WebContents.savePage failed:"+err }).end()
		})
		if (!result) {
			res.status(400).json({error: "router_WebContents.savePage could not initialise savePage"}).end()
		}
	}else{
		res.status(400).json({ error: "router_WebContents.savePage couldnt find savePage id:"+req.params.idnum }).end()
	}
}
// ShowDefinitionForSelection macOS - Shows pop-up dictionary that searches the selected word on the page.
function showDefinitionForSelection(req, res, next){
	console.log("router_WebContents.showDefinitionForSelection function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.showDefinitionForSelection()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.showDefinitionForSelection couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// IsOffscreen () bool - Indicates whether offscreen rendering is enabled.
function isOffscreen(req, res, next){
	console.log("router_WebContents.isOffscreen function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		result = w.isOffscreen()
		res.status(200).json({isOffscreen:result})
	}else{
		res.status(400).json({ error: "router_WebContents.isOffscreen couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// StartPainting If offscreen rendering is enabled and not painting
function startPainting(req, res, next){
	console.log("router_WebContents.startPainting function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.startPainting()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.startPainting couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// StopPainting If offscreen rendering is enabled and painting, stop painting.
function stopPainting(req, res, next){
	console.log("router_WebContents.stopPainting function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.stopPainting()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.stopPainting couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// IsPainting bool - If offscreen rendering is enabled returns whether it is currently painting.
function isPainting(req, res, next){
	console.log("router_WebContents.isPainting function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		result = w.isPainting()
		res.status(200).json({isPainting: result})
	}else{
		res.status(400).json({ error: "router_WebContents.isPainting couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// SetFrameRate (fps int) If offscreen rendering is enabled sets the frame rate to the specified number. Only values between 1 and 60 are accepted.
function setFrameRate(req, res, next){
	console.log("router_WebContents.setFrameRate function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.setFrameRate(req.body.fps)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.setFrameRate couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
// GetFrameRate () int - If offscreen rendering is enabled returns the current frame rate.
function getFrameRate(req, res, next){
	console.log("router_WebContents.getFrameRate function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		result = w.getFrameRate()
		res.status(200).json({fps: result})
	}else{
		res.status(400).json({ error: "router_WebContents.getFrameRate couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}
function invalidate(req, res, next){
	console.log("router_WebContents.invalidate function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		console.log("router found WebContents id", w.id)
		w.invalidate()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.invalidate couldnt find savePage id:"+req.params.idnum })
	}
	res.end()
}



// Registers an eventName to be listenned for
function listen4event(req, res, next){
	console.log("router_WebContents.listen4event function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		if (req.params.eventName == "did-fail-load") {
			w.on(req.params.eventName, function(e, ec, ed, vURL, imf){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, errorCode: ec, errorDescription: ed, validateURL: vURL, isMainFrame: imf})
			})
		}else if (req.params.eventName == "did-frame-finish-load") {
			w.on(req.params.eventName, function(e, imf){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, isMainFrame: imf})
			})
		}else if (req.params.eventName == "did-get-response-details") {
			w.on(req.params.eventName, function(e, s, nURL, oURL, httpRC, rm, r, h, rT){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, status: s, newURL: nURL, originalURL: oURL, httpResponseCode: httpRC, requestMethod: rm, referrer:r, headers:h, resourceType:rT})
			})
		}else if (req.params.eventName == "did-get-redirect-request") {
			w.on(req.params.eventName, function(e, oURL, nURL, imf, httpRC, rm, r, h){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, oldURL: oURL, newURL: nURL, isMainFrame:imf, httpResponseCode:httpRC, requestMethod: rm, referrer:r, headers:h})
			})
		}else if (req.params.eventName == "dom-ready") {
			w.on(req.params.eventName, function(e){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e})
			})
		}else if (req.params.eventName == "page-favicon-updated") {
			w.on(req.params.eventName, function(e, f){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, favicons:f})
			})
		}else if (req.params.eventName == "new-window") {
			w.on(req.params.eventName, function(e, u, fn, d, o, af){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e,url:u, frameName:fn, disposition:d, options:o, additionalFeatures:af})
			})
		}else if (req.params.eventName == "will-navigate") {
			w.on(req.params.eventName, function(e, u){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u})
			})
		}else if (req.params.eventName == "did-navigate") {
			w.on(req.params.eventName, function(e, u){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u})
			})
		}else if (req.params.eventName == "did-navigate-in-page") {
			w.on(req.params.eventName, function(e, u, imf){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u, isMainFrame:imf})
			})
		}else if (req.params.eventName == "crashed") {
			w.on(req.params.eventName, function(e, k){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, killed:k})
			})
		}else if (req.params.eventName == "plugin-crashed") {
			w.on(req.params.eventName, function(e, n, v){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, name:n,version:v})
			})
		}else if (req.params.eventName == "certificate-error") {
			w.on(req.params.eventName, function(e, u,err, cert, cb){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u, error:err, certificate:cert})
			})
		}else if (req.params.eventName == "select-client-certificate") {
			w.on(req.params.eventName, function(e, u, cert, cb){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u, error:err, certificate:cert})
			})
		}else if (req.params.eventName == "login") {
			w.on(req.params.eventName, function(e, r, auth, cb){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, request:r, authInfo:auth})
			})
		}else if (req.params.eventName == "found-in-page") {
			w.on(req.params.eventName, function(e, r){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, result:r})
			})
		}else if (req.params.eventName == "update-target-url") {
			w.on(req.params.eventName, function(e, u){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, url:u})
			})
		}else if (req.params.eventName == "cursor-changed") {
			w.on(req.params.eventName, function(e, t, i, s, si, ho){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, type:t, image:i, scale:s, size:si, hotspot:ho})
			})
		}else if (req.params.eventName == "context-menu") {
			w.on(req.params.eventName, function(e, p){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, params:p})
			})
		}else if (req.params.eventName == "select-bluetooth-device") {
			w.on(req.params.eventName, function(e, d, cb){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, devices:d})
			})
		}else if (req.params.eventName == "paint") {
			w.on(req.params.eventName, function(e, d, img){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e, dirtyRect:d, image:img})
			})
		}else{
			w.on(req.params.eventName, function(e){
				glec.send2go('/event/WebContents/'+w.id+"/"+req.params.eventName, "POST", {event: e})
			})
		}
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.listen4event couldnt find WebContents id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array of eventName currently listenned for
function unlisten4event(req, res, next){
	console.log("router_WebContents.unlisten4event function. id=", req.params.idnum)
	var w = webContents.fromId(parseInt(req.params.idnum))
	if(w && w!==null){
		w.removeAllListeners(req.params.eventName)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_WebContents.unlisten4event couldnt find webContents id:"+req.params.idnum })
	}
	res.end()
}
