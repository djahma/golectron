const {app} = require('electron')
// const {webContents} = require('electron')
// const {BrowserWindow} = require('electron')

var express = require('express')
console.log("main.js est demarre: ",app.getAppPath(), __dirname);

const goelecjs = require('./goelec.js')
goelec = new goelecjs(process.argv[3], process.argv[2])
var xprss = express()
xprss.locals.glec = goelec
xprss.locals.elec = app
console.log("before route");
require('./routes.js').initRoutes(xprss)
console.log("after route");

console.log("ELECTRON. listening port=", goelec.listeningPort, " go port=",goelec.goPort, goelec.sayhello())


app.on("ready", function(){

   goelec.send2go('/electronReady',"GET", {listeningPort: goelec.listeningPort})

   // Oddly enough, doing this dummy call to register function causes future calls to work on first key press, otherwise it only work after the second. This is the only turnaround I have found :-(
   const {globalShortcut} = require('electron')
   globalShortcut.register("CmdOrCtrl+Q",function(){})
})

// app.on("window-all-closed", function(){
//    // add a 3 sec delay before closing, to allow for final data exchanges.
//    console.log("Electron window-all-closed; will close in 3secs...");
//    process.nextTick(() => {
//       setTimeout(function(){
//          console.log("Electron exiting now");
//       }, 3000)
//    });
// })
