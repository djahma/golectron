/* This file cover the app API
It's made of a 'router' that route every absolute routes starting by '/App' to the corresponding function
*/
const app = require('electron').app //Should do the trick

// var BrowserWindow = require('electron').BrowserWindow
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	// '/App' routes come here
	router.get("/quit", quit)
	router.post("/exit", exit)
	router.get("/focus", focus)
	router.get("/hide", hide)
	router.get("/show", show)
	router.get("/getAppPath", getAppPath)
	router.post("/getPath", getPath)
	router.post("/setPath", setPath)
	router.get("/getVersion", getVersion)
	router.get("/getName", getName)
	router.post("/setName", setName)
	router.get("/getLocale", getLocale)
	router.post("/addRecentDocument", addRecentDocument)
	router.get("/clearRecentDocuments", clearRecentDocuments)
	router.post("/setAsDefaultProtocolClient", setAsDefaultProtocolClient)
	router.post("/removeAsDefaultProtocolClient", removeAsDefaultProtocolClient)
	router.post("/isDefaultProtocolClient", isDefaultProtocolClient)
	router.post("/setUserTasks", setUserTasks)
	router.get("/getJumpListSettings", getJumpListSettings)
	router.post("/setJumpList", setJumpList)
	router.get("/makeSingleInstance", makeSingleInstance)
	router.get("/releaseSingleInstance", releaseSingleInstance)
	router.post("/setUserActivity", setUserActivity)
	router.get("/getCurrentActivityType", getCurrentActivityType)
	router.post("/setAppUserModelId", setAppUserModelId)
	router.post("/importCertificate", importCertificate)
	router.get("/disableHardwareAcceleration", disableHardwareAcceleration)
	router.post("/setBadgeCount", setBadgeCount)
	router.get("/getBadgeCount", getBadgeCount)
	router.get("/isUnityRunning", isUnityRunning)
	router.get("/getLoginItemSettings", getLoginItemSettings)
	router.post("/setLoginItemSettings", setLoginItemSettings)
	router.get("/isAccessibilitySupportEnabled", isAccessibilitySupportEnabled)
	router.post("/cmdLineAppendSwitch", cmdLineAppendSwitch)
	router.post("/cmdLineAppendArgument", cmdLineAppendArgument)
	router.post("/dockBounce", dockBounce)
	router.post("/dockCancelBounce", dockCancelBounce)
	router.post("/dockDownloadFinished", dockDownloadFinished)
	router.post("/dockSetBadge", dockSetBadge)
	router.get("/dockGetBadge", dockGetBadge)
	router.get("/dockHide", dockHide)
	router.get("/dockShow", dockShow)
	router.get("/dockIsVisible", dockIsVisible)
	router.post("/dockSetMenu", dockSetMenu)
	// // Event routes
	router.get("/listen4event/:eventName", listen4event)
	router.get("/unlisten4event/:eventName", unlisten4event)
	return router
}


// Try to close all windows and by default the application will terminate.
function quit(req, res, next){
	console.log("router_App.quit function.")
	res.status(200).end()
	app.quit()
}
// Exits immediately with exitCode.
function exit(req, res, next){
	console.log("router_App.exit function.")
	res.status(200).end()
	app.exit(req.body.exitCode)
}
// On Linux, focuses on the first visible window. On macOS, makes the application the active app. On Windows, focuses on the application’s first window.
function focus(req, res, next){
	console.log("router_App.focus function.")
	app.focus()
	res.status(200).end()
}
// Hides all application windows without minimizing them.
function hide(req, res, next){
	console.log("router_App.hide function")
	app.hide()
	res.status(200).end()
}
// Shows application windows after they were hidden
function show(req, res, next){
	console.log("router_App.show function")
	app.show()
	res.status(200).end()
}
// Returns the current application directory.
function getAppPath(req, res, next){
	console.log("router_App.getAppPath function.")
	var ap = app.getAppPath()
	res.status(200).json({appPath: ap})
	res.end()
}
// Retrieves a path to a special directory or file associated with name. On failure an Error is thrown.
function getPath(req, res, next){
	console.log("router_App.getPath function. name=", req.body.name)
	try {
		p = app.getPath(req.body.name)
		res.status(200).json({path: p})
	} catch (e) {
		res.status(400).json({ error: "router_App.getPath couldnt find path for "+req.body.name })
	} finally{
		res.end()
	}
}
// Overrides the path to a special directory or file associated with name. If the path specifies a directory that does not exist, the directory will be created by this method. On failure an Error is thrown.
function setPath(req, res, next){
	console.log("router_App.setPath function. name=", req.body.name)
	try {
		app.setPath(req.body.name, req.body.path)
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: "router_App.setPath couldnt find path for "+req.body.name })
	} finally{
		res.end()
	}
}
// Returns the version of the loaded application. If no version is found in the application’s package.json file, the version of the current bundle or executable is returned.
function getVersion(req, res, next){
	console.log("router_App.getVersion function")
	try {
		p = app.getVersion()
		res.status(200).json({version: p})
	} catch (e) {
		res.status(400).json({ error: "router_App.getVersion did not work"})
	} finally{
		res.end()
	}
}
// Returns the current application’s name, which is the name in the application’s package.json file.
function getName(req, res, next){
	console.log("router_App.getName function")
	try {
		p = app.getName()
		res.status(200).json({name: p})
	} catch (e) {
		res.status(400).json({ error: "router_App.getName could not retrieve appName"})
	} finally{
		res.end()
	}
}
// Overrides the current application’s name.
function setName(req, res, next){
	console.log("router_App.setName function. name=", req.body.name)
	try {
		app.setName(req.body.name)
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: "router_App.setName couldnt find path for "+req.body.name })
	} finally{
		res.end()
	}
}
// Returns the current application locale. Possible return values are documented here.
function getLocale(req, res, next){
	console.log("router_App.getLocale function")
	try {
		p = app.getLocale()
		res.status(200).json({locale: p})
	} catch (e) {
		res.status(400).json({ error: "router_App.getLocale could not retrieve locale"})
	} finally{
		res.end()
	}
}
// Adds path to the recent documents list.
function addRecentDocument(req, res, next){
	console.log("router_App.addRecentDocument function. path=", req.body.path)
	try {
		app.addRecentDocument(req.body.path)
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: "router_App.addRecentDocument couldnt find path "+req.body.path })
	} finally{
		res.end()
	}
}
// Clears the recent documents list.
function clearRecentDocuments(req, res, next){
	console.log("router_App.clearRecentDocuments function.")
	app.clearRecentDocuments()
	res.status(200).end()
}
// This method sets the current executable as the default handler for a protocol (aka URI scheme). It allows you to integrate your app deeper into the operating system. Once registered, all links with your-protocol:// will be opened with the current executable. The whole link, including protocol, will be passed to your application as a parameter.
function setAsDefaultProtocolClient(req, res, next){
	console.log("router_App.setAsDefaultProtocolClient function. protocol=", req.body.protocol)
	try {
		var result = false
		if (req.body.protocol && req.body.path && req.body.args) {
			result = app.setAsDefaultProtocolClient(req.body.protocol && req.body.path && req.body.args)
		}else if(req.body.protocol && req.body.path){
			result = app.setAsDefaultProtocolClient(req.body.protocol && req.body.path)
		}else if (req.body.protocol) {
			result = app.setAsDefaultProtocolClient(req.body.protocol)
		}else {
			throw "ERROR router_App.setAsDefaultProtocolClient, protocol not defined"
		}
		if (result == false) {
			throw "ERROR router_App.setAsDefaultProtocolClient, call failed"
		}else{
			res.status(200)
		}
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// This method checks if the current executable as the default handler for a protocol (aka URI scheme). If so, it will remove the app as the default handler.
function removeAsDefaultProtocolClient(req, res, next){
	console.log("router_App.removeAsDefaultProtocolClient function. protocol=", req.body.protocol)
	try {
		var result = false
		if (req.body.protocol && req.body.path && req.body.args) {
			result = app.removeAsDefaultProtocolClient(req.body.protocol && req.body.path && req.body.args)
		}else if(req.body.protocol && req.body.path){
			result = app.removeAsDefaultProtocolClient(req.body.protocol && req.body.path)
		}else if (req.body.protocol) {
			result = app.removeAsDefaultProtocolClient(req.body.protocol)
		}else {
			throw "ERROR router_App.removeAsDefaultProtocolClient, protocol not defined"
		}
		if (result == false) {
			throw "ERROR router_App.removeAsDefaultProtocolClient, call failed"
		}else{
			res.status(200)
		}
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// This method checks if the current executable is the default handler for a protocol (aka URI scheme). If so, it will return true. Otherwise, it will return false.
function isDefaultProtocolClient(req, res, next){
	console.log("router_App.isDefaultProtocolClient function. protocol=", req.body.protocol)
	try {
		var result = false
		if (req.body.protocol && req.body.path && req.body.args) {
			result = app.isDefaultProtocolClient(req.body.protocol && req.body.path && req.body.args)
		}else if(req.body.protocol && req.body.path){
			result = app.isDefaultProtocolClient(req.body.protocol && req.body.path)
		}else if (req.body.protocol) {
			result = app.isDefaultProtocolClient(req.body.protocol)
		}else {
			throw "ERROR router_App.isDefaultProtocolClient, protocol not defined"
		}
		if (result == false) {
			throw "ERROR router_App.isDefaultProtocolClient, call failed"
		}else{
			res.status(200)
		}
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Adds tasks to the Tasks category of the JumpList on Windows.
function setUserTasks(req, res, next){
	console.log("router_App.setUserTasks function.")
	try {
		var result = app.setUserTasks(req.body.tasks)
		if (result == false) {
			throw "ERROR router_App.setUserTasks, call failed"
		}else{
			res.status(200)
		}
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
function getJumpListSettings(req, res, next){
	console.log("router_App.getJumpListSettings function")
	try {
		p = app.getJumpListSettings()
		res.status(200).json(p)
	} catch (e) {
		res.status(400).json({ error: "router_App.getJumpListSettings could not retrieve the jumplist"})
	} finally{
		res.end()
	}
}
// Sets or removes a custom Jump List for the application, and returns one of the following strings: ok, error, invalidSeparatorError, fileTypeRegistrationError, customCategoryAccessDeniedError.
function setJumpList(req, res, next){
	console.log("router_App.setJumpList function.")
	try {
		var result = app.setJumpList(req.body.categories)
		if (result !== 'ok') {
			throw "ERROR router_App.setJumpList, call failed: "+result
		}else{
			res.status(200)
		}
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// This method makes your application a Single Instance Application - instead of allowing multiple instances of your app to run, this will ensure that only a single instance of your app is running, and other instances signal this instance and exit.
function makeSingleInstance(req, res, next){
	console.log("router_App.makeSingleInstance function.")
	try {
		var result = app.makeSingleInstance(function(argv, wd){
			glec.send2go('/event/app/singleInstance', "POST", {args: argv, workingDirectory: wd})
		})
		res.status(200).json({isNotSingle: result})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Releases all locks that were created by makeSingleInstance. This will allow multiple instances of the application to once again run side by side.
function releaseSingleInstance(req, res, next){
	console.log("router_App.releaseSingleInstance function.")
	try {
		app.releaseSingleInstance()
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Creates an NSUserActivity and sets it as the current activity. The activity is eligible for Handoff to another device afterward.
function setUserActivity(req, res, next){
	console.log("router_App.setUserActivity function.")
	try {
		ui = JSON.parse(req.body.userInfo)
		app.setUserActivity(req.body.type, ui,req.body.webpageURL)
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Returns the type of the currently running activity.
function getCurrentActivityType(req, res, next){
	console.log("router_App.getCurrentActivityType function.")
	try {
		act=app.getCurrentActivityType()
		res.status(200).json({currentActivity: act})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Changes the Application User Model ID to id.
function setAppUserModelId(req, res, next){
	console.log("router_App.setAppUserModelId function.")
	try {
		app.setAppUserModelId(req.body.id)
		res.status(200)
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Imports the certificate in pkcs12 format into the platform certificate store. callback is called with the result of import operation, a value of 0 indicates success while any other value indicates failure according to chromium net_error_list.
function importCertificate(req, res, next){
	console.log("router_App.importCertificate function.")
	app.importCertificate({certificate:req.body.certificate, password: req.body.certificate}, function(r){
		res.status(200).json({result: r}).end()
	})
}
// Disables hardware acceleration for current app.
// This method can only be called before app is ready.
function disableHardwareAcceleration(req, res, next){
	console.log("router_App.disableHardwareAcceleration function.")
	app.disableHardwareAcceleration()
	res.status(200).end()
}
// Sets the counter badge for current app. Setting the count to 0 will hide the badge. Returns true when the call succeeded, otherwise returns false.
function setBadgeCount(req, res, next){
	console.log("router_App.setBadgeCount function.")
	r = app.setBadgeCount(req.body.count)
	res.status(200).json({success:r}).end()
}
// Returns the current value displayed in the counter badge.
function getBadgeCount(req, res, next){
	console.log("router_App.getBadgeCount function.")
	try {
		c=app.getBadgeCount()
		res.status(200).json({counter: c})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Returns whether current desktop environment is Unity launcher.
function isUnityRunning(req, res, next){
	console.log("router_App.isUnityRunning function.")
	try {
		c=app.isUnityRunning()
		res.status(200).json({isUnity: c})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Return a LoginItemSetting of the app.
function getLoginItemSettings(req, res, next){
	console.log("router_App.getLoginItemSettings function.")
	try {
		c=app.getLoginItemSettings()
		res.status(200).json({loginItem: c})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Set the app’s login item settings.
function setLoginItemSettings(req, res, next){
	console.log("router_App.setBadgeCount function.")
	r = app.setBadgsetLoginItemSettingseCount(req.body.loginItem)
	res.status(200).end()
}
// Returns a Boolean, true if Chrome’s accessibility support is enabled, false otherwise. This API will return true if the use of assistive technologies, such as screen readers, has been detected. See https://www.chromium.org/developers/design-documents/accessibility for more details.
function isAccessibilitySupportEnabled(req, res, next){
	console.log("router_App.isAccessibilitySupportEnabled function.")
	try {
		c=app.isAccessibilitySupportEnabled()
		res.status(200).json({accessEnabled: c})
	} catch (e) {
		res.status(400).json({ error: e })
	} finally{
		res.end()
	}
}
// Append a switch (with optional value) to Chromium’s command line.
function cmdLineAppendSwitch(req, res, next){
	console.log("router_App.cmdLineAppendSwitch function.")
	if (req.body.value) {
		r = app.commandLine.appendSwitch(req.body.switch, req.body.value)
	}else{
		r = app.commandLine.appendSwitch(req.body.switch)
	}
	res.status(200).end()
}
// Append an argument to Chromium’s command line. The argument will be quoted correctly.
function cmdLineAppendArgument(req, res, next){
	console.log("router_App.cmdLineAppendArgument function.")
	app.commandLine.appendArgument(req.body.value)
	res.status(200).end()
}
// Cause macOS dock to bounce
function dockBounce(req, res, next){
	console.log("router_App.dockBounce function.")
	i=app.dock.bounce(req.body.type)
	res.status(200).json({id:i}).end()
}
// Cancel the bounce of id.
function dockCancelBounce(req, res, next){
	console.log("router_App.dockCancelBounce function.")
	app.dock.cancelBounce(req.body.id)
	res.status(200).end()
}
// Bounces the Downloads stack if the filePath is inside the Downloads folder.
function dockDownloadFinished(req, res, next){
	console.log("router_App.dockDownloadFinished function.")
	iapp.dock.downloadFinished(req.body.filePath)
	res.status(200).end()
}
// Sets the string to be displayed in the dock’s badging area.
function dockSetBadge(req, res, next){
	console.log("router_App.dockSetBadge function.")
	app.dock.setBadge(req.body.text)
	res.status(200).end()
}
// Returns the badge string of the dock.
function dockGetBadge(req, res, next){
	console.log("router_App.dockGetBadge function.")
	b=app.dock.getBadge()
	res.status(200).json({text: b}).end()
}
// Hides the dock icon.
function dockHide(req, res, next){
	console.log("router_App.dockHide function.")
	app.dock.hide()
	res.status(200).end()
}
// Shows the dock icon.
function dockShow(req, res, next){
	console.log("router_App.dockShow function.")
	app.dock.show()
	res.status(200).end()
}
// Returns whether the dock icon is visible. The app.dock.show() call is asynchronous so this method might not return true immediately after that call.
function dockIsVisible(req, res, next){
	console.log("router_App.dockIsVisible function.")
	v=app.dock.isVisible()
	res.status(200).json({isVisible: v}).end()
}
// Sets the application’s dock menu.
function dockSetMenu(req, res, next){
	console.log("router_App.dockSetMenu function.")
	m = glec.getMenuById(req.body.menuID)
	if (m!==null) {
		app.dock.setMenu(m)
		res.status(200).end()
	}else{
		res.status(400).json({error: "menu not found"}).end()
	}
}

/*----------------------------------------------------------
electron.app API implementation for Golectron: Events
Check http://electron.atom.io/docs/api/app/ for more details
----------------------------------------------------------*/

// Registers an eventName to be listenned for
function listen4event(req, res, next){
	console.log("router_App.listen4event function. event=", req.params.eventName)
	if (req.params.eventName == "ready") {
		app.on(req.params.eventName, function(e, lInfo){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {launchInfo: lInfo, event: e})
		})
	}else if (req.params.eventName == "quit") {
		app.on(req.params.eventName, function(e, c){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {exitCode: c, event: e})
		})
	}else if (req.params.eventName == "open-file") {
		app.on(req.params.eventName, function(e, d){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {path: d, event: e})
		})
	}else if (req.params.eventName == "open-url") {
		app.on(req.params.eventName, function(e, u){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {url: u, event: e})
		})
	}else if (req.params.eventName == "activate") {
		app.on(req.params.eventName, function(e, h){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {hasVisibleWindows: h, event: e})
		})
	}else if (req.params.eventName == "continue-activity") {
		app.on(req.params.eventName, function(e, t, u){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {type: t, userInfo:u, event: e})
		})
	}else if (req.params.eventName == "browser-window-blur" ||
				req.params.eventName == "browser-window-focus"||
				req.params.eventName == "browser-window-created") {
		app.on(req.params.eventName, function(e, w){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {window: w.id, event: e})
		})
	}else if (req.params.eventName == "web-contents-created") {
		app.on(req.params.eventName, function(e, wc){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {webContents: wc, event: e})
		})
	}else if (req.params.eventName == "certificate-error") {
		app.on(req.params.eventName, function(e, wc, u, err, c, cb){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {webContents: wc, url:u, error:err, certificate: c, event: e})
		})
	}else if (req.params.eventName == "select-client-certificate") {
		app.on(req.params.eventName, function(e, wc, u, cl, cb){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {webContents: wc, url:u, certificateList: cl, event: e})
		})
	}else if (req.params.eventName == "login") {
		app.on(req.params.eventName, function(e, wc, r, ai, cb){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {webContents: wc, request:r, authInfo:ai, event: e})
		})
	}else if (req.params.eventName == "accessibility-support-changed") {
		app.on(req.params.eventName, function(e, ase){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {accessibilitySupportEnabled: ase, event: e})
		})
	}else{
		app.on(req.params.eventName, function(e){
			glec.send2go('/event/app/'+req.params.eventName, "POST", {event: e})
		})
	}
	res.status(200).end()
}
// Returns an array of eventName currently listenned for
function unlisten4event(req, res, next){
	console.log("router_App..unlisten4event function. event=", req.params.eventName)
	app.removeAllListeners(req.params.eventName)
	res.status(200).end()
}
