var BrowserWindow = require('electron').BrowserWindow
var bodyParser = require('body-parser')
var Menu = require('electron').Menu
var MenuItem = require('electron').MenuItem

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	console.log("initRoutes")
	router = xprss
	glec = xprss.locals.glec
	// router utility functions/setups
	xprss.use(bodyParser.json());
	xprss.use(bodyParser.urlencoded({ extended: true }));
	console.log("xprss.get('/test', test)")
	xprss.get("/test", test)
	xprss.post("/create/:what", r_create)
	// Electron APP
	console.log("xprss.get('/App', router_App)")
	var router_App = require('./routes_App.js').initRoutes(xprss)
	xprss.use('/App', router_App)
	// Electron GlobalShortcut + accelerator
	console.log("xprss.get('/GlobalShortcut', router_GlobalShortcut)")
	var router_GlobalShortcut = require('./routes_GlobalShortcut.js').initRoutes(xprss)
	xprss.use('/GlobalShortcut', router_GlobalShortcut)
	// Electron Menu
	console.log("xprss.get('/Menu', router_Menu)")
	var router_Menu = require('./routes_Menu.js').initRoutes(xprss)
	xprss.use('/Menu', router_Menu)
	// Electron MenuItem
	console.log("xprss.get('/MenuItem', router_MenuItem)")
	var router_MenuItem = require('./routes_MenuItem.js').initRoutes(xprss)
	xprss.use('/MenuItem', router_MenuItem)
	// Electron BrowserWindow
	console.log("xprss.get('/BrowserWindow', router_BrowserWindow)")
	var router_BrowserWindow =require('./routes_BrowserWindow.js').initRoutes(xprss)
	xprss.use('/BrowserWindow', router_BrowserWindow)
	// Electron WebContents
	console.log("xprss.get('/WebContents', router_WebContents)")
	var router_WebContents = require('./routes_WebContents.js').initRoutes(xprss)
	xprss.use('/WebContents', router_WebContents)
	// Electron Session
	console.log("xprss.get('/Session', router_Session)")
	var router_Session = require('./routes_Session.js').initRoutes(xprss)
	xprss.use('/Session', router_Session)

	// Server starts here
	var srvr = xprss.listen(xprss.locals.glec.listeningPort)
	srvr.on('listening', function(){
		console.log("ELECTRON listening on", srvr.address().port)
		xprss.locals.glec.listeningPort=srvr.address().port
	})
	srvr.on('close', function(){
		console.log("ELECTRON closing listening now!")
	})
}

function test(req, res, next){
	console.log("xprss.locals.glec test function")

	res.status(200).end()
}

function r_create(req, res, next){
	console.log("gole create function, what? ", req.params.what)
	if (req.params.what=="browserWindow") {
		win = new BrowserWindow(req.body)
		console.log("Have I found you?", glec.test2)
		glec.__addWindow(win)
		// win.loadURL("http://www.google.fr")
		res.json({'id':win.id})
	}else if(req.params.what=="menu"){
		m = new Menu()
		glec.__addMenu(m)
		res.status(200).json({'id':m.id})
	}else if(req.params.what=="menuitem"){
		console.log("menuitem menuitemoptions=", req.body);
		if(req.body.menuitemoptions.type=="submenu"){
			req.body.menuitemoptions.submenu=[]
		}
		m = new MenuItem(req.body.menuitemoptions)
		if (m.role=="") {
			m.click = function(menuItem, browserWindow, event){
				glec.send2go('/event/menuitem/'+menuitem.id+"/click","POST", {browserWindowID: browserWindow.id, event: event})
			}
		}
		if(m.type=="submenu"){
			glec.__addMenu(m.submenu)
		}
		glec.__addMenuItem(m)
		res.status(200).json({'menuitemID':m.menuitemID})
	}else{
		res.status(404)
	}
	res.end()
}
