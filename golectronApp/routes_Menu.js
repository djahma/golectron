/* This file cover the Menu API
It's made of a 'router' that route every absolute routes starting by '/Menu' to the corresponding function
*/
var Menu = require('electron').Menu
var MenuItem = require('electron').MenuItem
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	router.use(bodyParser.raw({ type: 'application/javascript' }));
	//routes come here
	router.get("/getApplicationMenu", getApplicationMenu)
	router.get("/sendActionToFirstResponder", sendActionToFirstResponder)
	router.post("/buildFromTemplate", buildFromTemplate)
	router.get("/:idnum", testID)
	router.get("/:idnum/destroy", destroy)
	router.get("/:idnum/setApplicationMenu", setApplicationMenu)
	router.post("/:idnum/popup", popup)
	router.post("/:idnum/append", append)
	router.post("/:idnum/insert", insert)

	return router
}

function testID(req, res, next){
	console.log("router_Menu.testID function. id=", req.params.idnum)
	var m = glec.getMenuById(req.params.idnum)
	if(m!==null){
		console.log("router found menu id", m.id)
		res.status(200)
	}else{
		console.log("router couldnt find menu id", req.params.idnum)
		res.status(404)
	}
	res.end()
}
// Destroy the menu, and its sub menuitems too.
function destroy(req, res, next){
	console.log("router_Menu.destroy function. id=", req.params.idnum)
	var response = glec.destroyMenu(req.params.idnum)
	res.status(200).json(response)
	res.end()
}
//Sets menu as the application menu on macOS. On Windows and Linux, the menu will be set as each window’s top menu.
function setApplicationMenu(req, res, next){
	console.log("router_Menu.setApplicationMenu function. id=", req.params.idnum)
	var m = glec.getMenuById(req.params.idnum)
	if(m!==null){
		console.log("router found menu id", m.id)
		Menu.setApplicationMenu(m)
		res.status(200)
	}else{
		console.log("router couldnt find menu id", req.params.idnum)
		res.status(404)
	}
	res.end()
}
//Returns the application menu (an instance of Menu), if set, or null, if not set.
function getApplicationMenu(req, res, next){
	console.log("router_Menu.getApplicationMenu function.")
	appMenu = Menu.getApplicationMenu()
	res.status(200).json({'menuID':appMenu.id})
	res.end()
}
//Sends the action to the first responder of application. This is used for emulating default Cocoa menu behaviors, usually you would just use the role property of MenuItem.
function sendActionToFirstResponder(req, res, next){
	console.log("router_Menu.sendActionToFirstResponder function. id=", req.params.idnum)
	Menu.sendActionToFirstResponder(req.body.action)
	res.status(200)
	res.end()
}
//Menu.buildFromTemplate(template)
function buildFromTemplate(req, res, next){
	console.log("router_Menu.buildFromTemplate function")
	try {
		m = Menu.buildFromTemplate(eval(req.body.toString()))
		glec.__addMenu(m)
		var x = function(o){
			var retVal = {}
			if (o instanceof Menu){
				glec.__addMenu(o)
				retVal.menuID=o.id
				retVal.items=[]
				for (var i = 0; i < o.items.length; i++) {
					retVal.items.push(x(o.items[i]))
				}
				return retVal
			}else if (o instanceof MenuItem){
				glec.__addMenuItem(o)
				retVal.menuitemID=o.menuitemID
				if(o.type==='submenu'){
					retVal.submenu = x(o.submenu)
				}
				return retVal
			}
		}
		ids = x(m)
		res.status(200).json(ids)
	} catch (e) {
		res.status(400).json({error:e.message})
	} finally {
		res.end()
	}
}
// Pops up this menu as a context menu in the browserWindow.
function popup(req, res, next){
	console.log("router_Menu.popup function. id=", req.params.idnum)
	var m = glec.getMenuById(req.params.idnum)
	var appMenu = null
	if(m!==null){
		if(req.body.browserWindowID!==-1 && glec.getWindowById(req.body.browserWindowID)!==null){
			win = glec.getWindowById(req.body.browserWindowID)
			if (req.body.x!==-1 && req.body.y!==-1) {
					m.popup(win, req.body.x,req.body.y)
			}else{
				m.popup(req.body.x,req.body.y)
			}
		}else if (req.body.x!==-1 && req.body.y!==-1){
			m.popup(req.body.x,req.body.y)
		}else{
			m.popup()
		}
		res.status(200)
	}else{
		res.status(400).json({ error: "router_Menu.popup couldnt find menu id:"+req.params.idnum })
	}
	res.end()
}
// Appends the menuItem to the menu.
function append(req, res, next){
	console.log("router_Menu.append function. id=", req.params.idnum)
	var m = glec.getMenuById(req.params.idnum)
	if(m!==null){
		console.log(req.body);
		miID = req.body.menuitemID
		mi = glec.getMenuItemById(miID)
		if (mi!==null) {
			m.append(mi)
			res.status(200)
		}else{
			res.status(400).json({ error: "router_Menu.append couldnt find menuitem id:"+miID })
		}
	}else{
		res.status(400).json({ error: "router_Menu.append couldnt find menu id:"+req.params.idnum })
	}
	res.end()
}
// Inserts the menuItem to the pos position of the menu.
function insert(req, res, next){
	console.log("router_Menu.insert function. id=", req.params.idnum)
	var m = glec.getMenuById(req.params.idnum)
	if(m!==null){
		miID = req.body.menuitem.id
		mi = glec.getMenuItemById(miID)
		if (mi!==null) {
			m.insert(req.body.pos, mi)
			res.status(200)
		}else{
			res.status(400).json({ error: "router_Menu.insert couldnt find menuitem id:"+miID })
		}
	}else{
		res.status(400).json({ error: "router_Menu.insert couldnt find menu id:"+req.params.idnum })
	}
	res.end()
}

/*
{
	"menuID":2,
	"items":[
		{
			"menuitemID":0, //File
			"submenu":{
				"menuID":0,
				"items":[
					{"menuitemID":1} //Quit
				]
			}
		},
		{
			"menuitemID":2,	//Edit
			"submenu":{
				"menuID":1,
				"items":[
					{"menuitemID":3},	//Undo
					{"menuitemID":4},	//Redo
					{"menuitemID":5},	//Sep
					{"menuitemID":6},	//Cut
					{"menuitemID":7},	//Copy
					{"menuitemID":8},	//Paste
					{"menuitemID":9}	//SelectAll
				]
			}
		},
		{
			"menuitemID":10,	//View
			"submenu":{
				"menuID":3,
				"items":[
					{"menuitemID":11},	//Toggle Full screen
					{"menuitemID":12},	//Toggle DevTools
					{"menuitemID":13},	//Sep
					{
						"menuitemID":14,	//Panels
						"submenu":{
							"menuID":4,
							"items":[]
						}
					},
					{
						"menuitemID":15,	//Tabs
						"submenu":{
							"menuID":5,
							"items":[]
						}
					}
				]
			}
		},
		{
			"menuitemID":16,	//Help
			"submenu":{
				"menuID":6,
				"items":[
					{"menuitemID":17}	//Electron Docs
				]
			}
		}
	]
}
*/
