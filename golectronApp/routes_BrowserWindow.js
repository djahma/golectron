/* This file cover the BrowserWindow API
It's made of a 'router' that route every absolute routes starting by '/BrowserWindow' to the corresponding function
*/

var BrowserWindow = require('electron').BrowserWindow
var bodyParser = require('body-parser')
var express = require('express')

module.exports.initRoutes = initRoutes;

var router
var glec

function initRoutes(xprss){
	router = express.Router()
	glec = xprss.locals.glec
	// router utility functions/setups
	router.use(bodyParser.json());
	router.use(bodyParser.urlencoded({ extended: true }));
	// /BrowserWindow routes come here
	router.get("/:idnum", testID)
	router.get("/:idnum/getWebContent", getWebContent)
	router.get("/:idnum/destroy", destroy)
	router.get("/:idnum/close", close)
	router.get("/:idnum/focus", focus)
	router.get("/:idnum/blur", blur)
	router.get("/:idnum/isFocused", isFocused)
	router.get("/:idnum/isDestroyed", isDestroyed)
	router.get("/:idnum/show", show)
	router.get("/:idnum/showInactive", showInactive)
	router.get("/:idnum/hide", hide)
	router.get("/:idnum/isVisible", isVisible)
	router.get("/:idnum/isModal", isModal)
	router.get("/:idnum/maximize", maximize)
	router.get("/:idnum/unmaximize", unmaximize)
	router.get("/:idnum/isMaximized", isMaximized)
	router.get("/:idnum/minimize", minimize)
	router.get("/:idnum/restore", restore)
	router.get("/:idnum/isMinimized", isMinimized)
	router.post("/:idnum/setFullScreen", setFullScreen)
	router.get("/:idnum/isFullScreen", isFullScreen)
	router.post("/:idnum/setAspectRatio", setAspectRatio)
	router.post("/:idnum/setBounds", setBounds)
	router.get("/:idnum/getBounds", getBounds)
	router.post("/:idnum/setContentBounds", setContentBounds)
	router.get("/:idnum/getContentBounds", getContentBounds)
	router.post("/:idnum/setSize", setSize)
	router.get("/:idnum/getSize", getSize)
	router.post("/:idnum/setContentSize", setContentSize)
	router.get("/:idnum/getContentSize", getContentSize)
	router.post("/:idnum/setMinimumSize", setMinimumSize)
	router.get("/:idnum/getMinimumSize", getMinimumSize)
	router.post("/:idnum/setMaximumSize", setMaximumSize)
	router.get("/:idnum/getMaximumSize", getMaximumSize)
	router.post("/:idnum/setResizable", setResizable)
	router.get("/:idnum/isResizable", isResizable)
	router.post("/:idnum/setMovable", setMovable)
	router.get("/:idnum/isMovable", isMovable)
	router.post("/:idnum/setMinimizable", setMinimizable)
	router.get("/:idnum/isMinimizable", isMinimizable)
	router.post("/:idnum/setMaximizable", setMaximizable)
	router.get("/:idnum/isMaximizable", isMaximizable)
	router.post("/:idnum/setFullScreenable", setFullScreenable)
	router.get("/:idnum/isFullScreenable", isFullScreenable)
	router.post("/:idnum/setClosable", setClosable)
	router.get("/:idnum/isClosable", isClosable)
	router.post("/:idnum/setAlwaysOnTop", setAlwaysOnTop)
	router.get("/:idnum/isAlwaysOnTop", isAlwaysOnTop)
	router.get("/:idnum/center", center)
	router.post("/:idnum/setPosition", setPosition)
	router.get("/:idnum/getPosition", getPosition)
	router.post("/:idnum/setTitle", setTitle)
	router.get("/:idnum/getTitle", getTitle)
	router.post("/:idnum/flashFrame", flashFrame)
	router.post("/:idnum/setSkipTaskbar", setSkipTaskbar)
	router.post("/:idnum/setKiosk", setKiosk)
	router.get("/:idnum/isKiosk", isKiosk)
	router.post("/:idnum/hookWindowMessage", hookWindowMessage)
	router.post("/:idnum/isWindowMessageHooked", isWindowMessageHooked)
	router.post("/:idnum/unhookWindowMessage", unhookWindowMessage)
	router.get("/:idnum/unhookAllWindowMessages", unhookAllWindowMessages)
	router.post("/:idnum/setRepresentedFilename", setRepresentedFilename)
	router.get("/:idnum/getRepresentedFilename", getRepresentedFilename)
	router.post("/:idnum/setDocumentEdited", setDocumentEdited)
	router.get("/:idnum/isDocumentEdited", isDocumentEdited)
	router.get("/:idnum/focusOnWebView", focusOnWebView)
	router.get("/:idnum/blurWebView", blurWebView)
	router.post("/:idnum/loadURL", loadURL)
	router.get("/:idnum/reload", reload)
	router.post("/:idnum/setProgressBar", setProgressBar)
	router.post("/:idnum/setHasShadow", setHasShadow)
	router.get("/:idnum/hasShadow", hasShadow)
	router.post("/:idnum/setThumbnailClip", setThumbnailClip)
	router.get("/:idnum/showDefinitionForSelection", showDefinitionForSelection)
	router.post("/:idnum/setAutoHideMenuBar", setAutoHideMenuBar)
	router.get("/:idnum/isMenuBarAutoHide", isMenuBarAutoHide)
	router.post("/:idnum/setMenuBarVisibility", setMenuBarVisibility)
	router.get("/:idnum/isMenuBarVisible", isMenuBarVisible)
	router.post("/:idnum/SetVisibleOnAllWorkspaces", SetVisibleOnAllWorkspaces)
	router.get("/:idnum/IsVisibleOnAllWorkspaces", IsVisibleOnAllWorkspaces)
	router.post("/:idnum/setIgnoreMouseEvents", setIgnoreMouseEvents)
	router.post("/:idnum/setContentProtection", setContentProtection)
	router.post("/:idnum/setFocusable", setFocusable)
	router.post("/:idnum/setParentWindow", setParentWindow)
	router.get("/:idnum/getParentWindow", getParentWindow)
	router.get("/:idnum/getChildWindows", getChildWindows)
	// Event routes
	router.get("/:idnum/listen4event/:eventName", listen4event)
	router.get("/:idnum/unlisten4event/:eventName", unlisten4event)
	return router
}

function testID(req, res, next){
	console.log("router_BrowserWindow.testID function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
	}else{
		console.log("router couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
function getWebContent(req, res, next){
	console.log("router_BrowserWindow.getWebContent function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router_BrowserWindow found BrowserWindow id", w.id,w.webContents.id)
			res.status(200).json({wcID: w.webContents.id}).end()
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getWebContent couldnt find BrowserWindow id:"+req.params.idnum }).end()
	}
}
// Force closing the window
function destroy(req, res, next){
	console.log("router_BrowserWindow.destroy function. id=", req.params.idnum)
	var response = {}
	response.menus = []
	response.menuitems = []
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		glec.__removeWindow(req.params.idnum)
		w.destroy()
	}else{
		console.log("router_BrowserWindow.destroy couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Try to close the window.
function close(req, res, next){
	console.log("router_BrowserWindow.close function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		glec.__removeWindow(req.params.idnum)
		w.close()
	}else{
		console.log("router_BrowserWindow.close couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Focuses on the window.
function focus(req, res, next){
	console.log("router_BrowserWindow.focus function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.focus()
	}else{
		console.log("router_BrowserWindow.focus couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Removes focus from the window.
function blur(req, res, next){
	console.log("router_BrowserWindow.blur function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.blur()
	}else{
		console.log("router_BrowserWindow.blur couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Returns a boolean, whether the window is focused.
function isFocused(req, res, next){
	console.log("router_BrowserWindow.isFocused function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isF = false
	if(w!==null){
		isF = w.isFocused()
		res.status(200).json({'isFocused':isF})
	}else{
		console.log("router_BrowserWindow.isFocused couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isFocused couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns a boolean, whether the window is destroyed.
function isDestroyed(req, res, next){
	console.log("router_BrowserWindow.isDestroyed function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isD = false
	if(w!==null){
		isD = w.isDestroyed()
		res.status(200).json({'isDestroyed':isD})
	}else{
		console.log("router_BrowserWindow.isDestroyed couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isDestroyed couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Shows and gives focus to the window.
function show(req, res, next){
	console.log("router_BrowserWindow.show function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		w.show()
	}else{
		console.log("router couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Shows the window but doesn’t focus on it.
function showInactive(req, res, next){
	console.log("router_BrowserWindow.showInactive function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.showInactive()
	}else{
		console.log("router_BrowserWindow.showInactive couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Hides the window.
function hide(req, res, next){
	console.log("router_BrowserWindow.hide function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.hide()
	}else{
		console.log("router_BrowserWindow.hide couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Returns a boolean, whether the window is visible to the user.
function isVisible(req, res, next){
	console.log("router_BrowserWindow.isVisible function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isV = false
	if(w!==null){
		isV = w.isVisible()
		res.status(200).json({'isVisible':isV})
	}else{
		console.log("router_BrowserWindow.isVisible couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isVisible couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns a boolean, whether current window is a modal window.
function isModal(req, res, next){
	console.log("router_BrowserWindow.isModal function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isM = false
	if(w!==null){
		isM = w.isModal()
		res.status(200).json({'isModal':isM})
	}else{
		console.log("router_BrowserWindow.isModal couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isModal couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Maximizes the window.
function maximize(req, res, next){
	console.log("router_BrowserWindow.maximize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.maximize()
	}else{
		console.log("router_BrowserWindow.maximize couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Unmaximizes the window.
function unmaximize(req, res, next){
	console.log("router_BrowserWindow.unmaximize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.unmaximize()
	}else{
		console.log("router_BrowserWindow.unmaximize couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Returns a boolean, whether the window is maximized.
function isMaximized(req, res, next){
	console.log("router_BrowserWindow.isMaximized function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isM = false
	if(w!==null){
		isM = w.isMaximized()
		res.status(200).json({'isMaximized':isM})
	}else{
		console.log("router_BrowserWindow.isMaximized couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isMaximized couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Minimizes the window. On some platforms the minimized window will be shown in the Dock.
function minimize(req, res, next){
	console.log("router_BrowserWindow.minimize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.minimize()
	}else{
		console.log("router_BrowserWindow.minimize couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Restores the window from minimized state to its previous state.
function restore(req, res, next){
	console.log("router_BrowserWindow.restore function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.restore()
	}else{
		console.log("router_BrowserWindow.restore couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Returns a boolean, whether the window is minimized.
function isMinimized(req, res, next){
	console.log("router_BrowserWindow.isMinimized function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isM = false
	if(w!==null){
		isM = w.isMinimized()
		res.status(200).json({'isMinimized':isM})
	}else{
		console.log("router_BrowserWindow.isMinimized couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isMinimized couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window should be in fullscreen mode.
function setFullScreen(req, res, next){
	console.log("router_BrowserWindow.setFullScreen function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setFullScreen(req.body.flag)
	}else{
		console.log("router_BrowserWindow.setFullScreen couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Returns a boolean, whether the window is in fullscreen mode.
function isFullScreen(req, res, next){
	console.log("router_BrowserWindow.isFullScreen function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var isF = false
	if(w!==null){
		isF = w.isFullScreen()
		res.status(200).json({'isFullScreen':isF})
	}else{
		console.log("router_BrowserWindow.isFullScreen couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.isFullScreen couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// This will make a window maintain an aspect ratio. The extra size allows a developer to have space, specified in pixels, not included within the aspect ratio calculations. This API already takes into account the difference between a window’s size and its content size.
function setAspectRatio(req, res, next){
	console.log("router_BrowserWindow.setAspectRatio function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setAspectRatio(req.body.aspectRatio, opts)
		res.status(200)
	}else{
		console.log("router_BrowserWindow.setAspectRatio couldnt find window id", req.params.idnum)
		res.status(400).json({ error: "router_BrowserWindow.setAspectRatio couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Resizes and moves the window to width, height, x, y.
function setBounds(req, res, next){
	console.log("router_BrowserWindow.setBounds function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.x!==0){opts.x=req.body.x}
		if(req.body.y!==0){opts.y=req.body.y}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setBounds(opts, req.body.animate)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setBounds couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an object that contains window’s width, height, x and y values.
function getBounds(req, res, next){
	console.log("router_BrowserWindow.getBounds function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getBounds()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getBounds couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Resizes and moves the window’s client area (e.g. the web page) to width, height, x, y.
function setContentBounds(req, res, next){
	console.log("router_BrowserWindow.setContentBounds function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.x!==0){opts.x=req.body.x}
		if(req.body.y!==0){opts.y=req.body.y}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setContentBounds(opts, req.body.animate)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setContentBounds couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an object that contains the window’s client area (e.g. the web page) width, height, x and y values.
function getContentBounds(req, res, next){
	console.log("router_BrowserWindow.getContentBounds function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getContentBounds()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getContentBounds couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Resizes the window to width and height.
function setSize(req, res, next){
	console.log("router_BrowserWindow.setSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setSize(opts, req.body.animate)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array that contains window’s width and height.
function getSize(req, res, next){
	console.log("router_BrowserWindow.getSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getSize()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Resizes the window’s client area (e.g. the web page) to width and height.
function setContentSize(req, res, next){
	console.log("router_BrowserWindow.setContentSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setContentSize(opts, req.body.animate)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setContentSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array that contains window’s client area’s width and height.
function getContentSize(req, res, next){
	console.log("router_BrowserWindow.getContentSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getContentSize()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getContentSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets the minimum size of window to width and height.
function setMinimumSize(req, res, next){
	console.log("router_BrowserWindow.setMinimumSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setMinimumSize(opts)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMinimumSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array that contains window’s minimum width and height.
function getMinimumSize(req, res, next){
	console.log("router_BrowserWindow.getMinimumSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getMinimumSize()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getMinimumSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets the maximum size of window to width and height.
function setMaximumSize(req, res, next){
	console.log("router_BrowserWindow.setMaximumSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts ={}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setMaximumSize(opts)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMaximumSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array that contains window’s maximum width and height.
function getMaximumSize(req, res, next){
	console.log("router_BrowserWindow.getMaximumSize function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getMaximumSize()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getMaximumSize couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window can be manually resized by user.
function setResizable(req, res, next){
	console.log("router_BrowserWindow.setResizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setResizable(req.body.resizable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setResizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window can be manually resized by user.
function isResizable(req, res, next){
	console.log("router_BrowserWindow.isResizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isResizable()
		res.status(200).json({'isResizable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isResizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window can be moved by user. On Linux does nothing.
function setMovable(req, res, next){
	console.log("router_BrowserWindow.setMovable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setMovable(req.body.movable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMovable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window can be moved by user. On Linux always returns true.
function isMovable(req, res, next){
	console.log("router_BrowserWindow.isMovable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isMovable()
		res.status(200).json({'isMovable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isMovable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window can be manually minimized by user. On Linux does nothing.
function setMinimizable(req, res, next){
	console.log("router_BrowserWindow.setMinimizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setMinimizable(req.body.minimizable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMinimizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window can be manually minimized by user. On Linux always returns true.
function isMinimizable(req, res, next){
	console.log("router_BrowserWindow.isMinimizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isMinimizable()
		res.status(200).json({'isMinimizable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isMinimizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window can be manually maximized by user. On Linux does nothing.
function setMaximizable(req, res, next){
	console.log("router_BrowserWindow.setMaximizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setMaximizable(req.body.maximizable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMaximizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window can be manually maximized by user. On Linux always returns true.
function isMaximizable(req, res, next){
	console.log("router_BrowserWindow.isMaximizable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isMaximizable()
		res.status(200).json({'isMaximizable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isMaximizable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the maximize/zoom window button toggles fullscreen mode or maximizes the window.
function setFullScreenable(req, res, next){
	console.log("router_BrowserWindow.setFullScreenable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setFullScreenable(req.body.fullScreenable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setFullScreenable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the maximize/zoom window button toggles fullscreen mode or maximizes the window.
function isFullScreenable(req, res, next){
	console.log("router_BrowserWindow.isFullScreenable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isFullScreenable()
		res.status(200).json({'isFullScreenable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isFullScreenable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window can be manually closed by user. On Linux does nothing.
function setClosable(req, res, next){
	console.log("router_BrowserWindow.setClosable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setClosable(req.body.closable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setClosable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window can be manually closed by user. On Linux always returns true.
function isClosable(req, res, next){
	console.log("router_BrowserWindow.isClosable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isClosable()
		res.status(200).json({'isClosable':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isClosable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window should show always on top of other windows. After setting this, the window is still a normal window, not a toolbox window which can not be focused on.
function setAlwaysOnTop(req, res, next){
	console.log("router_BrowserWindow.setAlwaysOnTop function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setAlwaysOnTop(req.body.alwaysOnTop)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setAlwaysOnTop couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window is always on top of other windows.
function isAlwaysOnTop(req, res, next){
	console.log("router_BrowserWindow.isAlwaysOnTop function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isAlwaysOnTop()
		res.status(200).json({'isAlwaysOnTop':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isAlwaysOnTop couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Moves window to the center of the screen.
function center(req, res, next){
	console.log("router_BrowserWindow.center function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.center()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.center couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Moves window to x and y.
function setPosition(req, res, next){
	console.log("router_BrowserWindow.setPosition function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setPosition(req.body.x, req.body.y, req.body.animate)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setPosition couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array that contains window’s current position.
function getPosition(req, res, next){
	console.log("router_BrowserWindow.getPosition function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.getPosition()
		res.status(200).json(bounds)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getPosition couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Changes the title of native window to title.
function setTitle(req, res, next){
	console.log("router_BrowserWindow.setTitle function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setTitle(req.body.title)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setTitle couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns the title of the native window.
function getTitle(req, res, next){
	console.log("router_BrowserWindow.getTitle function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		var bounds = w.getTitle()
		res.status(200).json({ "title": bounds })
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getTitle couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Starts or stops flashing the window to attract user’s attention.
function flashFrame(req, res, next){
	console.log("router_BrowserWindow.flashFrame function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.flashFrame(req.body.flashFrame)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.flashFrame couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Makes the window not show in the taskbar.
function setSkipTaskbar(req, res, next){
	console.log("router_BrowserWindow.setSkipTaskbar function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setSkipTaskbar(req.body.skip)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setSkipTaskbar couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Enters or leaves the kiosk mode.
function setKiosk(req, res, next){
	console.log("router_BrowserWindow.setKiosk function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setKiosk(req.body.flag)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setKiosk couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window is in kiosk mode.
function isKiosk(req, res, next){
	console.log("router_BrowserWindow.isKiosk function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isKiosk()
		res.status(200).json({'isKiosk':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isKiosk couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Hooks a windows message. The callback is called when the message is received in the WndProc. WINDOWS
function hookWindowMessage(req, res, next){
	console.log("router_BrowserWindow.hookWindowMessage function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.hookWindowMessage(req.body.message,function(){
			res.status(200)
		})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.hookWindowMessage couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns true or false depending on whether the message is hooked. WINDOWS
function isWindowMessageHooked(req, res, next){
	console.log("router_BrowserWindow.isWindowMessageHooked function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isWindowMessageHooked(req.body.message)
		res.status(200).json({'isWindowMessageHooked':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isWindowMessageHooked couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Unhook the window message. WINDOWS
function unhookWindowMessage(req, res, next){
	console.log("router_BrowserWindow.unhookWindowMessage function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.unhookWindowMessage(req.body.message)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.unhookWindowMessage couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Unhooks all of the window messages. WINDOWS
function unhookAllWindowMessages(req, res, next){
	console.log("router_BrowserWindow.unhookAllWindowMessages function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.unhookAllWindowMessages()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.unhookAllWindowMessages couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets the pathname of the file the window represents, and the icon of the file will show in window’s title bar.
function setRepresentedFilename(req, res, next){
	console.log("router_BrowserWindow.setRepresentedFilename function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.setRepresentedFilename(req.body.filename)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setRepresentedFilename couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns the pathname of the file the window represents.
function getRepresentedFilename(req, res, next){
	console.log("router_BrowserWindow.getRepresentedFilename function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = ""
	if(w!==null){
		bounds = w.getRepresentedFilename()
		res.status(200).json({'filename':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getRepresentedFilename couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Specifies whether the window’s document has been edited, and the icon in title bar will become gray when set to true.
function setDocumentEdited(req, res, next){
	console.log("router_BrowserWindow.setDocumentEdited function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setDocumentEdited(req.body.edited)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setDocumentEdited couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Whether the window’s document has been edited.
function isDocumentEdited(req, res, next){
	console.log("router_BrowserWindow.isDocumentEdited function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isDocumentEdited()
		res.status(200).json({'edited':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isDocumentEdited couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// win.focusOnWebView()
function focusOnWebView(req, res, next){
	console.log("router_BrowserWindow.focusOnWebView function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.focusOnWebView()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.focusOnWebView couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// win.blurWebView()
function blurWebView(req, res, next){
	console.log("router_BrowserWindow.blurWebView function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.blurWebView()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.blurWebView couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Same as webContents.loadURL(url[, options]).
function loadURL(req, res, next){
	console.log("router_BrowserWindow.loadURL function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		w.loadURL(req.body.url, req.body)
	}else{
		console.log("router_BrowserWindow.loadURL couldnt find window id", req.params.idnum)
	}
	res.status(200).end()
}
// Same as webContents.reload.
function reload(req, res, next){
	console.log("router_BrowserWindow.reload function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.reload()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.reload couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets progress value in progress bar. Valid range is [0, 1.0].
function setProgressBar(req, res, next){
	console.log("router_BrowserWindow.setProgressBar function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setProgressBar(req.body.progress)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setProgressBar couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window should have a shadow. On Windows and Linux does nothing.
function setHasShadow(req, res, next){
	console.log("router_BrowserWindow.setHasShadow function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setHasShadow(req.body.hasShadow)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setHasShadow couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window has a shadow. On Windows and Linux always returns true.
function hasShadow(req, res, next){
	console.log("router_BrowserWindow.hasShadow function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.hasShadow()
		res.status(200).json({'hasShadow':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.hasShadow couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets the region of the window to show as the thumbnail image displayed when hovering over the window in the taskbar. You can reset the thumbnail to be the entire window by specifying an empty region: {x: 0, y: 0, width: 0, height: 0}.
function setThumbnailClip(req, res, next){
	console.log("router_BrowserWindow.setThumbnailClip function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		var opts = {x: 0, y: 0, width: 0, height: 0}
		if(req.body.x!==0){opts.x=req.body.x}
		if(req.body.y!==0){opts.y=req.body.y}
		if(req.body.width!==0){opts.width=req.body.width}
		if(req.body.height!==0){opts.height=req.body.height}
		w.setThumbnailClip(opts)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setThumbnailClip couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets the toolTip that is displayed when hovering over the window thumbnail in the taskbar.
function setThumbnailToolTip(req, res, next){
	console.log("router_BrowserWindow.setThumbnailToolTip function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setThumbnailToolTip(req.body.tooltip)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setThumbnailToolTip couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Same as webContents.showDefinitionForSelection().
function showDefinitionForSelection(req, res, next){
	console.log("router_BrowserWindow.showDefinitionForSelection function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.showDefinitionForSelection()
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.showDefinitionForSelection couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window menu bar should hide itself automatically. Once set the menu bar will only show when users press the single Alt key.
function setAutoHideMenuBar(req, res, next){
	console.log("router_BrowserWindow.setAutoHideMenuBar function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setAutoHideMenuBar(req.body.hide)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setAutoHideMenuBar couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether menu bar automatically hides itself.
function isMenuBarAutoHide(req, res, next){
	console.log("router_BrowserWindow.isMenuBarAutoHide function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isMenuBarAutoHide()
		res.status(200).json({'isMenuBarAutoHide':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isMenuBarAutoHide couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the menu bar should be visible. If the menu bar is auto-hide, users can still bring up the menu bar by pressing the single Alt key.
function setMenuBarVisibility(req, res, next){
	console.log("router_BrowserWindow.setMenuBarVisibility function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setMenuBarVisibility(req.body.visible)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setMenuBarVisibility couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the menu bar is visible.
function isMenuBarVisible(req, res, next){
	console.log("router_BrowserWindow.isMenuBarVisible function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.isMenuBarVisible()
		res.status(200).json({'isMenuBarVisible':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.isMenuBarVisible couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets whether the window should be visible on all workspaces.
function SetVisibleOnAllWorkspaces(req, res, next){
	console.log("router_BrowserWindow.SetVisibleOnAllWorkspaces function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.SetVisibleOnAllWorkspaces(req.body.visible)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.SetVisibleOnAllWorkspaces couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns whether the window is visible on all workspaces.
function IsVisibleOnAllWorkspaces(req, res, next){
	console.log("router_BrowserWindow.IsVisibleOnAllWorkspaces function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds = false
	if(w!==null){
		bounds = w.IsVisibleOnAllWorkspaces()
		res.status(200).json({'IsVisibleOnAllWorkspaces':bounds})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.IsVisibleOnAllWorkspaces couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Makes the window ignore all mouse events.
function setIgnoreMouseEvents(req, res, next){
	console.log("router_BrowserWindow.setIgnoreMouseEvents function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setIgnoreMouseEvents(req.body.ignore)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setIgnoreMouseEvents couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Prevents the window contents from being captured by other apps.
function setContentProtection(req, res, next){
	console.log("router_BrowserWindow.setContentProtection function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setContentProtection(req.body.enable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setContentProtection couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Changes whether the window can be focused.
function setFocusable(req, res, next){
	console.log("router_BrowserWindow.setFocusable function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setFocusable(req.body.focusable)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setFocusable couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Sets parent as current window’s parent window, passing null will turn current window into a top-level window.
function setParentWindow(req, res, next){
	console.log("router_BrowserWindow.setParentWindow function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var parentWin = glec.getWindowById(req.body.parentID)
	if(w!==null){
		console.log("router found window id", w.id)
		console.log("router body=", req.body)
		w.setParentWindow(parentWin)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.setParentWindow couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns the parent window.
function getParentWindow(req, res, next){
	console.log("router_BrowserWindow.getParentWindow function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds
	if(w!==null){
		bounds = w.getParentWindow()
		res.status(200).json({'parentID':bounds.id})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getParentWindow couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns all child windows.
function getChildWindows(req, res, next){
	console.log("router_BrowserWindow.getChildWindows function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	var bounds
	var ids = []
	if(w!==null){
		bounds = w.getChildWindows()
		for (var i = bounds.length - 1; i >= 0; i--) {
			ids.push(bounds[i].id)
		}
		res.status(200).json({'childrenIDs':ids})
	}else{
		res.status(400).json({ error: "router_BrowserWindow.getChildWindows couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
//--------------------------------------------------------------
// BrowserWindow Instance Events
//--------------------------------------------------------------

/* WebContents has the following list of events:
page-title-updated returns event Event, title string
close
closed
unresponsive
responsive
blur
focus
show
hide
ready-to-show
maximize
unmaximize
minimize
restore
resize
move
moved
enter-full-screen
leave-full-screen
enter-html-full-screen
leave-html-full-screen
app-command returns event Event, command string
scroll-touch-begin
scroll-touch-end
scroll-touch-edge
swipe returns event Event, direction string
*/

// Registers an eventName to be listenned for
function listen4event(req, res, next){
	console.log("router_BrowserWindow.listen4event function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		if (req.params.eventName == "page-title-updated") {
			w.on(req.params.eventName, function(e, t){
				if(req.query.preventDefault=="true")e.preventDefault();
				glec.send2go('/event/browserwindow/'+w.id+"/"+req.params.eventName, "POST", {title: t, event: e})
			})
		}else if (req.params.eventName == "app-command") {
			w.on(req.params.eventName, function(e, c){
				if(req.query.preventDefault=="true")e.preventDefault();
				glec.send2go('/event/browserwindow/'+w.id+"/"+req.params.eventName, "POST", {cmd: c, event: e})
			})
		}else if (req.params.eventName == "swipe") {
			w.on(req.params.eventName, function(e, d){
				if(req.query.preventDefault=="true")e.preventDefault();
				glec.send2go('/event/browserwindow/'+w.id+"/"+req.params.eventName, "POST", {direction: d, event: e})
			})
		}else{
			w.on(req.params.eventName, function(e){
				if(req.query.preventDefault=="true"){
					e.preventDefault();
					console.log("preventDefault is true");
				}
				glec.send2go('/event/browserwindow/'+w.id+"/"+req.params.eventName, "POST", {event: e})
			})
		}
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.listen4event couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
// Returns an array of eventName currently listenned for
function unlisten4event(req, res, next){
	console.log("router_BrowserWindow.unlisten4event function. id=", req.params.idnum)
	var w = glec.getWindowById(req.params.idnum)
	if(w!==null){
		w.removeAllListeners(req.params.eventName)
		res.status(200)
	}else{
		res.status(400).json({ error: "router_BrowserWindow.unlisten4event couldnt find window id:"+req.params.idnum })
	}
	res.end()
}
