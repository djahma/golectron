package golectron

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// BrowserWindow stores an instance of a browser window, very much like
// a Chromium window. Use the NewBrowserWindow() and Destroy() functions
// to ensure synchronisation with JavaScript side, responsible for the
// actual management of these objects.
type BrowserWindow struct {
	// Internal fields
	inSync     bool
	eventMap   map[string]func(...interface{})
	elecClient *http.Client
	gelec      *Golectron
	webContent WebContents
	// Public fields
	Id         int         `json:"id"`
	WebContent WebContents `json:"webContents"`
}

// BrowserWindowOptions is used to set initial values in NewBrowserWindow
type BrowserWindowOptions struct {
	Width          int    `json:"width"`          //Window’s width in pixels. Default is 800.
	Height         int    `json:"height"`         //Window’s height in pixels. Default is 600.
	X              int    `json:"x"`              //(required if y is used) - Window’s left offset from screen. Default is 0.
	Y              int    `json:"y"`              //(required if x is used) - Window’s top offset from screen. Default is 0.
	UseContentSize bool   `json:"useContentSize"` //The width and height would be used as web page’s size, which means the actual window’s size will include window frame’s size and be slightly larger. Default is false.
	Center         bool   `json:"center"`         //Show window in the center of the screen.
	MinWidth       int    `json:"minWidth"`       //Window’s minimum width. Default is 0.
	MinHeight      int    `json:"minHeight"`      //Window’s minimum height. Default is 0.
	MaxWidth       int    `json:"maxWidth"`       //Window’s maximum width. Default is no limit.
	MaxHeight      int    `json:"maxHeight"`      //Window’s maximum height. Default is no limit.
	Resizable      bool   `json:"Resizable"`      //Whether window is resizable. Default is true.
	Movable        bool   `json:"movable"`        //Whether window is movable. This is not implemented on Linux. Default is true.
	Minimizable    bool   `json:"minimizable"`    //Whether window is minimizable. This is not implemented on Linux. Default is true.
	Maximizable    bool   `json:"maximizable"`    //Whether window is maximizable. This is not implemented on Linux. Default is true.
	Closable       bool   `json:"closable"`       //Whether window is closable. This is not implemented on Linux. Default is true.
	Focusable      bool   `json:"focusable"`      //Whether the window can be focused. Default is true. On Windows setting focusable: false also implies setting skipTaskbar: true. On Linux setting focusable: false makes the window stop interacting with wm, so the window will always stay on top in all workspaces.
	AlwaysOnTop    bool   `json:"alwaysOnTop"`    //Whether the window should always stay on top of other windows. Default is false.
	Fullscreen     bool   `json:"fullscreen"`     //Whether the window should show in fullscreen. When explicitly set to false the fullscreen button will be hidden or disabled on macOS. Default is false.
	Fullscreenable bool   `json:"fullscreenable"` //Whether the window can be put into fullscreen mode. On macOS, also whether the maximize/zoom button should toggle full screen mode or maximize window. Default is true.
	SkipTaskbar    bool   `json:"skipTaskbar"`    //Whether to show the window in taskbar. Default is false.
	Kiosk          bool   `json:"kiosk"`          //The kiosk mode. Default is false.
	Title          string `json:"title"`          //Default window title. Default is "Electron".
	//TODO: Icon NativeImage - The window icon. On Windows it is recommended to use ICO icons to get best visual effects, you can also leave it undefined so the executable’s icon will be used.
	Show  bool `json:"show"`  //Whether window should be shown when created. Default is true.
	Frame bool `json:"frame"` //Specify false to create a Frameless Window. Default is true.
	//TODO: Parent BrowserWindow - Specify parent window. Default is null.
	//maybe implementing a parentwindow.Id trick instead of the browserwindow object?
	Modal                  bool                 `json:"modal"`                  //Whether this is a modal window. This only works when the window is a child window. Default is false.
	AcceptFirstMouse       bool                 `json:"acceptFirstMouse"`       //Whether the web view accepts a single mouse-down event that simultaneously activates the window. Default is false.
	DisableAutoHideCursor  bool                 `json:"disableAutoHideCursor"`  //Whether to hide cursor when typing. Default is false.
	AutoHideMenuBar        bool                 `json:"autoHideMenuBar"`        //Auto hide the menu bar unless the Alt key is pressed. Default is false.
	EnableLargerThanScreen bool                 `json:"enableLargerThanScreen"` //Enable the window to be resized larger than screen. Default is false.
	BackgroundColor        string               `json:"backgroundColor"`        //Window’s background color as Hexadecimal value, like #66CD00 or #FFF or #80FFFFFF (alpha is supported). Default is #FFF (white).
	HasShadow              bool                 `json:"hasShadow"`              //Whether window should have a shadow. This is only implemented on macOS. Default is true.
	DarkTheme              bool                 `json:"darkTheme"`              //Forces using dark theme for the window, only works on some GTK+3 desktop environments. Default is false.
	Transparent            bool                 `json:"transparent"`            //Makes the window transparent. Default is false.
	Type                   string               `json:"type"`                   //The type of window, default is normal window. See more about this below.
	TitleBarStyle          string               `json:"titleBarStyle"`          //The style of window title bar. See more about this below.
	ThickFrame             bool                 `json:"thickFrame"`             //Use WS_THICKFRAME style for frameless windows on Windows, which adds standard window frame. Setting it to false will remove window shadow and window animations. Default is true.
	WebPreferences         WebPreferencesOption //Settings of web page’s features. See more about this below
}

// WebPreferencesOption ...
type WebPreferencesOption struct {
	NodeIntegration                bool       `json:"nodeIntegration"` //Whether node integration is enabled. Default is true.
	Preload                        string     `json:"preload"`         //Specifies a script that will be loaded before other scripts run in the page. This script will always have access to node APIs no matter whether node integration is turned on or off. The value should be the absolute file path to the script. When node integration is turned off, the preload script can reintroduce Node global symbols back to the global scope.
	Session                        Session    //Sets the session used by the page. Instead of passing the Session object directly, you can also choose to use the partition option instead, which accepts a partition string. When both session and partition are provided, session will be preferred. Default is the default session.
	Partition                      string     `json:"partition"`                      //Sets the session used by the page according to the session’s partition string. If partition starts with persist:, the page will use a persistent session available to all pages in the app with the same partition. If there is no persist: prefix, the page will use an in-memory session. By assigning the same partition, multiple pages can share the same session. Default is the default session.
	ZoomFactor                     float32    `json:"zoomFactor"`                     //The default zoom factor of the page, 3.0 represents 300%. Default is 1.0.
	Javascript                     bool       `json:"javascript"`                     //Enables JavaScript support. Default is true.
	WebSecurity                    bool       `json:"webSecurity"`                    //When false, it will disable the same-origin policy (usually using testing websites by people), and set allowDisplayingInsecureContent and allowRunningInsecureContent to true if these two options are not set by user. Default is true.
	AllowDisplayingInsecureContent bool       `json:"allowDisplayingInsecureContent"` //Allow an https page to display content like images from http URLs. Default is false.
	AllowRunningInsecureContent    bool       `json:"allowRunningInsecureContent"`    //Allow an https page to run JavaScript, CSS or plugins from http URLs. Default is false.
	Images                         bool       `json:"images"`                         //Enables image support. Default is true.
	TextAreasAreResizable          bool       `json:"textAreasAreResizable"`          //Make TextArea elements resizable. Default is true.
	Webgl                          bool       `json:"webgl"`                          //Enables WebGL support. Default is true.
	Webaudio                       bool       `json:"webaudio"`                       //Enables WebAudio support. Default is true.
	Plugins                        bool       `json:"plugins"`                        //Whether plugins should be enabled. Default is false.
	ExperimentalFeatures           bool       `json:"experimentalFeatures"`           //Enables Chromium’s experimental features. Default is false.
	ExperimentalCanvasFeatures     bool       `json:"experimentalCanvasFeatures"`     //Enables Chromium’s experimental canvas features. Default is false.
	ScrollBounce                   bool       `json:"scrollBounce"`                   //Enables scroll bounce (rubber banding) effect on macOS. Default is false.
	BlinkFeatures                  string     `json:"blinkFeatures"`                  //A list of feature strings separated by ,, like CSSVariables,KeyboardEventKey to enable. The full list of supported feature strings can be found in the RuntimeEnabledFeatures.in file.
	DisableBlinkFeatures           string     `json:"disableBlinkFeatures"`           //A list of feature strings separated by ,, like CSSVariables,KeyboardEventKey to disable. The full list of supported feature strings can be found in the RuntimeEnabledFeatures.in file.
	DefaultFontFamily              FontFamily //Sets the default font for the font-family.
	DefaultFontSize                int        `json:"defaultFontSize"`          //Defaults to 16.
	DefaultMonospaceFontSize       int        `json:"defaultMonospaceFontSize"` //Defaults to 13.
	MinimumFontSize                int        `json:"minimumFontSize"`          //Defaults to 0.
	DefaultEncoding                string     `json:"defaultEncoding"`          //Defaults to ISO-8859-1.
	BackgroundThrottling           bool       `json:"backgroundThrottling"`     //Whether to throttle animations and timers when the page becomes background. Defaults to true
}

// FontFamily sets the font to use in webPreference
type FontFamily struct {
	Standard  string `json:"standard"`  //Defaults to Times New Roman.
	Serif     string `json:"serif"`     //Defaults to Times New Roman.
	SansSerif string `json:"sansSerif"` //Defaults to Arial.
	Monospace string `json:"monospace"` //Defaults to Courier New.
}

//NewBrowserWindow creates a window on JS side and return a Go *BrowserWindow
// in sync with JS side.
//Takes a *BrowserWindow with basic options set, then set its internal fields,
//and intialize the window with electron
func (g *Golectron) NewBrowserWindow(windowOptions *BrowserWindowOptions) *BrowserWindow {
	jsData, err := json.Marshal(windowOptions)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow:", err)
	}
	win := &BrowserWindow{}
	win.elecClient = g.elecClient
	win.gelec = g
	req, _ := http.NewRequest("POST", "http://localhost/create/browserWindow", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := win.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR POST /create/BrowserWindow:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("NewBrowserWindow response=", resp.Status, string(r))
	if resp.StatusCode == 200 {
		win2 := &BrowserWindow{}
		if err := json.Unmarshal(r, win2); err != nil {
			fmt.Println("ERROR unmarshalling response:", err)
		}
		win.Id = win2.Id
		win.eventMap = make(map[string]func(...interface{}))
		g.windows = append(g.windows, win)
	}
	return win
}

// NewBrowserWindowOptions returns a BrowserWindowOptions initialised to its default values, instead of the Go zero values (false for bool, 0 for int, "" for string etc)
func (g *Golectron) NewBrowserWindowOptions() *BrowserWindowOptions {
	bwo := &BrowserWindowOptions{}
	bwo.Width = 800
	bwo.Height = 600
	bwo.UseContentSize = false
	bwo.Center = true
	bwo.MaxWidth = 1000000
	bwo.MaxHeight = 1000000
	bwo.Resizable = true
	bwo.Movable = true
	bwo.Minimizable = true
	bwo.Maximizable = true
	bwo.Closable = true
	bwo.Focusable = true
	bwo.AlwaysOnTop = false
	bwo.Fullscreen = false
	bwo.Fullscreenable = true
	bwo.SkipTaskbar = false
	bwo.Kiosk = false
	bwo.Title = "Golectron"
	//TODO: Icon NativeImage - The window icon. On Windows it is recommended to use ICO icons to get best visual effects, you can also leave it undefined so the executable’s icon will be used.
	bwo.Show = true
	bwo.Frame = true
	//TODO: Parent BrowserWindow - Specify parent window. Default is null.
	//maybe implementing a parentwindow.Id trick instead of the browserwindow object?
	bwo.Modal = false
	bwo.AcceptFirstMouse = false
	bwo.DisableAutoHideCursor = false
	bwo.AutoHideMenuBar = false
	bwo.EnableLargerThanScreen = false
	bwo.BackgroundColor = "#FFF"
	bwo.HasShadow = true
	bwo.DarkTheme = true
	bwo.Transparent = false
	bwo.Type = "normal"
	bwo.TitleBarStyle = "default"
	bwo.ThickFrame = true
	bwo.WebPreferences = WebPreferencesOption{}
	bwo.WebPreferences.NodeIntegration = true
	bwo.WebPreferences.ZoomFactor = 1.0
	bwo.WebPreferences.Javascript = true
	bwo.WebPreferences.WebSecurity = true
	bwo.WebPreferences.AllowDisplayingInsecureContent = false
	bwo.WebPreferences.AllowRunningInsecureContent = false
	bwo.WebPreferences.Images = true
	bwo.WebPreferences.TextAreasAreResizable = true
	bwo.WebPreferences.Webgl = true
	bwo.WebPreferences.Webaudio = true
	bwo.WebPreferences.Plugins = false
	bwo.WebPreferences.ExperimentalFeatures = false
	bwo.WebPreferences.ExperimentalCanvasFeatures = false
	bwo.WebPreferences.ScrollBounce = false
	bwo.WebPreferences.BlinkFeatures = ""
	bwo.WebPreferences.DisableBlinkFeatures = ""
	bwo.WebPreferences.DefaultFontSize = 16
	bwo.WebPreferences.DefaultMonospaceFontSize = 13
	bwo.WebPreferences.MinimumFontSize = 0
	bwo.WebPreferences.DefaultEncoding = "ISO-8859-1"
	bwo.WebPreferences.BackgroundThrottling = true
	return bwo
}

//----------------------------------------------------------------------------
// Instance Methods
// Objects created with NewBrowserWindow have the following instance methods:
//----------------------------------------------------------------------------
// GetWebContent returns the WebContents used by this window
func (b *BrowserWindow) GetWebContent() *WebContents {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getWebContent", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetWebContent function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	fmt.Println("BrowserWindow GetWebContent response=", resp.Status, string(r))
	data := struct {
		WcID int `json:"wcID"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetWebContent:", err)
		return nil
	}
	wc := b.gelec.newBlankWebContents()
	wc.Id = data.WcID
	return b.gelec.addWebContents(wc)
}

// Destroy force closing the window, the unload and beforeunload event won’t be emitted for the web page, and close event will also not be emitted for this window, but it guarantees the closed event will be emitted.
func (b *BrowserWindow) Destroy() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/destroy", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Destroy function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Destroy response=", resp.Status, string(r))
	var destroyedInd int
	for i := 0; i < len(b.gelec.windows); i++ {
		if b.gelec.windows[i].Id == b.Id {
			destroyedInd = i
			break
		}
	}
	wins := make([]*BrowserWindow, len(b.gelec.windows)-1)
	copy(wins, b.gelec.windows[:destroyedInd])
	copy(wins, b.gelec.windows[destroyedInd+1:])
	b.gelec.windows = wins
}

// Close Try to close the window. This has the same effect as a user manually clicking the close button of the window. The web page may cancel the close though. See the close event.
func (b *BrowserWindow) Close() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/close", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Close function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Close response=", resp.Status, string(r))
}

// Focus Focuses on the window.
func (b *BrowserWindow) Focus() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/focus", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Focus function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Focus response=", resp.Status, string(r))
}

// Blur Removes focus from the window.
func (b *BrowserWindow) Blur() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/blur", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Blur function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Blur response=", resp.Status, string(r))
}

// IsFocused Returns a boolean, whether the window is focused.
func (b *BrowserWindow) IsFocused() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isFocused", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsFocused function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsFocused bool `json:"isFocused"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsFocused:", err)
	}
	fmt.Println("BrowserWindow IsFocused response=", resp.Status, string(r))
	return data.IsFocused
}

// IsDestroyed Returns a boolean, whether the window is destroyed.
func (b *BrowserWindow) IsDestroyed() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isDestroyed", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsDestroyed function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsDestroyed bool `json:"isDestroyed"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsDestroyed:", err)
	}
	fmt.Println("BrowserWindow IsDestroyed response=", resp.Status, string(r))
	return data.IsDestroyed
}

// ShowIt Shows and gives focus to the window.
// Func name is modified from js API to avoid same field and function names
func (b *BrowserWindow) ShowIt() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/show", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Show function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow show response=", resp.Status, string(r))
}

// ShowInactive Shows the window but doesn’t focus on it.
func (b *BrowserWindow) ShowInactive() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/showInactive", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call ShowInactive function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow ShowInactive response=", resp.Status, string(r))
}

// HideIt Hides the window.
// Func name is modified from js API to avoid same field and function names
func (b *BrowserWindow) HideIt() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/hide", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Show function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow HideIt response=", resp.Status, string(r))
}

// IsVisible Returns a boolean, whether the window is visible to the user.
func (b *BrowserWindow) IsVisible() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isVisible", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsVisible function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsVisible bool `json:"isVisible"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsVisible:", err)
	}
	fmt.Println("BrowserWindow IsVisible response=", resp.Status, string(r))
	return data.IsVisible
}

// IsModal Returns a boolean, whether current window is a modal window.
func (b *BrowserWindow) IsModal() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isModal", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsModal function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsModal bool `json:"isModal"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsModal:", err)
	}
	fmt.Println("BrowserWindow IsModal response=", resp.Status, string(r))
	return data.IsModal
}

// Maximize Maximizes the window.
func (b *BrowserWindow) Maximize() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/maximize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Maximize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Maximize response=", resp.Status, string(r))
}

// Unmaximize Unmaximizes the window.
func (b *BrowserWindow) Unmaximize() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/unmaximize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Unmaximize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Unmaximize response=", resp.Status, string(r))
}

// IsMaximized Returns a boolean, whether the window is maximized.
func (b *BrowserWindow) IsMaximized() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMaximized", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMaximized function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMaximized bool `json:"isMaximized"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMaximized:", err)
	}
	fmt.Println("BrowserWindow IsMaximized response=", resp.Status, string(r))
	return data.IsMaximized
}

// Minimize Minimizes the window. On some platforms the minimized window will be shown in the Dock.
func (b *BrowserWindow) Minimize() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/minimize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Minimize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Minimize response=", resp.Status, string(r))
}

// Restore Restores the window from minimized state to its previous state.
func (b *BrowserWindow) Restore() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/restore", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Restore function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow Restore response=", resp.Status, string(r))
}

// IsMinimized Returns a boolean, whether the window is minimized.
func (b *BrowserWindow) IsMinimized() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMinimized", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMinimized function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMinimized bool `json:"isMinimized"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMinimized:", err)
	}
	fmt.Println("BrowserWindow IsMinimized response=", resp.Status, string(r))
	return data.IsMinimized
}

// SetFullScreen sets whether the window should be in fullscreen mode.
func (b *BrowserWindow) SetFullScreen(flag bool) {
	data := struct {
		Flag bool `json:"flag"`
	}{flag}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetFullScreen:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetFullScreen worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setFullScreen", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetFullScreen function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow SetFullScreen response=", resp.Status, string(r))
}

// IsFullScreen returns a boolean, whether the window is in fullscreen mode.
func (b *BrowserWindow) IsFullScreen() bool {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isFullScreen", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsFullScreen function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsFullScreen bool `json:"isFullScreen"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsFullScreen:", err)
	}
	return data.IsFullScreen
}

// SetAspectRatio (aspectRatio, width, height) macOS
// aspectRatio Float - The aspect ratio to maintain for some portion of the content view.
// extraSize Object (optional) - The extra size not to be included while maintaining the aspect ratio.
// width Integer
// height Integer
// This will make a window maintain an aspect ratio. The extra size allows a developer to have space, specified in pixels, not included within the aspect ratio calculations. This API already takes into account the difference between a window’s size and its content size.
// Consider a normal window with an HD video player and associated controls. Perhaps there are 15 pixels of controls on the left edge, 25 pixels of controls on the right edge and 50 pixels of controls below the player. In order to maintain a 16:9 aspect ratio (standard aspect ratio for HD @1920x1080) within the player itself we would call this function with arguments of 16/9 and [ 40, 50 ]. The second argument doesn’t care where the extra width and height are within the content view–only that they exist. Just sum any extra width and height areas you have within the overall content view.
// 0 value for width and height will be discarded
func (b *BrowserWindow) SetAspectRatio(aspectRatio float32, width, height int) {
	data := struct {
		AspectRatio float32 `json:"aspectRatio"`
		Width       int     `json:"width"`
		Height      int     `json:"height"`
	}{aspectRatio, width, height}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetAspectRatio:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setAspectRatio", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetAspectRatio function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow SetAspectRatio response=", resp.Status, string(r))
}

// SetBounds (options[, animate])
// options Object
// x Integer
// y Integer
// width Integer
// height Integer
// animate Boolean (optional) macOS
// Resizes and moves the window to width, height, x, y.
// 0 values for int parameters will be discarded
func (b *BrowserWindow) SetBounds(x, y, width, height int, animate bool) {
	data := struct {
		X       int  `json:"x"`
		Y       int  `json:"y"`
		Width   int  `json:"width"`
		Height  int  `json:"height"`
		Animate bool `json:"animate"`
	}{x, y, width, height, animate}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetBounds:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetBounds worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setBounds", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetBounds function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetBounds response:", resp.StatusCode, string(r))
	}
}

// GetBounds Returns window’s width, height, x and y values.
func (b *BrowserWindow) GetBounds() (width, height, x, y int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getBounds", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetBounds function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		X      int `json:"x"`
		Y      int `json:"y"`
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetBounds:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetBounds response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height, data.X, data.Y
}

// SetContentBounds (options[, animate])
// options Object
// x Integer
// y Integer
// width Integer
// height Integer
// animate Boolean (optional) macOS
// Resizes and moves the window’s client area (e.g. the web page) to width, height, x, y.
func (b *BrowserWindow) SetContentBounds(x, y, width, height int, animate bool) {
	data := struct {
		X       int  `json:"x"`
		Y       int  `json:"y"`
		Width   int  `json:"width"`
		Height  int  `json:"height"`
		Animate bool `json:"animate"`
	}{x, y, width, height, animate}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetContentBounds:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetContentBounds worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setContentBounds", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetContentBounds function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetContentBounds response:", resp.StatusCode, string(r))
	}
}

// GetContentBounds Returns an object that contains the window’s client area (e.g. the web page) width, height, x and y values.
func (b *BrowserWindow) GetContentBounds() (width, height, x, y int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getContentBounds", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetContentBounds function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		X      int `json:"x"`
		Y      int `json:"y"`
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetContentBounds:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetContentBounds response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height, data.X, data.Y
}

// SetSize (width, height[, animate])
// width Integer
// height Integer
// animate Boolean (optional) macOS
// Resizes the window to width and height.
func (b *BrowserWindow) SetSize(width, height int, animate bool) {
	data := struct {
		Width   int  `json:"width"`
		Height  int  `json:"height"`
		Animate bool `json:"animate"`
	}{width, height, animate}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetSize:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setSize", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetSize response:", resp.StatusCode, string(r))
	}
}

// GetSize Returns an array that contains window’s width and height.
func (b *BrowserWindow) GetSize() (width, height int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getSize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetSize:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetSize response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height
}

// SetContentSize (width, height[, animate])
// width Integer
// height Integer
// animate Boolean (optional) macOS
// Resizes the window’s client area (e.g. the web page) to width and height.
func (b *BrowserWindow) SetContentSize(width, height int, animate bool) {
	data := struct {
		Width   int  `json:"width"`
		Height  int  `json:"height"`
		Animate bool `json:"animate"`
	}{width, height, animate}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetContentSize:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setContentSize", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetContentSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetContentSize response:", resp.StatusCode, string(r))
	}
}

// GetContentSize Returns an array that contains window’s client area’s width and height.
func (b *BrowserWindow) GetContentSize() (width, height int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getContentSize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetContentSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetContentSize:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetContentSize response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height
}

// SetMinimumSize (width, height)
// width Integer
// height Integer
// Sets the minimum size of window to width and height.
func (b *BrowserWindow) SetMinimumSize(width, height int) {
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{width, height}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMinimumSize:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMinimumSize", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMinimumSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMinimumSize response:", resp.StatusCode, string(r))
	}
}

// GetMinimumSize Returns an array that contains window’s minimum width and height.
func (b *BrowserWindow) GetMinimumSize() (width, height int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getMinimumSize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetMinimumSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetMinimumSize:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetMinimumSize response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height
}

// SetMaximumSize (width, height)
// width Integer
// height Integer
// Sets the maximum size of window to width and height.
func (b *BrowserWindow) SetMaximumSize(width, height int) {
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{width, height}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMaximumSize:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMaximumSize", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMaximumSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMaximumSize response:", resp.StatusCode, string(r))
	}
}

// GetMaximumSize Returns an array that contains window’s maximum width and height.
func (b *BrowserWindow) GetMaximumSize() (width, height int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getMaximumSize", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetMaximumSize function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Width  int `json:"width"`
		Height int `json:"height"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetMaximumSize:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetMaximumSize response:", resp.StatusCode, string(r))
	}
	return data.Width, data.Height
}

// SetResizable Sets whether the window can be manually resized by user.
func (b *BrowserWindow) SetResizable(resizable bool) {
	data := struct {
		Resizable bool `json:"resizable"`
	}{resizable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetResizable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setResizable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetResizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetResizable response:", resp.StatusCode, string(r))
	}
}

// IsResizable Returns whether the window can be manually resized by user.
func (b *BrowserWindow) IsResizable() (isResizable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isResizable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsResizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsResizable bool `json:"isResizable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsResizable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsResizable response:", resp.StatusCode, string(r))
	}
	return data.IsResizable
}

// SetMovable (movable) macOS Windows
// movable Boolean
// Sets whether the window can be moved by user. On Linux does nothing.
func (b *BrowserWindow) SetMovable(movable bool) {
	data := struct {
		Movable bool `json:"movable"`
	}{movable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMovable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMovable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMovable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMovable response:", resp.StatusCode, string(r))
	}
}

// IsMovable macOS Windows
// Returns whether the window can be moved by user. On Linux always returns true.
func (b *BrowserWindow) IsMovable() (isMovable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMovable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMovable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMovable bool `json:"isMovable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMovable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsMovable response:", resp.StatusCode, string(r))
	}
	return data.IsMovable
}

// SetMinimizable (minimizable) macOS Windows
// minimizable Boolean
// Sets whether the window can be manually minimized by user. On Linux does nothing.
func (b *BrowserWindow) SetMinimizable(minimizable bool) {
	data := struct {
		Minimizable bool `json:"minimizable"`
	}{minimizable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMinimizable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMinimizable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMinimizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMinimizable response:", resp.StatusCode, string(r))
	}
}

// IsMinimizable Returns whether the window can be manually minimized by user. On Linux always returns true.
func (b *BrowserWindow) IsMinimizable() (isMinimizable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMinimizable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMinimizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMinimizable bool `json:"isMinimizable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMinimizable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsMinimizable response:", resp.StatusCode, string(r))
	}
	return data.IsMinimizable
}

// SetMaximizable (maximizable) macOS Windows
// maximizable Boolean
// Sets whether the window can be manually maximized by user. On Linux does nothing.
func (b *BrowserWindow) SetMaximizable(maximizable bool) {
	data := struct {
		Maximizable bool `json:"maximizable"`
	}{maximizable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMaximizable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMaximizable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMaximizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMaximizable response:", resp.StatusCode, string(r))
	}
}

// IsMaximizable macOS Windows
// Returns whether the window can be manually maximized by user. On Linux always returns true.
func (b *BrowserWindow) IsMaximizable() (isMaximizable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMaximizable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMaximizable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMaximizable bool `json:"isMaximizable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMaximizable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsMaximizable response:", resp.StatusCode, string(r))
	}
	return data.IsMaximizable
}

// SetFullScreenable (fullscreenable)
// fullscreenable Boolean
// Sets whether the maximize/zoom window button toggles fullscreen mode or maximizes the window.
func (b *BrowserWindow) SetFullScreenable(fullScreenable bool) {
	data := struct {
		FullScreenable bool `json:"fullScreenable"`
	}{fullScreenable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetFullScreenable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setFullScreenable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetFullScreenable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetFullScreenable response:", resp.StatusCode, string(r))
	}
}

// IsFullScreenable Returns whether the maximize/zoom window button toggles fullscreen mode or maximizes the window.
func (b *BrowserWindow) IsFullScreenable() (isFullScreenable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isFullScreenable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsFullScreenable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsFullScreenable bool `json:"isFullScreenable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsFullScreenable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsFullScreenable response:", resp.StatusCode, string(r))
	}
	return data.IsFullScreenable
}

// SetClosable (closable) macOS Windows
// closable Boolean
// Sets whether the window can be manually closed by user. On Linux does nothing.
func (b *BrowserWindow) SetClosable(closable bool) {
	data := struct {
		Closable bool `json:"closable"`
	}{closable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetClosable:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setClosable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetClosable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetClosable response:", resp.StatusCode, string(r))
	}
}

// IsClosable macOS Windows
// Returns whether the window can be manually closed by user. On Linux always returns true.
func (b *BrowserWindow) IsClosable() (isClosable bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isClosable", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsClosable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsClosable bool `json:"isClosable"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsClosable:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsClosable response:", resp.StatusCode, string(r))
	}
	return data.IsClosable
}

// SetAlwaysOnTop (flag bool)
// Sets whether the window should show always on top of other windows. After setting this, the window is still a normal window, not a toolbox window which can not be focused on.
func (b *BrowserWindow) SetAlwaysOnTop(alwaysOnTop bool) {
	data := struct {
		AlwaysOnTop bool `json:"alwaysOnTop"`
	}{alwaysOnTop}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetAlwaysOnTop:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setAlwaysOnTop", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetAlwaysOnTop function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetAlwaysOnTop response:", resp.StatusCode, string(r))
	}
}

// IsAlwaysOnTop Returns whether the window is always on top of other windows.
func (b *BrowserWindow) IsAlwaysOnTop() (isAlwaysOnTop bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isAlwaysOnTop", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsAlwaysOnTop function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsAlwaysOnTop bool `json:"isAlwaysOnTop"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsAlwaysOnTop:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsAlwaysOnTop response:", resp.StatusCode, string(r))
	}
	return data.IsAlwaysOnTop
}

// CenterIt Moves window to the center of the screen.
func (b *BrowserWindow) CenterIt() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/center", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Center function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow Center response:", resp.StatusCode, string(r))
	}
}

// SetPosition (x, y[, animate])
// x Integer
// y Integer
// animate Boolean (optional) macOS
// Moves window to x and y.
func (b *BrowserWindow) SetPosition(x, y int, animate bool) {
	data := struct {
		X       int  `json:"x"`
		Y       int  `json:"y"`
		Animate bool `json:"animate"`
	}{x, y, animate}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetPosition:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetPosition worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setPosition", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetPosition function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetPosition response:", resp.StatusCode, string(r))
	}
}

// GetPosition Returns an array that contains window’s current position.
func (b *BrowserWindow) GetPosition() (x, y int) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getPosition", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetPosition function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		X int `json:"x"`
		Y int `json:"y"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetPosition:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetPosition response:", resp.StatusCode, string(r))
	}
	return data.X, data.Y
}

// SetTitle Changes the title of native window to title.
func (b *BrowserWindow) SetTitle(title string) {
	data := struct {
		Title string `json:"title"`
	}{title}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetTitle:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setTitle", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetTitle function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetTitle response:", resp.StatusCode, string(r))
	}
}

// GetTitle Returns the title of the native window.
// Note: The title of web page can be different from the title of the native window.
func (b *BrowserWindow) GetTitle() (title string) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getTitle", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetTitle function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Title string `json:"title"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetTitle:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetTitle response:", resp.StatusCode, string(r))
	}
	return data.Title
}

// win.setSheetOffset(offsetY[, offsetX]) macOS
// offsetY Float
// offsetX Float (optional)
// Changes the attachment point for sheets on macOS. By default, sheets are attached just below the window frame, but you may want to display them beneath a HTML-rendered toolbar. For example:
// const {BrowserWindow} = require('electron')
// let win = new BrowserWindow()
// let toolbarRect = document.getElementById('toolbar').getBoundingClientRect()
// win.setSheetOffset(toolbarRect.height)

// FlashFrame (flag)
// flag Boolean
// Starts or stops flashing the window to attract user’s attention.
func (b *BrowserWindow) FlashFrame(flashFrame bool) {
	data := struct {
		FlashFrame bool `json:"flashFrame"`
	}{flashFrame}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.FlashFrame:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/flashFrame", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call FlashFrame function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow FlashFrame response:", resp.StatusCode, string(r))
	}
}

// SetSkipTaskbar (skip)
// skip Boolean
// Makes the window not show in the taskbar.
func (b *BrowserWindow) SetSkipTaskbar(skip bool) {
	data := struct {
		Skip bool `json:"skip"`
	}{skip}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetSkipTaskbar:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setSkipTaskbar", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetSkipTaskbar function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetSkipTaskbar response:", resp.StatusCode, string(r))
	}
}

// SetKiosk (flag)
// flag Boolean
// Enters or leaves the kiosk mode.
func (b *BrowserWindow) SetKiosk(flag bool) {
	data := struct {
		Flag bool `json:"flag"`
	}{flag}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetKiosk:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setKiosk", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetKiosk function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetKiosk response:", resp.StatusCode, string(r))
	}
}

// IsKiosk Returns whether the window is in kiosk mode.
func (b *BrowserWindow) IsKiosk() (isKiosk bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isKiosk", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsKiosk function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsKiosk bool `json:"isKiosk"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsKiosk:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsKiosk response:", resp.StatusCode, string(r))
	}
	return data.IsKiosk
}

// win.getNativeWindowHandle()
// Returns the platform-specific handle of the window as Buffer.
// The native type of the handle is HWND on Windows, NSView* on macOS, and Window (unsigned long) on Linux.

// HookWindowMessage (message, callback) Windows
// message Integer
// callback Function
// Hooks a windows message. The callback is called when the message is received in the WndProc.
func (b *BrowserWindow) HookWindowMessage(message int, callback func()) {
	data := struct {
		Message int `json:"message"`
	}{message}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.HookWindowMessage:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/hookWindowMessage", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call HookWindowMessage function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow HookWindowMessage response:", resp.StatusCode, string(r))
	}
	callback()
}

// IsWindowMessageHooked (message) Windows
// message Integer
// Returns true or false depending on whether the message is hooked.
func (b *BrowserWindow) IsWindowMessageHooked(message int) (isWindowMessageHooked bool) {
	data := struct {
		Message               int  `json:"message"`
		IsWindowMessageHooked bool `json:"isWindowMessageHooked"`
	}{message, false}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.IsWindowMessageHooked:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isWindowMessageHooked", bytes.NewBuffer(jsData))
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsWindowMessageHooked function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsWindowMessageHooked:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsWindowMessageHooked response:", resp.StatusCode, string(r))
	}
	return data.IsWindowMessageHooked
}

// UnhookWindowMessage (message) Windows
// message Integer
// Unhook the window message.
func (b *BrowserWindow) UnhookWindowMessage(message int) {
	data := struct {
		Message int `json:"message"`
	}{message}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.UnhookWindowMessage:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/unhookWindowMessage", bytes.NewBuffer(jsData))
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call UnhookWindowMessage function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow UnhookWindowMessage response:", resp.StatusCode, string(r))
	}
}

// UnhookAllWindowMessages Windows
// Unhooks all of the window messages.
func (b *BrowserWindow) UnhookAllWindowMessages() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/unhookAllWindowMessages", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call UnhookAllWindowMessages function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow UnhookAllWindowMessages response:", resp.StatusCode, string(r))
	}
}

// SetRepresentedFilename (filename) macOS
// filename String
// Sets the pathname of the file the window represents, and the icon of the file will show in window’s title bar.
func (b *BrowserWindow) SetRepresentedFilename(filename string) {
	data := struct {
		Filename string `json:"filename"`
	}{filename}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetRepresentedFilename:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setRepresentedFilename", bytes.NewBuffer(jsData))
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetRepresentedFilename function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetRepresentedFilename response:", resp.StatusCode, string(r))
	}
}

// GetRepresentedFilename macOS
// Returns the pathname of the file the window represents.
func (b *BrowserWindow) GetRepresentedFilename() (filename string) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getRepresentedFilename", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetRepresentedFilename function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Filename string `json:"filename"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetRepresentedFilename:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetRepresentedFilename response:", resp.StatusCode, string(r))
	}
	return data.Filename
}

// SetDocumentEdited macOS
// edited Boolean
// Specifies whether the window’s document has been edited, and the icon in title bar will become gray when set to true.
func (b *BrowserWindow) SetDocumentEdited(edited bool) {
	data := struct {
		Edited bool `json:"edited"`
	}{edited}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetDocumentEdited:", err)
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setDocumentEdited", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetDocumentEdited function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetDocumentEdited response:", resp.StatusCode, string(r))
	}
}

// IsDocumentEdited macOS
// Whether the window’s document has been edited.
func (b *BrowserWindow) IsDocumentEdited() (edited bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isDocumentEdited", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsDocumentEdited function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Edited bool `json:"edited"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsDocumentEdited:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsDocumentEdited response:", resp.StatusCode, string(r))
	}
	return data.Edited
}

// FocusOnWebView ()
func (b *BrowserWindow) FocusOnWebView() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/focusOnWebView", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call FocusOnWebView function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow FocusOnWebView response:", resp.StatusCode, string(r))
	}
}

// BlurWebView ()
func (b *BrowserWindow) BlurWebView() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/blurWebView", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call BlurWebView function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow BlurWebView response:", resp.StatusCode, string(r))
	}
}

// win.capturePage([rect, ]callback)
// Same as webContents.capturePage([rect, ]callback).

// LoadURL (url[, options])
// Same as webContents.loadURL(url[, options]).
// The url can be a remote address (e.g. http://) or a path to a local HTML file using the file:// protocol.
// To ensure that file URLs are properly formatted, it is recommended to use Node’s url.format method:
// let url = require('url').format({
//   protocol: 'file',
//   slashes: true,
//   pathname: require('path').join(__dirname, 'index.html')
// })
// win.loadURL(url)
func (b *BrowserWindow) LoadURL(url, httpReferrer, userAgent, extraHeaders string) {
	data := struct {
		Url          string `json:"url"`
		HttpReferrer string `json:"httpReferrer"`
		UserAgent    string `json:"userAgent"`
		ExtraHeaders string `json:"extraHeaders"`
	}{url, httpReferrer, userAgent, extraHeaders}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.LoadURL:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.LoadURL worked:", string(jsData))

	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/loadURL", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call LoadURL function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow LoadURL response=", resp.Status, string(r))
}

// Reload ()
// Same as webContents.reload.
func (b *BrowserWindow) Reload() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/reload", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Reload function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow Reload response:", resp.StatusCode, string(r))
	}
}

// win.setMenu(menu) Linux Windows
// menu Menu
// Sets the menu as the window’s menu bar, setting it to null will remove the menu bar.

// SetProgressBar (progress float64)
// Sets progress value in progress bar. Valid range is [0, 1.0].
// Remove progress bar when progress < 0; Change to indeterminate mode when progress > 1.
// On Linux platform, only supports Unity desktop environment, you need to specify the *.desktop file name to desktopName field in package.json. By default, it will assume app.getName().desktop.
func (b *BrowserWindow) SetProgressBar(progress float64) {
	data := struct {
		Progress float64 `json:"progress"`
	}{progress}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetProgressBar:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetProgressBar worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setProgressBar", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetProgressBar function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow SetProgressBar response=", resp.Status, string(r))
}

// win.setOverlayIcon(overlay, description) Windows
// overlay NativeImage - the icon to display on the bottom right corner of the taskbar icon. If this parameter is null, the overlay is cleared
// description String - a description that will be provided to Accessibility screen readers
// Sets a 16 x 16 pixel overlay onto the current taskbar icon, usually used to convey some sort of application status or to passively notify the user.

// SetHasShadow (hasShadow bool) macOS
// Sets whether the window should have a shadow. On Windows and Linux does nothing.
func (b *BrowserWindow) SetHasShadow(hasShadow bool) {
	data := struct {
		HasShadow bool `json:"hasShadow"`
	}{hasShadow}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetHasShadow:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetHasShadow worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setHasShadow", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetHasShadow function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("BrowserWindow SetHasShadow response=", resp.Status, string(r))
}

// HasAShadow () macOS
// Returns whether the window has a shadow. On Windows and Linux always returns true.
func (b *BrowserWindow) HasAShadow() (hasShadow bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/hasShadow", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call HasShadow function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		HasShadow bool `json:"hasShadow"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal HasShadow:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow HasShadow response:", resp.StatusCode, string(r))
	}
	return data.HasShadow
}

// win.setThumbarButtons(buttons) Windows
// buttons Array
// Add a thumbnail toolbar with a specified set of buttons to the thumbnail image of a window in a taskbar button layout. Returns a Boolean object indicates whether the thumbnail has been added successfully.

// The number of buttons in thumbnail toolbar should be no greater than 7 due to the limited room. Once you setup the thumbnail toolbar, the toolbar cannot be removed due to the platform’s limitation. But you can call the API with an empty array to clean the buttons.

// The buttons is an array of Button objects:

// Button Object
// icon NativeImage - The icon showing in thumbnail toolbar.
// click Function
// tooltip String (optional) - The text of the button’s tooltip.
// flags Array (optional) - Control specific states and behaviors of the button. By default, it is ['enabled'].
// The flags is an array that can include following Strings:

// enabled - The button is active and available to the user.
// disabled - The button is disabled. It is present, but has a visual state indicating it will not respond to user action.
// dismissonclick - When the button is clicked, the thumbnail window closes immediately.
// nobackground - Do not draw a button border, use only the image.
// hidden - The button is not shown to the user.
// noninteractive - The button is enabled but not interactive; no pressed button state is drawn. This value is intended for instances where the button is used in a notification.

// SetThumbnailClip (region) Windows
// region Object - Region of the window
// x Integer - x-position of region
// y Integer - y-position of region
// width Integer - width of region
// height Integer - height of region
// Sets the region of the window to show as the thumbnail image displayed when hovering over the window in the taskbar. You can reset the thumbnail to be the entire window by specifying an empty region: {x: 0, y: 0, width: 0, height: 0}.
func (b *BrowserWindow) SetThumbnailClip(x, y, width, height int) {
	data := struct {
		X      int `json:"x"`
		Y      int `json:"y"`
		Width  int `json:"width"`
		Height int `json:"height"`
	}{x, y, width, height}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetThumbnailClip:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetThumbnailClip worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setThumbnailClip", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetThumbnailClip function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetThumbnailClip response:", resp.StatusCode, string(r))
	}
}

// SetThumbnailToolTip (toolTip) Windows
// toolTip String
// Sets the toolTip that is displayed when hovering over the window thumbnail in the taskbar.
func (b *BrowserWindow) SetThumbnailToolTip(toolTip string) {
	data := struct {
		ToolTip string `json:"toolTip"`
	}{toolTip}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetThumbnailToolTip:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetThumbnailToolTip worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setThumbnailToolTip", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetThumbnailToolTip function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetThumbnailToolTip response:", resp.StatusCode, string(r))
	}
}

// ShowDefinitionForSelection macOS
// Same as webContents.showDefinitionForSelection().
func (b *BrowserWindow) ShowDefinitionForSelection() {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/showDefinitionForSelection", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call ShowDefinitionForSelection function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow ShowDefinitionForSelection response:", resp.StatusCode, string(r))
	}
}

// win.setIcon(icon) Windows Linux
// icon NativeImage
// Changes window icon.

// SetAutoHideMenuBar (hide)
// hide Boolean
// Sets whether the window menu bar should hide itself automatically. Once set the menu bar will only show when users press the single Alt key.
// If the menu bar is already visible, calling setAutoHideMenuBar(true) won’t hide it immediately.
func (b *BrowserWindow) SetAutoHideMenuBar(hide bool) {
	data := struct {
		Hide bool `json:"hide"`
	}{hide}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetAutoHideMenuBar:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetAutoHideMenuBar worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setAutoHideMenuBar", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetAutoHideMenuBar function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetAutoHideMenuBar response:", resp.StatusCode, string(r))
	}
}

// IsMenuBarAutoHide Returns whether menu bar automatically hides itself.
func (b *BrowserWindow) IsMenuBarAutoHide() (isMenuBarAutoHide bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMenuBarAutoHide", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMenuBarAutoHide function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMenuBarAutoHide bool `json:"isMenuBarAutoHide"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMenuBarAutoHide:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsMenuBarAutoHide response:", resp.StatusCode, string(r))
	}
	return data.IsMenuBarAutoHide
}

// SetMenuBarVisibility Sets whether the menu bar should be visible. If the menu bar is auto-hide, users can still bring up the menu bar by pressing the single Alt key.
func (b *BrowserWindow) SetMenuBarVisibility(visible bool) {
	data := struct {
		Visible bool `json:"visible"`
	}{visible}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetMenuBarVisibility:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetMenuBarVisibility worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setMenuBarVisibility", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetMenuBarVisibility function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetMenuBarVisibility response:", resp.StatusCode, string(r))
	}
}

// IsMenuBarVisible Returns whether the menu bar is visible.
func (b *BrowserWindow) IsMenuBarVisible() (isMenuBarVisible bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isMenuBarVisible", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsMenuBarVisible function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsMenuBarVisible bool `json:"isMenuBarVisible"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsMenuBarVisible:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsMenuBarVisible response:", resp.StatusCode, string(r))
	}
	return data.IsMenuBarVisible
}

// SetVisibleOnAllWorkspaces Sets whether the window should be visible on all workspaces.
// Note: This API does nothing on Windows.
func (b *BrowserWindow) SetVisibleOnAllWorkspaces(visible bool) {
	data := struct {
		Visible bool `json:"visible"`
	}{visible}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetVisibleOnAllWorkspaces:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetVisibleOnAllWorkspaces worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setVisibleOnAllWorkspaces", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetVisibleOnAllWorkspaces function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetVisibleOnAllWorkspaces response:", resp.StatusCode, string(r))
	}
}

// IsVisibleOnAllWorkspaces Returns whether the window is visible on all workspaces.
// Note: This API always returns false on Windows.
func (b *BrowserWindow) IsVisibleOnAllWorkspaces() (isVisibleOnAllWorkspaces bool) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/isVisibleOnAllWorkspaces", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call IsVisibleOnAllWorkspaces function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsVisibleOnAllWorkspaces bool `json:"isVisibleOnAllWorkspaces"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal IsVisibleOnAllWorkspaces:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow IsVisibleOnAllWorkspaces response:", resp.StatusCode, string(r))
	}
	return data.IsVisibleOnAllWorkspaces
}

// SetIgnoreMouseEvents Makes the window ignore all mouse events.
// All mouse events happened in this window will be passed to the window below this window, but if this window has focus, it will still receive keyboard events.
func (b *BrowserWindow) SetIgnoreMouseEvents(ignore bool) {
	data := struct {
		Ignore bool `json:"ignore"`
	}{ignore}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetIgnoreMouseEvents:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetIgnoreMouseEvents worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setIgnoreMouseEvents", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetIgnoreMouseEvents function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetIgnoreMouseEvents response:", resp.StatusCode, string(r))
	}
}

// SetContentProtection (enable) macOS Windows
// Prevents the window contents from being captured by other apps.
// On macOS it sets the NSWindow’s sharingType to NSWindowSharingNone. On Windows it calls SetWindowDisplayAffinity with WDA_MONITOR.
func (b *BrowserWindow) SetContentProtection(enable bool) {
	data := struct {
		Enable bool `json:"enable"`
	}{enable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetContentProtection:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetContentProtection worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setContentProtection", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetContentProtection function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetContentProtection response:", resp.StatusCode, string(r))
	}
}

// SetFocusable (focusable) Windows
// focusable Boolean
// Changes whether the window can be focused.
func (b *BrowserWindow) SetFocusable(focusable bool) {
	data := struct {
		Focusable bool `json:"focusable"`
	}{focusable}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetFocusable:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetFocusable worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setFocusable", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetFocusable function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetFocusable response:", resp.StatusCode, string(r))
	}
}

// SetParentWindow (parent) Linux macOS
// parent BrowserWindow
// Sets parent as current window’s parent window, passing null will turn current window into a top-level window.
func (b *BrowserWindow) SetParentWindow(parent *BrowserWindow) {
	data := struct {
		ParentID int `json:"parentID"`
	}{parent.Id}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling BrowserWindow.SetParentWindow:", err)
	} else {
		fmt.Println("marshalling BrowserWindow.SetParentWindow worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/setParentWindow", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call SetParentWindow function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow SetParentWindow response:", resp.StatusCode, string(r))
	}
}

// GetParentWindow Returns the parent window.
func (b *BrowserWindow) GetParentWindow() (parent *BrowserWindow) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getParentWindow", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetParentWindow function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		ParentID int `json:"parentID"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetParentWindow:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetParentWindow response:", resp.StatusCode, string(r))
		return nil
	}
	var parentWin *BrowserWindow
	for i := 0; i < len(b.gelec.windows); i++ {
		if b.gelec.windows[i].Id == data.ParentID {
			parentWin = b.gelec.windows[i]
			break
		}
	}
	return parentWin
}

// GetChildWindows Returns all child windows.
func (b *BrowserWindow) GetChildWindows() (children []*BrowserWindow) {
	req, _ := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/getChildWindows", nil)
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call GetChildWindows function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		ChildrenIDs []int `json:"childrenIDs"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to Unmarshal GetChildWindows:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in BrowserWindow GetChildWindows response:", resp.StatusCode, string(r))
		return nil
	}
	var childrenWins = make([]*BrowserWindow, len(data.ChildrenIDs))
	for _, val := range data.ChildrenIDs {
		for i := 0; i < len(b.gelec.windows); i++ {
			if b.gelec.windows[i].Id == val {
				childrenWins = append(childrenWins, b.gelec.windows[i])
				break
			}
		}
	}
	return childrenWins
}

/*
	BrowserWindow Eventer interface
*/

// On registers the func 'fn' to be called when eventName signal is received. Many events are emitted with an 'event' object, passed as JSON to the interface{} of the fn func.
/*
BrowserWindow has the following list of events:
	page-title-updated returns event Event, title String
	close returns event Event
	closed
	unresponsive
	responsive
	blur
	focus
	show
	hide
	ready-to-show
	maximize
	unmaximize
	minimize
	restore
	resize
	move
	moved macOS
	enter-full-screen
	leave-full-screen
	enter-html-full-screen
	leave-html-full-screen
	app-command Windows returns event Event, command string
	scroll-touch-begin macOS
	scroll-touch-end macOS
	swipe macOS returns event Event, direction String
*/
// Check http://electron.atom.io/docs/api/browser-window/ for more details
func (b *BrowserWindow) On(eventName string, preventDefault bool, fn func(...interface{})) {
	pd := "false"
	if preventDefault {
		pd = "true"
	}
	req, err := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/listen4event/"+eventName+"?preventDefault="+pd, nil)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed generate Listen4Event request:", err)
	}
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR BrowserWindow failed to Unmarshal Listen4Event error message:", err)
		}
		fmt.Println("ERROR BrowserWindow couldn't listen to event", eventName, ":", data.Error)
	} else {
		b.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (b *BrowserWindow) RemoveAllListeners(eventName string) {
	req, err := http.NewRequest("GET", "http://localhost/BrowserWindow/"+strconv.Itoa(b.Id)+"/unlisten4event/"+eventName, nil)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed generate RemoveAllListeners request:", err)
	}
	resp, err := b.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR BrowserWindow failed to call Unlisten4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR BrowserWindow failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR BrowserWindow couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(b.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (b *BrowserWindow) Emit(eventName string, data interface{}) {
	if val, ok := b.eventMap[eventName]; ok {
		val(data)
	} else {
		fmt.Println("ERROR: Received event in BrowserWindow is out of sync with Electron. EventName=", eventName)
	}
}

// EventList returns an array of event names currently registered.
func (b *BrowserWindow) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(b.eventMap))
	for key := range b.eventMap {
		evList = append(evList, key)
	}
	return evList
}
