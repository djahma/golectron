package golectron

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Menu is responsible for storing the menu items. It can then be displayed as an app menu with the SetAsApplicationMenu() function, or as context menu thanks to the Popup() function.
type Menu struct {
	elecClient *http.Client
	gelec      *Golectron
	Items      []*MenuItem `json:"items"`
	Id         int         `json:"id"`
}

// NewMenu creates a new *Menu, synchronised with JS Electron,
// and with its internal Fields set
func (g *Golectron) NewMenu() *Menu {
	m := &Menu{}
	m.elecClient = g.elecClient
	m.gelec = g
	req, _ := http.NewRequest("POST", "http://localhost/create/menu", nil)
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR /create/menu:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("NewMenu response=", resp.Status, string(r))
	if resp.StatusCode == 200 {
		if err := json.Unmarshal(r, m); err != nil {
			fmt.Println("ERROR unmarshalling response:", err)
		}
		g.menus = append(g.menus, m)
	}
	return m
}
func (g *Golectron) newMenuUnsynced(id int) *Menu {
	m := &Menu{}
	m.elecClient = g.elecClient
	m.gelec = g
	m.Id = id
	g.menus = append(g.menus, m)
	return m
}

//----------------------------------------------------------------------------
// Instance Methods
// Objects created with NewMenu have the following instance methods:
//----------------------------------------------------------------------------

//Destroy is the proper way to dispose of a Menu for it ensures the JS side remains in sync.
//It removes m Menu from the registry, its menuitems too, and their submenus as well. So watch out if you hold a reference to these submenu/menuitems as they will no longer be in sync with JS side and you will get errors upon invocation of their functions as their Ids will no longer point to anything.
//If m was the application menu it will remain active; you will have to call SetAsApplicationMenu on a different menu to change that.
func (m *Menu) Destroy() {
	req, _ := http.NewRequest("GET", "http://localhost/Menu/"+strconv.Itoa(m.Id)+"/destroy", nil)
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call Destroy function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu Destroy response=", resp.Status, string(r))
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Menu.Destroy response:", resp.StatusCode, string(r))
		return
	}
	data := struct {
		Menus     []int `json:"menus"`
		MenuItems []int `json:"menuitems"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Menu failed to Unmarshal Destroy response:", err)
	}
	for _, v := range data.Menus {
		m.gelec.RemoveMenuById(v)
	}
	// var destroyedInd int
	// for i := 0; i < len(m.gelec.windows); i++ {
	// 	if m.gelec.windows[i].Id == m.Id {
	// 		destroyedInd = i
	// 		break
	// 	}
	// }
	// wins := make([]*BrowserWindow, len(m.gelec.windows)-1)
	// copy(wins, m.gelec.windows[:destroyedInd])
	// copy(wins, m.gelec.windows[destroyedInd+1:])
	// m.gelec.windows = wins
}

//SetAsApplicationMenu sets m *Menu as the application menu on macOS. On Windows and Linux, the menu will be set as each window’s top menu.
func (m *Menu) SetAsApplicationMenu() {
	req, _ := http.NewRequest("GET", "http://localhost/Menu/"+strconv.Itoa(m.Id)+"/setApplicationMenu", nil)
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call SetApplicationMenu function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.SetApplicationMenu response=", resp.Status, string(r))
}

//MenuGetApplicationMenu returns the application *Menu, if set, or nil, if not set.
func (g *Golectron) MenuGetApplicationMenu() *Menu {
	req, _ := http.NewRequest("GET", "http://localhost/Menu/getApplicationMenu", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call GetApplicationMenu function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		MenuID int `json:"menuID"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Menu failed to Unmarshal GetApplicationMenu:", err)
	}
	for _, mtemp := range g.menus {
		if mtemp.Id == data.MenuID {
			return mtemp
		}
	}
	fmt.Println("Menu.GetApplicationMenu response=", resp.Status, string(r))
	return nil
}

//MenuSendActionToFirstResponder (action) macOS
//action String
//Sends the action to the first responder of application. This is used for emulating default Cocoa menu behaviors, usually you would just use the role property of MenuItem.
//See the macOS Cocoa Event Handling Guide for more information on macOS’ native actions.
func (g *Golectron) MenuSendActionToFirstResponder(action string) {
	data := struct {
		Action string `json:"action"`
	}{action}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.SendActionToFirstResponder:", err)
	} else {
		fmt.Println("marshalling Menu.SendActionToFirstResponder worked:", string(jsData))
	}
	req, _ := http.NewRequest("GET", "http://localhost/Menu/sendActionToFirstResponder", bytes.NewBuffer(jsData))
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call SendActionToFirstResponder function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.SendActionToFirstResponder response=", resp.Status, string(r))
}

// MenuBuildFromTemplate (template)
//template Array
//Generally, the template is just an array of options for constructing a MenuItem. The usage can be referenced above.
//You can also attach other fields to the element of the template and they will become properties of the constructed menu items.
func (g *Golectron) MenuBuildFromTemplate(templateJS []byte) *Menu {
	var data map[string]interface{}
	err := json.Unmarshal(templateJS, &data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.BuildFromTemplate:", err)
	} else {
		fmt.Println("data=", data)
	}
	// data := struct {
	// 	TemplateJS string `json:"templateJS"`
	// }{string(templateJS)}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.BuildFromTemplate:", err)
	} else {
		fmt.Println("marshalling Menu.BuildFromTemplate worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Menu/buildFromTemplate", bytes.NewBuffer(templateJS))
	// req, _ := http.NewRequest("POST", "http://localhost/Menu/buildFromTemplate", bytes.NewBuffer(jsData))
	// req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Content-Type", "application/javascript")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call BuildFromTemplate function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)

	data2 := struct {
		Menu map[string]interface{}
		Err  string `json:"error"`
	}{}
	fmt.Println("Menu.BuildFromTemplate response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data2.Menu)
	if err != nil {
		fmt.Println("MenuBuildFromTemplate failed unmarshalling:", err)
		return nil
	}
	fmt.Println("Marshalling succeeded; data2=", data2)
	if resp.StatusCode != 200 {
		fmt.Println("MenuBuildFromTemplate failed with error:", data2.Err)
		return nil
	}

	mi := g.buildFromTemplate(data2.Menu)
	if m, ok := (mi).(*Menu); ok {
		return m
	}
	return nil
}

// buildFromTemplate builds Menu tree from a tree of IDs returned by JS side
// returns either a *Menu or a *MenuItem, or nil in case of error
func (g *Golectron) buildFromTemplate(o map[string]interface{}) interface{} {
	if i, ok := o["menuID"]; ok {
		mu := g.newMenuUnsynced(int((i).(float64)))
		items := (o["items"]).([]interface{})
		for _, k := range items {
			mi := g.buildFromTemplate((k).(map[string]interface{}))
			if m, ok := (mi).(*MenuItem); ok {
				mu.Items = append(mu.Items, m)
			}
		}
		g.menus = append(g.menus, mu)
		return mu
	} else if v, ok := o["menuitemID"]; ok {
		miu := g.newMenuItemUnsynced(int((v).(float64)))
		submenu, exist := (o["submenu"]).(map[string]interface{})
		if exist {
			smi := g.buildFromTemplate(submenu)
			if sm, ok := (smi).(*Menu); ok {
				miu.SubMenu = sm
			}
		}
		g.menuitems = append(g.menuitems, miu)
		return miu
	}
	return nil
}

// Popup ([browserWindow, x, y])
// browserWindow BrowserWindow (optional) - Default is BrowserWindow.getFocusedWindow().
// x Number (optional = set to -1) - Default is the current mouse cursor position.
// y Number (optional = set to -1) - Default is the current mouse cursor position.
// Pops up this menu as a context menu in the browserWindow.
func (m *Menu) Popup(browserWindow *BrowserWindow, x, y int) {
	data := struct {
		BrowserWindow int `json:"browserWindowID"`
		X             int `json:"x"`
		Y             int `json:"y"`
	}{}
	if browserWindow == nil {
		data.BrowserWindow = -1
	} else {
		data.BrowserWindow = browserWindow.Id
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.Popup:", err)
	} else {
		fmt.Println("marshalling Menu.Popup worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Menu/"+strconv.Itoa(m.Id)+"/popup", bytes.NewBuffer(jsData))
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call Popup function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.Popup response=", resp.Status, string(r))
}

// Append (menuItem)
// menuItem MenuItem
// Appends the menuItem to the menu.
func (m *Menu) Append(menuitem *MenuItem) {
	data := struct {
		MenuitemID int `json:"menuitemID"`
	}{menuitem.MenuItemID}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.Append:", err)
	} else {
		fmt.Println("marshalling Menu.Append worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Menu/"+strconv.Itoa(m.Id)+"/append", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call Append function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.Append response=", resp.Status, string(r))
}

// Insert (pos, menuItem)
// pos Integer
// menuItem MenuItem
// Inserts the menuItem to the pos position of the menu.
func (m *Menu) Insert(pos int, menuitem *MenuItem) {
	data := struct {
		Pos      int       `json:"pos"`
		Menuitem *MenuItem `json:"menuitem"`
	}{pos, menuitem}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.Insert:", err)
	} else {
		fmt.Println("marshalling Menu.Insert worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Menu/"+strconv.Itoa(m.Id)+"/insert", bytes.NewBuffer(jsData))
	resp, err := m.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Menu failed to call Insert function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.Insert response=", resp.Status, string(r))
}

//MenuItem is responsible for storing properties of one menu item
type MenuItem struct {
	elecClient *http.Client
	gelec      *Golectron
	eventMap   map[string]func(interface{})
	// Public Fields
	MenuItemID int `json:"menuitemID"`
	SubMenu    *Menu
}

// MenuItemOptions holds the MenuItem properties
type MenuItemOptions struct {
	Role     string `json:"role"` //Define the action of the menu item, when specified the click property will be ignored.
	Type     string `json:"type"` //Can be normal, separator, submenu, checkbox or radio.
	Label    string `json:"label"`
	Sublabel string `json:"sublabel"`
	// Accelerator Accelerator
	// Icon        NativeImage `json:"icon"`
	Enabled bool `json:"enabled"` //If false, the menu item will be greyed out and unclickable.
	Visible bool `json:"visible"` //If false, the menu item will be entirely hidden.
	Checked bool `json:"checked"` //Should only be specified for checkbox or radio type menu items.
	// Submenu  Menu   `json:"submenu"`  //Should be specified for submenu type menu items. If submenu is specified, the type: 'submenu' can me omitted. If the value is not a Menu then it will be automatically converted to one using Menu.buildFromTemplate.
	Id       int    `json:"id"`       //Unique within a single menu. If defined then it can be used as a reference to this item by the position attribute.
	Position string `json:"position"` //This field allows fine-grained definition of the specific location within a given menu.
}

// MenuItemClickEv stores info passed via the 'click' event
type MenuItemClickEv struct {
	BrowserWindowID int
	Data            interface{}
}

// NewMenuItem creates and sync a new *MenuItem with golectron.
// mi parameter is to set initial fields, eg:
// mi := &MenuItem{label: "MyMenu action", Type:"normal", etc}
// miSynced = g.NewMenuItem(mi)
//The click event is received through the ClickCh channel
func (g *Golectron) NewMenuItem(mio *MenuItemOptions) *MenuItem {
	data := struct {
		MenuItemOptions *MenuItemOptions `json:"menuitemoptions"`
	}{mio}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling MenuItem:", err)
	}
	mi := &MenuItem{}
	mi.elecClient = g.elecClient
	mi.gelec = g
	req, _ := http.NewRequest("POST", "http://localhost/create/menuitem", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := mi.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR POST /create/menuitem:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("NewMenuItem response=", resp.Status, string(r))
	if resp.StatusCode == 200 {
		if err := json.Unmarshal(r, mi); err != nil {
			fmt.Println("ERROR unmarshalling response:", err)
			return nil
		}
		mi.eventMap = make(map[string]func(interface{}))
		// mi.ClickCh = make(chan ClickResponse)
		g.menuitems = append(g.menuitems, mi)
	}
	return mi
}

// newMenuItemUnsynced creates a MenuItem on Go side, unsynced with JS side
func (g *Golectron) newMenuItemUnsynced(id int) *MenuItem {
	m := &MenuItem{}
	m.elecClient = g.elecClient
	m.gelec = g
	m.MenuItemID = id
	g.menuitems = append(g.menuitems, m)
	return m
}

// MenuItemBuildFromTemplate (templateJS)
//templateJS []byte: holds the content - presumably from a file - describing a javascript menuitem object. Example:
// 	{
//    	label: 'Journal View',
//    	accelerator: (function() {
//       	if (process.platform == 'darwin')
//      		    return 'CmdOrCtrl+X';
//       	else
//          	return 'CmdOrCtrl+J';
//    	})(),
//    	click: function(item, focusedWindow) {
//       	if (focusedWindow) console.log("Journal menuitem clicked!");
//    	}
// 	}
func (g *Golectron) MenuItemBuildFromTemplate(templateJS []byte) *MenuItem {
	req, _ := http.NewRequest("POST", "http://localhost/MenuItem/buildFromTemplate", bytes.NewBuffer(templateJS))
	req.Header.Set("Content-Type", "application/javascript")
	resp, err := g.elecClient.Do(req)
	if err != nil {
	}
	r, _ := ioutil.ReadAll(resp.Body)

	data2 := struct {
		MenuItemID int    `json:"menuitemID"`
		Err        string `json:"error"`
	}{}
	fmt.Println("MenuItem.BuildFromTemplate response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data2)
	if err != nil {
		fmt.Println("MenuItemBuildFromTemplate failed unmarshalling:", err)
		return nil
	}
	fmt.Println("Marshalling succeeded; data2=", data2)
	if resp.StatusCode != 200 {
		fmt.Println("MenuItemBuildFromTemplate failed with error:", data2.Err)
		return nil
	}

	mi := g.newMenuItemUnsynced(data2.MenuItemID)
	if mi == nil {
		return nil
	}
	return mi
}

// GetMenuItemOptions returns the *MenuItemOptions describing mi
func (mi *MenuItem) GetMenuItemOptions() *MenuItemOptions {
	req, _ := http.NewRequest("GET", "http://localhost/MenuItem/"+strconv.Itoa(mi.MenuItemID)+"/getMenuItemOptions", nil)
	resp, err := mi.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR MenuItem failed to call getMenuItemOptions function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.GetMenuItemOptions response=", resp.Status, string(r))
	data2 := struct {
		Mi  MenuItemOptions `json:"options"`
		Err string          `json:"error"`
	}{}
	err = json.Unmarshal(r, &data2)
	if err != nil {
		fmt.Println("GetMenuItemOptions failed unmarshalling:", err)
		return nil
	}
	if resp.StatusCode != 200 {
		fmt.Println("GetMenuItemOptions failed with error:", data2.Err)
		return nil
	}
	return &data2.Mi
}

// SetMenuItemOptions sets mio the MenuItemOptions to JS side MenuItem
// It is best to call GetMenuItemOptions first and edit its returned value and then re-Set it: this allows to avoid Go default values to be set accidently.
func (mi *MenuItem) SetMenuItemOptions(mio MenuItemOptions) error {
	jsData, err := json.Marshal(mio)
	if err != nil {
		fmt.Println("ERROR marshalling Menu.SetMenuItemOptions:", err)
	} else {
		fmt.Println("marshalling Menu.SetMenuItemOptions worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/MenuItem/"+strconv.Itoa(mi.MenuItemID)+"/setMenuItemOptions", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := mi.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR MenuItem failed to call SetMenuItemOptions function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.SetMenuItemOptions response=", resp.Status, string(r))
	if resp.StatusCode != 200 {
		data2 := struct {
			Err string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data2)
		if err != nil {
			fmt.Println("SetMenuItemOptions failed unmarshalling:", err)
			return errors.New(err.Error())
		}
		fmt.Println("SetMenuItemOptions failed with error:", data2.Err)
		return errors.New(data2.Err)
	}
	return nil
}

func (mi *MenuItem) GetSubmenu() *Menu {
	req, _ := http.NewRequest("GET", "http://localhost/MenuItem/"+strconv.Itoa(mi.MenuItemID)+"/getSubmenu", nil)
	resp, err := mi.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR MenuItem failed to call GetMenuItemSubmenu function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Menu.GetMenuItemSubmenu response=", resp.Status, string(r))
	data2 := struct {
		SubID int    `json:"submenuID"`
		Err   string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data2)
	if err != nil {
		fmt.Println("GetMenuItemSubmenu failed unmarshalling:", err)
		return nil
	}
	if resp.StatusCode != 200 {
		fmt.Println("GetMenuItemSubmenu failed with error:", data2.Err)
		return nil
	}
	if mi.gelec.GetMenuByID(data2.SubID) == nil {
		tempMenu := mi.gelec.newMenuUnsynced(data2.SubID)
		mi.gelec.menus = append(mi.gelec.menus, tempMenu)
		return tempMenu
	}
	return mi.gelec.GetMenuByID(data2.SubID)
}

/*
	MenuItem Eventer interface
*/

// On registers the func 'fn' to be called when eventName signal is received
// MenuItem has only one event you can listen to: 'click'. Also, when the 'Role' field is set, the 'click' event never trigger. 'Role' can only be set in NewMenuItem(mio). Subsequent modifications of this field have no effect.
func (mi *MenuItem) On(eventName string, fn func(interface{})) {
	mi.eventMap["click"] = fn
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (mi *MenuItem) RemoveAllListeners(eventName string) {
	delete(mi.eventMap, "click")
}

// Emit calls the func associated with the eventName received.
func (mi *MenuItem) Emit(eventName string, data interface{}) {
	if val, ok := mi.eventMap["click"]; ok {
		val(data)
	} else {
		fmt.Println("ERROR: Received event in MenuItem is out of sync with Electron. EventName=", eventName)
	}
}

// EventList returns an array of event names currently registered.
// On a MenuItem, there should only be one name (click) at most.
func (mi *MenuItem) EventList() []string {
	evList := make([]string, len(mi.eventMap))
	for key := range mi.eventMap {
		evList = append(evList, key)
	}
	return evList
}
