package golectron

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Protocol implements the protocol methods to manage app or session protocols
type Protocol struct {
	// Private
	elecClient   *http.Client
	golectronCtx *Golectron
	eventMap     map[string]func(...interface{}) interface{}
	//Public
	SessionID int
}

// RegisterStandardSchemes Registering a scheme as standard will allow access to files through the FileSystem API. Otherwise the renderer will throw a security error for the scheme.
// Note: This method can only be used before the ready event of the app module gets emitted.
func (p *Protocol) RegisterStandardSchemes(schemes []string) {
	data := struct {
		Schemes []string `json:"schemes"`
		Err     string   `json:"error"`
	}{Schemes: schemes}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterStandardSchemes:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterStandardSchemes worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerStandardSchemes", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterStandardSchemes function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterStandardSchemes response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterStandardSchemes:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterStandardSchemes:", resp.StatusCode, data.Err)
	}
}

// RegisterServiceWorkerSchemes - Custom schemes to be registered to handle service workers.
func (p *Protocol) RegisterServiceWorkerSchemes(schemes []string) {
	data := struct {
		Schemes []string `json:"schemes"`
		Err     string   `json:"error"`
	}{Schemes: schemes}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterServiceWorkerSchemes:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterServiceWorkerSchemes worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerServiceWorkerSchemes", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterServiceWorkerSchemes function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterServiceWorkerSchemes response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterServiceWorkerSchemes:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterServiceWorkerSchemes:", resp.StatusCode, data.Err)
	}
}

// RegisterFileProtocol Registers a protocol of scheme that will send the file as a response. The handler will be called with handler(request, callback) when a request is going to be created with scheme. completion will be called with completion(null) when scheme is successfully registered or completion(error) when failed.
func (p *Protocol) RegisterFileProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterFileProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterFileProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerFileProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterFileProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterFileProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterFileProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterFileProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}

// ProtocolRequest holds request data sent to handler in FileProtocol
type ProtocolRequest struct {
	Url        string            `json:"url"`
	Referrer   string            `json:"referrer"`
	Method     string            `json:"method"`
	UploadData []UploadDataStore `json:"uploadData"`
}

// RegisterBufferProtocol The usage is the same with registerFileProtocol, except that the callback should be called with either a Buffer object or an object that has the data, mimeType, and charset properties.
func (p *Protocol) RegisterBufferProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterBufferProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterBufferProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerBufferProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterBufferProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterBufferProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterBufferProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterBufferProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}

// RegisterStringProtocol The usage is the same with registerFileProtocol, except that the callback should be called with either a String or an object that has the data, mimeType, and charset properties.
func (p *Protocol) RegisterStringProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterStringProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterStringProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerStringProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterStringProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterStringProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterStringProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterStringProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}

// RegisterHttpProtocol The usage is the same with registerFileProtocol, except that the callback should be called with a redirectRequest object that has the url, method, referrer, uploadData and session properties.
func (p *Protocol) RegisterHttpProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.RegisterHttpProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.RegisterHttpProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/registerHttpProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call RegisterHttpProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol RegisterHttpProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.RegisterHttpProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.RegisterHttpProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}

type ProtocolRedirectRequest struct {
	Url        string                            `json:"url"`
	Method     string                            `json:"method"`
	Referrer   string                            `json:"referrer"`
	UploadData ProtocolRedirectRequestUploadData `json:"uploadData"`
}

type ProtocolRedirectRequestUploadData struct {
	ContentType string `json:"contentType"` // MIME type of the content.
	Data        string `json:"data"`        // Content to be sent.
}

// UnregisterProtocol Unregisters the custom protocol of scheme.
func (p *Protocol) UnregisterProtocol(scheme string) {
	delete(p.eventMap, scheme)
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.UnregisterProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.UnregisterProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/unregisterProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call UnregisterProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol UnregisterProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.UnregisterProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.UnregisterProtocol:", resp.StatusCode, data.Err)
	}
}

// IsProtocolHandled indicates whether there is already a handler for scheme.
func (p *Protocol) IsProtocolHandled(scheme string) bool {
	delete(p.eventMap, scheme)
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
		Resp   bool   `json:"resp"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.IsProtocolHandled:", err)
	} else {
		fmt.Println("marshalling Protocol.IsProtocolHandled worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/isProtocolHandled", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call IsProtocolHandled function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol IsProtocolHandled response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.IsProtocolHandled:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.IsProtocolHandled:", resp.StatusCode, data.Err)
	}
	return data.Resp
}

func (p *Protocol) InterceptFileProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.InterceptFileProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.InterceptFileProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/interceptFileProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call InterceptFileProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol InterceptFileProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.InterceptFileProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.InterceptFileProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}
func (p *Protocol) InterceptBufferProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.InterceptBufferProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.InterceptBufferProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/interceptBufferProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call InterceptBufferProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol InterceptBufferProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.InterceptBufferProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.InterceptBufferProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}
func (p *Protocol) InterceptStringProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.InterceptStringProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.InterceptStringProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/interceptStringProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call InterceptStringProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol InterceptStringProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.InterceptStringProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.InterceptStringProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}
func (p *Protocol) InterceptHttpProtocol(scheme string, fn func(...interface{}) interface{}) error {
	p.eventMap[scheme] = fn
	data := struct {
		Scheme string `json:"scheme"`
		Err    string `json:"error"`
	}{Scheme: scheme}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Protocol.InterceptHttpProtocol:", err)
	} else {
		fmt.Println("marshalling Protocol.InterceptHttpProtocol worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/Session/"+strconv.Itoa(p.SessionID)+"/protocol/interceptHttpProtocol", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := p.golectronCtx.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Protocol failed to call InterceptHttpProtocol function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Protocol InterceptHttpProtocol response=", resp.Status, string(r))
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR unmarshalling Protocol.InterceptHttpProtocol:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Protocol.InterceptHttpProtocol:", resp.StatusCode, data.Err)
		delete(p.eventMap, scheme)
		return errors.New(data.Err)
	}
	return nil
}
