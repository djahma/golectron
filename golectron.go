//Package golectron provides an Electron.io interface in Go.
//It is a wrapper for electron nodeJS API, using http servers for interprocess communication and synchronisation of objects across both worlds.
package golectron

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
)

// Golectron struct is responsible for holding a Go clone of Electron JS objects and communicating with Electron through an http server.
type Golectron struct {
	//internal vars
	golecReadyCh     chan int
	golecErrorCh     chan string
	elecCmd          *exec.Cmd
	elecClient       *http.Client
	router           *httprouter.Router
	pathGolectron    string
	pathGolectronApp string
	pathElectron     string
	eventMap         map[string]func(interface{})
	//config.json vars
	Version             string `json:"version"`
	ElectronServerPort  string `json:"electronServerPort"`
	GolectronServerPort string `json:"golectronServerPort"`
	//API vars
	QCh         chan int
	windows     []*BrowserWindow
	menus       []*Menu
	menuitems   []*MenuItem
	webContents []*WebContents
	sessions    []*Session
}

//NewGolectron creates a Golectron context, starts Electron, initialise the http servers on both sides.
//It will look for an Electron installation in source golectron folder (folder containing electron binary must have a name that start by 'electron'), then in PATH.
func NewGolectron() (g *Golectron, retErr error) {
	defer func() {
		if r := recover(); r != nil {
			retErr = errors.New(r.(string))
			fmt.Println(retErr)
			panic(retErr)
		}
	}()

	golec := &Golectron{}
	golec.QCh = make(chan int, 1)
	// Fill the paths: pathGolectron, pathGolectronApp, pathElectron
	if err := golec.allocatePaths(); err != nil {
		return nil, err
	}
	// Parse any config.json file if any
	if err := golec.parseConfig(); err != nil {
		// 0 will ensure the system will allocate a free port
		golec.GolectronServerPort = strconv.Itoa(0)
	}
	// Start golectron server to listen for requests from golectronApp
	fmt.Println("creating golectron listening server")
	golec.router = httprouter.New()
	golec.setRoutes()

	golecServer := &http.Server{
		Handler: golec.router,
		// ReadTimeout:    10 * time.Second,
		// WriteTimeout:   10 * time.Second,
		// MaxHeaderBytes: 1 << 20,
	}
	l, err := net.Listen("tcp", "127.0.0.1:"+golec.GolectronServerPort)
	if err != nil {
		return nil, errors.New("ERROR in golectron while creating listener")
	}
	psss := strings.Split(l.Addr().String(), ":")
	fmt.Println("psss=", psss, golec.GolectronServerPort, psss[1])
	golec.GolectronServerPort = psss[1]
	fmt.Println("golec.GolectronServerPort =", psss[1])
	go func() {
		defer l.Close()
		fmt.Println("GOLECTRON is serving")
		err = golecServer.Serve(l)
		if err != nil {
			panic("ERROR serving in GOLECTRON:" + err.Error())
		}
	}()
	//start electron into elecCmd
	golec.golecReadyCh = make(chan int, 1)
	golec.golecErrorCh = make(chan string, 1)
	golec.elecCmd = exec.Command(golec.pathElectron, golec.pathGolectronApp, golec.GolectronServerPort, golec.ElectronServerPort)
	golec.elecCmd.Stderr = os.Stdout
	golec.elecCmd.Stdout = os.Stdout
	elecErr := golec.elecCmd.Start()
	if elecErr != nil {
		panic("ERROR starting Electron from Golectron:" + elecErr.Error())
	} else {
		go func(errCh chan string) {
			waitErr := golec.elecCmd.Wait()
			if waitErr != nil {
				errCh <- "ERROR in Electron from Golectron:" + waitErr.Error()
			} else {
				fmt.Println("Electron has exited with status 0")
				golec.QCh <- 1
			}
		}(golec.golecErrorCh)
	}
	var errChStr string
	select {
	case <-golec.golecReadyCh:
	case errChStr = <-golec.golecErrorCh:
		return nil, errors.New(errChStr)
	}
	// initialise Client to electron
	tr := &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.Dial("tcp", "localhost:"+golec.ElectronServerPort)
		},
	}
	golec.elecClient = &http.Client{
		Transport: tr,
	}
	time.Sleep(10e6) //give some time to slow Electron/NodeJS
	_, err = golec.elecClient.Get("http://localhost/test")
	if err != nil {
		fmt.Println("golectron.go GET error:", err)
	}
	// req, _ := http.NewRequest("GET", "/test", nil)
	golec.eventMap = make(map[string]func(interface{}))
	return golec, nil
}

// Close must be called to request termination of plugin: close, deactivation, crash
// Do not forget to call it before program close, so as to close subprocesses cleanly
func (g *Golectron) Close() {
	fmt.Println("Golectron is closing...")
	g.Quit()
	g.QCh <- 1
	// TODO: send electron 'close' signal
	g.elecCmd.Process.Kill()
	//once elecCmd status is exited, shut server down

}

/*
	Golectron specifics
*/

func (g *Golectron) allocatePaths() error {
	//Looking for golectron path in following order
	// 1. in binaryFolder/golectron
	// 2. in binaryFolder/../src/golectron
	// 3. in GOPATH/src/golectron
	binPath, _ := filepath.Abs(os.Args[0])
	binPath = filepath.Dir(binPath)
	// 1.
	if _, err := os.Stat(binPath + string(os.PathSeparator) + "golectron"); err != nil {
		fmt.Println("golectron is not in", binPath, ":", err)
	} else {
		g.pathGolectron = binPath + string(os.PathSeparator) + "golectron"
	}
	// 2.
	if g.pathGolectron == "" {
		fmt.Println("Looking into binaryFolder/../src/golectron")
		golecPath, _ := filepath.Abs(binPath + "/../src/golectron")
		fmt.Println("golecPath looks as follow:", golecPath)
		if _, err := os.Stat(golecPath); err != nil {
			fmt.Println("golectron is not in", golecPath, ":", err)
		} else {
			g.pathGolectron = golecPath
		}
	}
	// 3.
	if g.pathGolectron == "" {
		fmt.Println("Looking into GOPATH/src/golectron")
		gopath, isThere := os.LookupEnv("GOPATH")
		if !isThere {
			//look for a golectron folder in parent bin folder
			fmt.Println(os.Args[0])
			return errors.New("Golectron: golectron folder not found")
		}
		golecPath, _ := filepath.Abs(gopath + "/src/golectron")
		if _, err := os.Stat(golecPath); err != nil {
			return errors.New("Golectron: golectron folder not found")
		}
		g.pathGolectron = golecPath
	}
	//Looking for electron path in golectron folder or in PATH
	f, _ := os.Open(g.pathGolectron)
	dirs, _ := f.Readdir(-1)
	electronFolderName := ""
	r := regexp.MustCompile(`^electron`)
	for _, d := range dirs {
		if r.MatchString(d.Name()) {
			electronFolderName = g.pathGolectron + string(os.PathSeparator) + d.Name()
			fmt.Println("electron found in", electronFolderName)
			break
		}
	}
	searchElectronInPATH := false
	if electronFolderName != "" {
		elecFolder := filepath.Dir(electronFolderName)
		fmt.Println("elecFolder=", elecFolder)
		if _, err := os.Stat(electronFolderName + string(os.PathSeparator) + "electron"); err != nil {
			searchElectronInPATH = true
		} else {
			g.pathElectron = electronFolderName + string(os.PathSeparator) + "electron"
		}
	} else {
		searchElectronInPATH = true
	}
	if searchElectronInPATH {
		if eStr, err := exec.LookPath("electron"); err != nil {
			return errors.New("Golectron: electron bin not found, neither in golectron folder nor in PATH:" + err.Error())
		} else {
			g.pathElectron = eStr
		}
	}
	// Looking for golectronApp in golectron folder
	if _, err := os.Stat(g.pathGolectron + string(os.PathSeparator) + "golectronApp"); err != nil {
		return errors.New("Golectron: golectronApp can't be found in golectron folder")
	}
	g.pathGolectronApp = g.pathGolectron + string(os.PathSeparator) + "golectronApp"
	return nil
}

//Parse plugin 'config.json' file
func (g *Golectron) parseConfig() error {
	//Opening config.json file
	wd, _ := os.Getwd()
	fmt.Println(wd)
	f, err := os.Open(g.pathGolectron + string(os.PathSeparator) + "config.json")
	defer f.Close()
	if err != nil {
		return err
	}
	//Parsing config.json file into *g receiver
	dec := json.NewDecoder(f)
	err = dec.Decode(g)
	if err != nil {
		return err
	}
	return nil
}

/*---------------------------------------------------
	Golectron API
---------------------------------------------------*/

// GetWindowByID returns a *BrowserWindow given an id, and nil otherwise
func (g *Golectron) GetWindowByID(id int) *BrowserWindow {
	for _, v := range g.windows {
		if v.Id == id {
			return v
		}
	}
	return nil
}

// GetMenuByID returns a *Menu given an id, and nil otherwise
func (g *Golectron) GetMenuByID(id int) *Menu {
	for _, v := range g.menus {
		if v.Id == id {
			return v
		}
	}
	return nil
}

// RemoveMenuById removes the *Menu from the registry whose Menu.Id is id.
func (g *Golectron) RemoveMenuById(id int) {
	for i, v := range g.menus {
		if v.Id == id {
			menus := make([]*Menu, len(g.menus)-1)
			copy(menus, g.menus[:i])
			copy(menus, g.menus[i+1:])
			g.menus = menus
			break
		}
	}
}

// GetMenuItemByID returns a *MenuItem given an id, and nil otherwise
func (g *Golectron) GetMenuItemByID(id int) *MenuItem {
	for _, v := range g.menuitems {
		if v.MenuItemID == id {
			return v
		}
	}
	return nil
}

// RemoveMenuItemById removes the *MenuItem from the registry whose MenuItem.Id is id.
func (g *Golectron) RemoveMenuItemById(id int) {
	for i, v := range g.menuitems {
		if v.MenuItemID == id {
			mis := make([]*MenuItem, len(g.menuitems)-1)
			copy(mis, g.menuitems[:i])
			copy(mis, g.menuitems[i+1:])
			g.menuitems = mis
			break
		}
	}
}

// GetWebContentsByID returns a *WebContents given an id, and nil otherwise
func (g *Golectron) GetWebContentsByID(id int) *WebContents {
	for _, v := range g.webContents {
		if v.Id == id {
			return v
		}
	}
	return nil
}

// RemoveWebContentsByID removes the *WebContents from the registry whose WebContents.Id is id.
func (g *Golectron) RemoveWebContentsByID(id int) {
	for i, v := range g.webContents {
		if v.Id == id {
			wc := make([]*WebContents, len(g.webContents)-1)
			copy(wc, g.webContents[:i])
			copy(wc, g.webContents[i+1:])
			g.webContents = wc
			break
		}
	}
}

// GetSessionByID returns a *Session given an id, and nil otherwise
func (g *Golectron) GetSessionByID(id int) *Session {
	for _, v := range g.sessions {
		if v.Id == id {
			return v
		}
	}
	return nil
}

// RemoveSessionByID removes the *Session from the registry whose Session.Id is id.
func (g *Golectron) RemoveSessionByID(id int) {
	for i, v := range g.sessions {
		if v.Id == id {
			s := make([]*Session, len(g.sessions)-1)
			copy(s, g.sessions[:i])
			copy(s, g.sessions[i+1:])
			g.sessions = s
			break
		}
	}
}

/*---------------------------------------------------
Golectron Eventer interface
---------------------------------------------------*/

// Eventer is used to subscribe to event on objects on Electron JavaScript side.
type Eventer interface {
	// string is eventName as found in Electron API, and fn(interface{}) is function called when eventName triggers)
	On(eventName string, fn func(...interface{}))
	// string is the eventName to stop listening to
	RemoveAllListeners(eventName string)
	// Calling triggerevent should cause the receiver to look for the correct function in its eventmap and to call it.
	Emit(eventName string, data ...interface{})
	// EventList returns an array of event names currently registered
	EventList() []string
}

/*----------------------------------------------------------
electron.app API implementation in Golectron: Events
Check http://electron.atom.io/docs/api/app/ for more details
----------------------------------------------------------*/

// On registers the func 'fn' to be called when eventName signal is received. Many events are emitted with an 'event' object, passed as JSON in the 'event' property to the interface{} of the fn func.
/* Golectron has the following list of events:
will-finish-launching
ready returns launchInfo Object macOS
window-all-closed
before-quit returns event Event
will-quit returns event Event
quit returns event Event, exitCode Integer
open-file macOS returns event Event, path String
open-url macOS returns event Event, url String
activate macOS returns event Event, hasVisibleWindows Boolean
continue-activity macOS returns event Event,type String, userInfo Object
browser-window-blur returns event Event, window BrowserWindow
browser-window-focus returns event Event, window BrowserWindow
browser-window-created returns event Event, window BrowserWindow
web-contents-created returns event Event, webContents WebContents
certificate-error returns event Event, webContents WebContents, url URL, error String, certificate Object, callback Function
select-client-certificate returns event Event, webContents WebContents, url URL, certificateList [Objects], callback Function
login returns event Event, webContents WebContents, request Object, authInfo Object, callback Function
gpu-process-crashed
accessibility-support-changed macOS Windows returns event Event, accessibilitySupportEnabled Boolean
*/
func (g *Golectron) On(eventName string, fn func(interface{})) {
	req, _ := http.NewRequest("GET", "http://localhost/App/listen4event/"+eventName, nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Listen4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		fmt.Println("ERROR Golectron couldn't listen to event", eventName, ":", string(r))
	} else {
		g.eventMap[eventName] = fn
	}
}

// RemoveAllListeners unregisters/deletes the eventName from the event registry map. Future signals will thus be ignored.
func (g *Golectron) RemoveAllListeners(eventName string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/unlisten4event/"+eventName, nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Unlisten4Event function:", err)
	}
	if resp.StatusCode != 200 {
		r, _ := ioutil.ReadAll(resp.Body)
		data := struct {
			Error string `json:"error"`
		}{}
		err = json.Unmarshal(r, &data)
		if err != nil {
			fmt.Println("ERROR Golectron failed to Unmarshal Unlisten4Event error message:", err)
		}
		fmt.Println("ERROR Golectron couldn't Unlisten4Event", eventName, ":", data.Error)
	} else {
		delete(g.eventMap, eventName)
	}

}

// Emit calls the func associated with the eventName received.
func (g *Golectron) Emit(eventName string, data interface{}) {
	if val, ok := g.eventMap[eventName]; ok {
		val(data)
	} else {
		fmt.Println("ERROR: Received event in Golectron app is out of sync with Electron. EventName=", eventName)
	}
}

// EventList returns an array of event names currently registered.
func (g *Golectron) EventList() []string {
	//retrieve eventNames on Go side
	evList := make([]string, len(g.eventMap))
	for key := range g.eventMap {
		evList = append(evList, key)
	}
	return evList
}

/*---------------------------------------------------
electron.app API implementation in Golectron: Methods
---------------------------------------------------*/

// Quit tries to close all windows. The before-quit event will be emitted first. If all windows are successfully closed, the will-quit event will be emitted and by default the application will terminate.
// This method guarantees that all beforeunload and unload event handlers are correctly executed. It is possible that a window cancels the quitting by returning false in the beforeunload event handler.
func (g *Golectron) Quit() {
	req, _ := http.NewRequest("GET", "http://localhost/App/quit", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Quit function:", err)
		return
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Golectron quit response=", resp.Status, string(r))
}

// Exit (exitCode)
// exitCode Integer
// Exits immediately with exitCode. exitCode defaults to 0.
// All windows will be closed immediately without asking user and the before-quit and will-quit events will not be emitted.
func (g *Golectron) Exit(exitCode int) {
	data := struct {
		ExitCode int `json:"exitCode"`
	}{exitCode}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.Exit:", err)
	} else {
		fmt.Println("marshalling Golectron.Exit worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/App/exit", bytes.NewBuffer(jsData))
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Exit function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Golectron Exit response=", resp.Status, string(r))
}

// Relaunch Electron. Exit(0) Electron first, then relaunch one instance. Event maps are cleared. Any other Golectron objects generated previous to the call (Menu, BrowserWindow etc) will not behave properly any more.
// Returns true is relaunch succeed, false otherwise.
func (g *Golectron) Relaunch() (retVal bool) {
	defer func() {
		if r := recover(); r != nil {
			retVal = false
		}
	}()

	g.Exit(0)
	g.elecCmd.Process.Kill()
	//start a new electron into elecCmd
	g.golecReadyCh = make(chan int, 1)
	g.elecCmd = exec.Command(g.pathElectron, g.pathGolectronApp, g.GolectronServerPort, g.ElectronServerPort)
	g.elecCmd.Stderr = os.Stdout
	g.elecCmd.Stdout = os.Stdout
	elecErr := g.elecCmd.Start()
	if elecErr != nil {
		panic("ERROR starting Electron from Golectron:" + elecErr.Error())
	} else {
		go func() {
			waitErr := g.elecCmd.Wait()
			if waitErr != nil {
				panic("ERROR in Electron from Golectron:" + waitErr.Error())
			}
		}()
	}
	<-g.golecReadyCh
	// initialise Client to electron
	tr := &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.Dial("tcp", "localhost:"+g.ElectronServerPort)
		},
	}
	g.elecClient = &http.Client{
		Transport: tr,
	}
	_, err := g.elecClient.Get("http://localhost/test")
	if err != nil {
		fmt.Println("GET error:", err)
	}
	g.eventMap = make(map[string]func(interface{}))
	return true
}

// Focus ()
// On Linux, focuses on the first visible window. On macOS, makes the application the active app. On Windows, focuses on the application’s first window.
func (g *Golectron) Focus() {
	req, _ := http.NewRequest("GET", "http://localhost/App/focus", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Focus function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Golectron Focus response=", resp.Status, string(r))
}

// Hide () macOS
// Hides all application windows without minimizing them.
func (g *Golectron) Hide() {
	req, _ := http.NewRequest("GET", "http://localhost/App/hide", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call hide function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Golectron hide response=", resp.Status, string(r))
}

// Show () macOS
// Shows application windows after they were hidden. Does not automatically focus them.
func (g *Golectron) Show() {
	req, _ := http.NewRequest("GET", "http://localhost/App/show", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call Show function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("Golectron Show response=", resp.Status, string(r))
}

// GetAppPath Returns the current application directory.
func (g *Golectron) GetAppPath() (appPath string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/getAppPath", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetAppPath function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		AppPath string `json:"appPath"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetAppPath:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetAppPath response:", resp.StatusCode, string(r))
		return ""
	}
	return data.AppPath
}

// GetPath (name string)
// Retrieves a path to a special directory or file associated with name. On failure an Error is thrown.
/* You can request the following paths by the name:
home User’s home directory.
appData Per-user application data directory, which by default points to:
%APPDATA% on Windows
$XDG_CONFIG_HOME or ~/.config on Linux
~/Library/Application Support on macOS
userData The directory for storing your app’s configuration files, which by default it is the appData directory appended with your app’s name.
temp Temporary directory.
exe The current executable file.
module The libchromiumcontent library.
desktop The current user’s Desktop directory.
documents Directory for a user’s “My Documents”.
downloads Directory for a user’s downloads.
music Directory for a user’s music.
pictures Directory for a user’s pictures.
videos Directory for a user’s videos.
pepperFlashSystemPlugin Full path to the system version of the Pepper Flash plugin.*/
func (g *Golectron) GetPath(name string) (path string, err error) {
	data := struct {
		Name string `json:"name"`
		Path string `json:"path"`
	}{Name: name}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.GetPath:", err)
		return "", err
	}
	fmt.Println("marshalling Golectron.GetPath worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/getPath", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetPath function:", err)
		return "", err
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetPath:", err)
		return "", err
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetPath response:", resp.StatusCode, string(r))
		return "", err
	}
	return data.Path, nil
}

// SetPath (name, path string)
// Overrides the path to a special directory or file associated with name. If the path specifies a directory that does not exist, the directory will be created by this method. On failure an Error is thrown.
// You can only override paths of a name defined in app.getPath.
// By default, web pages’ cookies and caches will be stored under the userData directory. If you want to change this location, you have to override the userData path before the ready event of the app module is emitted.
func (g *Golectron) SetPath(name, path string) (err error) {
	data := struct {
		Name string `json:"name"`
		Path string `json:"path"`
	}{Name: name}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetPath:", err)
		return err
	}
	fmt.Println("marshalling Golectron.SetPath worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setPath", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetPath function:", err)
		return err
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetPath response:", resp.StatusCode, string(r))
		return err
	}
	return nil
}

// GetVersion Returns the version of the loaded application. If no version is found in the application’s package.json file, the version of the current bundle or executable is returned.
func (g *Golectron) GetVersion() (version string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/getVersion", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetVersion function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Version string `json:"version"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetVersion:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetVersion response:", resp.StatusCode, string(r))
		return ""
	}
	return data.Version
}

// GetName Returns the current application’s name, which is the name in the application’s package.json file.
// Usually the name field of package.json is a short lowercased name, according to the npm modules spec. You should usually also specify a productName field, which is your application’s full capitalized name, and which will be preferred over name by Electron.
func (g *Golectron) GetName() (appName string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/getName", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetName function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Name string `json:"name"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetName:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetName response:", resp.StatusCode, string(r))
		return ""
	}
	return data.Name
}

// SetName (name string) Overrides the current application’s name.
func (g *Golectron) SetName(name string) {
	data := struct {
		Name string `json:"name"`
	}{Name: name}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetName:", err)
	}
	fmt.Println("marshalling Golectron.SetName worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setName", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetName function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetName response:", resp.StatusCode, string(r))
	}
}

// GetLocale () Returns the current application locale. Possible return values are documented here.
// Note: When distributing your packaged app, you have to also ship the locales folder.
// Note: On Windows you have to call it after the ready events gets emitted.
func (g *Golectron) GetLocale() (locale string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/getLocale", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetLocale function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Locale string `json:"locale"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetLocale:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetLocale response:", resp.StatusCode, string(r))
		return ""
	}
	return data.Locale
}

// AddRecentDocument (path string) macOS Windows
// Adds path to the recent documents list.
// This list is managed by the OS. On Windows you can visit the list from the task bar, and on macOS you can visit it from dock menu.
func (g *Golectron) AddRecentDocument(path string) {
	data := struct {
		Path string `json:"path"`
	}{Path: path}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.AddRecentDocument:", err)
	} else {
		fmt.Println("marshalling Golectron.AddRecentDocument worked:", string(jsData))
	}
	req, _ := http.NewRequest("POST", "http://localhost/App/addRecentDocument", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call AddRecentDocument function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron AddRecentDocument response:", resp.StatusCode, string(r))
	}
}

// ClearRecentDocuments () macOS Windows
// Clears the recent documents list.
func (g *Golectron) ClearRecentDocuments() {
	req, _ := http.NewRequest("GET", "http://localhost/App/clearRecentDocuments", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call ClearRecentDocuments function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron ClearRecentDocuments response:", resp.StatusCode, string(r))
	}
}

// SetAsDefaultProtocolClient (protocol, path string, args []string) success bool ; macOS Windows
// protocol String - The name of your protocol, without ://. If you want your app to handle electron:// links, call this method with electron as the parameter.
// path String Windows - empty string defaults to process.execPath
// args Array Windows - Defaults to an empty array
// This method sets the current executable as the default handler for a protocol (aka URI scheme). It allows you to integrate your app deeper into the operating system. Once registered, all links with your-protocol:// will be opened with the current executable. The whole link, including protocol, will be passed to your application as a parameter.
// On Windows you can provide optional parameters path, the path to your executable, and args, an array of arguments to be passed to your executable when it launches.
// Returns true when the call succeeded, otherwise returns false.
// Note: On macOS, you can only register protocols that have been added to your app’s info.plist, which can not be modified at runtime. You can however change the file with a simple text editor or script during build time. Please refer to Apple’s documentation for details.
// The API uses the Windows Registry and LSSetDefaultHandlerForURLScheme internally.
func (g *Golectron) SetAsDefaultProtocolClient(protocol, path string, args []string) bool {
	data := struct {
		Protocol string   `json:"protocol"`
		Path     string   `json:"path"`
		Args     []string `json:"args"`
		Err      string   `json:"error"`
	}{Path: path}
	if path != "" {
		data.Path = path
	}
	if len(args) > 0 {
		data.Args = args
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetAsDefaultProtocolClient:", err)
		return false
	}
	fmt.Println("marshalling Golectron.SetAsDefaultProtocolClient worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setAsDefaultProtocolClient", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetAsDefaultProtocolClient function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal SetAsDefaultProtocolClient:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetAsDefaultProtocolClient response:", resp.StatusCode, string(r), data.Err)
		return false
	}
	return true
}

// RemoveAsDefaultProtocolClient (protocol, path string, args []string) success bool ; macOS Windows
// protocol String - The name of your protocol, without ://.
// path String (optional) Windows - Defaults to process.execPath
// args Array (optional) Windows - Defaults to an empty array
// This method checks if the current executable as the default handler for a protocol (aka URI scheme). If so, it will remove the app as the default handler.
// Returns true when the call succeeded, otherwise returns false.
func (g *Golectron) RemoveAsDefaultProtocolClient(protocol, path string, args []string) bool {
	data := struct {
		Protocol string   `json:"protocol"`
		Path     string   `json:"path"`
		Args     []string `json:"args"`
		Err      string   `json:"error"`
	}{Path: path}
	if path != "" {
		data.Path = path
	}
	if len(args) > 0 {
		data.Args = args
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.RemoveAsDefaultProtocolClient:", err)
		return false
	}
	fmt.Println("marshalling Golectron.RemoveAsDefaultProtocolClient worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/removeAsDefaultProtocolClient", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call RemoveAsDefaultProtocolClient function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal RemoveAsDefaultProtocolClient:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron RemoveAsDefaultProtocolClient response:", resp.StatusCode, string(r), data.Err)
		return false
	}
	return true
}

// IsDefaultProtocolClient (protocol[, path, args]) macOS Windows
// protocol String - The name of your protocol, without ://.
// path String (optional) Windows - Defaults to process.execPath
// args Array (optional) Windows - Defaults to an empty array
// This method checks if the current executable is the default handler for a protocol (aka URI scheme). If so, it will return true. Otherwise, it will return false.
// Note: On macOS, you can use this method to check if the app has been registered as the default protocol handler for a protocol. You can also verify this by checking ~/Library/Preferences/com.apple.LaunchServices.plist on the macOS machine. Please refer to Apple’s documentation for details.
// The API uses the Windows Registry and LSCopyDefaultHandlerForURLScheme internally.
func (g *Golectron) IsDefaultProtocolClient(protocol, path string, args []string) bool {
	data := struct {
		Protocol string   `json:"protocol"`
		Path     string   `json:"path"`
		Args     []string `json:"args"`
		Err      string   `json:"error"`
	}{Path: path}
	if path != "" {
		data.Path = path
	}
	if len(args) > 0 {
		data.Args = args
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.IsDefaultProtocolClient:", err)
		return false
	}
	fmt.Println("marshalling Golectron.IsDefaultProtocolClient worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/isDefaultProtocolClient", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call IsDefaultProtocolClient function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal IsDefaultProtocolClient:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron IsDefaultProtocolClient response:", resp.StatusCode, string(r), data.Err)
		return false
	}
	return true
}

// SetUserTasks (tasks []UserTask) success bool ; Windows
// Adds tasks to the Tasks category of the JumpList on Windows.
// Returns true when the call succeeded, otherwise returns false.
// Note: If you’d like to customize the Jump List even more use app.setJumpList(categories) instead.
func (g *Golectron) SetUserTasks(tasks []UserTask) bool {
	data := struct {
		Tasks []UserTask `json:"tasks"`
		Err   string     `json:"error"`
	}{Tasks: tasks}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetUserTasks:", err)
		return false
	}
	fmt.Println("marshalling Golectron.SetUserTasks worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setUserTasks", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetUserTasks function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal SetUserTasks:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetUserTasks response:", resp.StatusCode, string(r), data.Err)
		return false
	}
	return true
}

// UserTask Object:
// program String - Path of the program to execute, usually you should specify process.execPath which opens the current program.
// arguments String - The command line arguments when program is executed.
// title String - The string to be displayed in a JumpList.
// description String - Description of this task.
// iconPath String - The absolute path to an icon to be displayed in a JumpList, which can be an arbitrary resource file that contains an icon. You can usually specify process.execPath to show the icon of the program.
// iconIndex Integer - The icon index in the icon file. If an icon file consists of two or more icons, set this value to identify the icon. If an icon file consists of one icon, this value is 0.
type UserTask struct {
	Program     string `json:"program"`
	Arguments   string `json:"Arguments"`
	Title       string `json:"title"`
	Description string `json:"description"`
	IconPath    string `json:"iconPath"`
	IconIndex   int    `json:"iconIndex"`
}

// GetJumpListSettings () (minItems int, removedItems []JumpListItem); Windows
// Returns an Object with the following properties:
// minItems Integer - The minimum number of items that will be shown in the Jump List (for a more detailed description of this value see the MSDN docs).
// removedItems Array - Array of JumpListItem objects that correspond to items that the user has explicitly removed from custom categories in the Jump List. These items must not be re-added to the Jump List in the next call to app.setJumpList(), Windows will not display any custom category that contains any of the removed items.
func (g *Golectron) GetJumpListSettings() (int, []JumpListItem) {
	req, _ := http.NewRequest("GET", "http://localhost/App/getJumpListSettings", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetJumpListSettings function:", err)
		return 0, nil
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		MinItems     int            `json:"minItems"`
		RemovedItems []JumpListItem `json:"removedItems"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetJumpListSettings:", err)
		return 0, nil
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetJumpListSettings response:", resp.StatusCode, string(r))
		return 0, nil
	}
	return data.MinItems, data.RemovedItems
}

// JumpListItem objects should have the following properties:
// type String - One of the following:
// task - A task will launch an app with specific arguments.
// separator - Can be used to separate items in the standard Tasks category.
// file - A file link will open a file using the app that created the Jump List, for this to work the app must be registered as a handler for the file type (though it doesn’t have to be the default handler).
// path String - Path of the file to open, should only be set if type is file.
// program String - Path of the program to execute, usually you should specify process.execPath which opens the current program. Should only be set if type is task.
// args String - The command line arguments when program is executed. Should only be set if type is task.
// title String - The text to be displayed for the item in the Jump List. Should only be set if type is task.
// description String - Description of the task (displayed in a tooltip). Should only be set if type is task.
// iconPath String - The absolute path to an icon to be displayed in a Jump List, which can be an arbitrary resource file that contains an icon (e.g. .ico, .exe, .dll). You can usually specify process.execPath to show the program icon.
// iconIndex Integer - The index of the icon in the resource file. If a resource file contains multiple icons this value can be used to specify the zero-based index of the icon that should be displayed for this task. If a resource file contains only one icon, this property should be set to zero.
type JumpListItem struct {
	Type        string `json:"type"` // task, separator, file
	Path        string `json:"path"`
	Program     string `json:"program"`
	Args        string `json:"args"`
	Title       string `json:"title"`
	Description string `json:"description"`
	IconPath    string `json:"iconPath"`
	IconIndex   int    `json:"iconIndex"`
}

// SetJumpList (categories []JumpListCategory) string ; Windows
// categories Array or nil- Array of JumpListCategory objects.
// Sets or removes a custom Jump List for the application, and returns one of the following strings:
// ok - Nothing went wrong.
// error - One or more errors occured, enable runtime logging to figure out the likely cause.
// invalidSeparatorError - An attempt was made to add a separator to a custom category in the Jump List. Separators are only allowed in the standard Tasks category.
// fileTypeRegistrationError - An attempt was made to add a file link to the Jump List for a file type the app isn’t registered to handle.
// customCategoryAccessDeniedError - Custom categories can’t be added to the Jump List due to user privacy or group policy settings.
// If categories is null the previously set custom Jump List (if any) will be replaced by the standard Jump List for the app (managed by Windows).
func (g *Golectron) SetJumpList(categories []JumpListCategory) string {
	data := struct {
		Categories []JumpListCategory `json:"categories"`
		Err        string             `json:"error"`
		RetVal     string             `json:"retVal"`
	}{Categories: categories}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetJumpList:", err)
		return ""
	}
	fmt.Println("marshalling Golectron.SetJumpList worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setJumpList", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetJumpList function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal SetJumpList:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetJumpList response:", resp.StatusCode, string(r), data.Err)
		return data.Err
	}
	return data.RetVal
}

// JumpListCategory objects should have the following properties:
// type String - One of the following:
// tasks - Items in this category will be placed into the standard Tasks category. There can be only one such category, and it will always be displayed at the bottom of the Jump List.
// frequent - Displays a list of files frequently opened by the app, the name of the category and its items are set by Windows.
// recent - Displays a list of files recently opened by the app, the name of the category and its items are set by Windows. Items may be added to this category indirectly using app.addRecentDocument(path).
// custom - Displays tasks or file links, name must be set by the app.
// name String - Must be set if type is custom, otherwise it should be omitted.
// items Array - Array of JumpListItem objects if type is tasks or custom, otherwise it should be omitted.
// Note: If a JumpListCategory object has neither the type nor the name property set then its type is assumed to be tasks. If the name property is set but the type property is omitted then the type is assumed to be custom.
// Note: Users can remove items from custom categories, and Windows will not allow a removed item to be added back into a custom category until after the next successful call to app.setJumpList(categories). Any attempt to re-add a removed item to a custom category earlier than that will result in the entire custom category being omitted from the Jump List. The list of removed items can be obtained using app.getJumpListSettings().
type JumpListCategory struct {
	Type  string         `json:"type"` // task, frequent, recent, custom
	Name  string         `json:"name"`
	Items []JumpListItem `json:"items"`
}

// MakeSingleInstance (callback) isNotSingle bool
// callback Function
// This method makes your application a Single Instance Application - instead of allowing multiple instances of your app to run, this will ensure that only a single instance of your app is running, and other instances signal this instance and exit.
// callback will be called with callback(argv []string, workingDirectory string) when a second instance has been executed. argv is an Array of the second instance’s command line arguments, and workingDirectory is its current working directory. Usually applications respond to this by making their primary window focused and non-minimized.
// The callback is guaranteed to be executed after the ready event of app gets emitted.
// This method returns false if your process is the primary instance of the application and your app should continue loading. And returns true if your process has sent its parameters to another instance, and you should immediately quit.
// On macOS the system enforces single instance automatically when users try to open a second instance of your app in Finder, and the open-file and open-url events will be emitted for that. However when users start your app in command line the system’s single instance mechanism will be bypassed and you have to use this method to ensure single instance.
/*An example of activating the window of primary instance when a second instance starts:
const {app} = require('electron')
let myWindow = null
const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) => {
  // Someone tried to run a second instance, we should focus our window.
  if (myWindow) {
    if (myWindow.isMinimized()) myWindow.restore()
    myWindow.focus()
  }
})
if (shouldQuit) {
  app.quit()
}
// Create myWindow, load the rest of the app, etc...
app.on('ready', () => {
})*/
func (g *Golectron) MakeSingleInstance(fn func(interface{})) (isNotSingle bool) {
	g.eventMap["singleInstance"] = fn
	req, _ := http.NewRequest("GET", "http://localhost/App/makeSingleInstance", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call MakeSingleInstance function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsNotSingle bool   `json:"isNotSingle"`
		Err         string `json:"error"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal MakeSingleInstance:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron MakeSingleInstance response:", resp.StatusCode, string(r), data.Err)
		return false
	}
	return data.IsNotSingle
}

// ReleaseSingleInstance ()
// Releases all locks that were created by makeSingleInstance. This will allow multiple instances of the application to once again run side by side.
func (g *Golectron) ReleaseSingleInstance() {
	delete(g.eventMap, "singleInstance")
	req, _ := http.NewRequest("GET", "http://localhost/App/releaseSingleInstance", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call ReleaseSingleInstance function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron ReleaseSingleInstance response:", resp.StatusCode, string(r))
	}
}

// SetUserActivity (type, userInfo, webpageURL string) macOS
// type String - Uniquely identifies the activity. Maps to NSUserActivity.activityType.
// userInfo Object - App-specific state to store for use by another device.
// webpageURL String - The webpage to load in a browser if no suitable app is installed on the resuming device. The scheme must be http or https.
// Creates an NSUserActivity and sets it as the current activity. The activity is eligible for Handoff to another device afterward.
func (g *Golectron) SetUserActivity(t, userInfoJS, webpageURL string) {
	data := struct {
		Type    string `json:"type"`
		UInfoJS string `json:"userInfo"`
		WebPage string `json:"webpageURL"`
	}{Type: t, UInfoJS: userInfoJS, WebPage: webpageURL}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetUserActivity:", err)
	}
	fmt.Println("marshalling Golectron.SetUserActivity worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setUserActivity", bytes.NewBuffer(jsData))
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetUserActivity function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetUserActivity response:", resp.StatusCode, string(r))
	}
}

// GetCurrentActivityType () macOS
// Returns the type of the currently running activity.
func (g *Golectron) GetCurrentActivityType() string {
	req, _ := http.NewRequest("GET", "http://localhost/App/getCurrentActivityType", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetCurrentActivityType function:", err)
		return ""
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		CurrentActivity string `json:"currentActivity"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetCurrentActivityType:", err)
		return ""
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetCurrentActivityType response:", resp.StatusCode, string(r))
		return ""
	}
	return data.CurrentActivity
}

// SetAppUserModelId (id string) Windows
// Changes the Application User Model ID to id.
func (g *Golectron) SetAppUserModelId(id string) {
	data := struct {
		ID string `json:"id"`
	}{ID: id}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetAppUserModelId:", err)
	}
	fmt.Println("marshalling Golectron.SetAppUserModelId worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setAppUserModelId", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetAppUserModelId function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetAppUserModelId response:", resp.StatusCode, string(r))
	}
}

// ImportCertificate (certificate, password string) LINUX
// options Object
// 	certificate String - Path for the pkcs12 file.
// 	password String - Passphrase for the certificate.
// callback Function
// 	result Integer - Result of import.
// Imports the certificate in pkcs12 format into the platform certificate store. callback is called with the result of import operation, a value of 0 indicates success while any other value indicates failure according to chromium net_error_list.
func (g *Golectron) ImportCertificate(certificate, password string) int {
	data := struct {
		Certificate string `json:"certificate"`
		Password    string `json:"password"`
		Result      int    `json:"result"`
	}{Certificate: certificate, Password: password}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.ImportCertificate:", err)
	}
	fmt.Println("marshalling Golectron.ImportCertificate worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/importCertificate", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call ImportCertificate function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal ImportCertificate:", err)
		return -1
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron ImportCertificate response:", resp.StatusCode, string(r))
		return -1
	}
	return data.Result
}

// DisableHardwareAcceleration ()
// Disables hardware acceleration for current app.
// This method can only be called before app is ready.
func (g *Golectron) DisableHardwareAcceleration() {
	req, _ := http.NewRequest("GET", "http://localhost/App/disableHardwareAcceleration", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DisableHardwareAcceleration function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DisableHardwareAcceleration response:", resp.StatusCode, string(r))
	}
}

// SetBadgeCount (count int) (success bool) Linux macOS
// Sets the counter badge for current app. Setting the count to 0 will hide the badge. Returns true when the call succeeded, otherwise returns false.
// On macOS it shows on the dock icon. On Linux it only works for Unity launcher,
// Note: Unity launcher requires the exsistence of a .desktop file to work, for more information please read Desktop Environment Integration.
func (g *Golectron) SetBadgeCount(count int) bool {
	data := struct {
		Count   int  `json:"count"`
		Success bool `json:"success"`
	}{Count: count}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetBadgeCount:", err)
	}
	fmt.Println("marshalling Golectron.SetBadgeCount worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setBadgeCount", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetBadgeCount function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal SetBadgeCount:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetBadgeCount response:", resp.StatusCode, string(r))
		return false
	}
	return true
}

// GetBadgeCount () counter int Linux macOS
// Returns the current value displayed in the counter badge.
func (g *Golectron) GetBadgeCount() int {
	req, _ := http.NewRequest("GET", "http://localhost/App/getBadgeCount", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetBadgeCount function:", err)
		return -1
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Counter int `json:"counter"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetBadgeCount:", err)
		return -1
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetBadgeCount response:", resp.StatusCode, string(r))
		return -1
	}
	return data.Counter
}

// IsUnityRunning () bool Linux
// Returns whether current desktop environment is Unity launcher.
func (g *Golectron) IsUnityRunning() bool {
	req, _ := http.NewRequest("GET", "http://localhost/App/isUnityRunning", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call IsUnityRunning function:", err)
		return false
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsUnity bool `json:"isUnity"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal IsUnityRunning:", err)
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron IsUnityRunning response:", resp.StatusCode, string(r))
		return false
	}
	return data.IsUnity
}

// GetLoginItemSettings () macOS Windows
// Return a LoginItemSetting of the app.
func (g *Golectron) GetLoginItemSettings() LoginItemSetting {
	req, _ := http.NewRequest("GET", "http://localhost/App/getLoginItemSettings", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call GetLoginItemSettings function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		LoginItem LoginItemSetting `json:"loginItem"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal GetLoginItemSettings:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron GetLoginItemSettings response:", resp.StatusCode, string(r))
	}
	return data.LoginItem
}

// LoginItemSetting has the following fields:
// openAtLogin Boolean - true if the app is set to open at login.
// openAsHidden Boolean - true if the app is set to open as hidden at login. This setting is only supported on macOS.
// wasOpenedAtLogin Boolean - true if the app was opened at login automatically. This setting is only supported on macOS.
// wasOpenedAsHidden Boolean - true if the app was opened as a hidden login item. This indicates that the app should not open any windows at startup. This setting is only supported on macOS.
// restoreState Boolean - true if the app was opened as a login item that should restore the state from the previous session. This indicates that the app should restore the windows that were open the last time the app was closed. This setting is only supported on macOS.
type LoginItemSetting struct {
	OpenAtLogin       bool `json:"openAtLogin"`
	OpenAsHidden      bool `json:"openAsHidden"`
	WasOpenedAtLogin  bool `json:"wasOpenedAtLogin"`
	WasOpenedAsHidden bool `json:"wasOpenedAsHidden"`
	RestoreState      bool `json:"restoreState"`
}

// SetLoginItemSettings (settings LoginItemSetting) macOS Windows
// settings Object
// openAtLogin Boolean - true to open the app at login, false to remove the app as a login item. Defaults to false.
// openAsHidden Boolean - true to open the app as hidden. Defaults to false. The user can edit this setting from the System Preferences so app.getLoginItemStatus().wasOpenedAsHidden should be checked when the app is opened to know the current value. This setting is only supported on macOS.
// Set the app’s login item settings.
func (g *Golectron) SetLoginItemSettings(li LoginItemSetting) {
	data := struct {
		LoginItem LoginItemSetting `json:"loginItem"`
	}{LoginItem: li}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.SetLoginItemSettings:", err)
	}
	fmt.Println("marshalling Golectron.SetLoginItemSettings worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/setLoginItemSettings", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call SetLoginItemSettings function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron SetLoginItemSettings response:", resp.StatusCode, string(r))
	}
}

// IsAccessibilitySupportEnabled () accessEnabled bool macOS Windows
// Returns a Boolean, true if Chrome’s accessibility support is enabled, false otherwise. This API will return true if the use of assistive technologies, such as screen readers, has been detected. See https://www.chromium.org/developers/design-documents/accessibility for more details.
func (g *Golectron) IsAccessibilitySupportEnabled() bool {
	req, _ := http.NewRequest("GET", "http://localhost/App/isAccessibilitySupportEnabled", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call IsAccessibilitySupportEnabled function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		AccessEnabled bool `json:"accessEnabled"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal IsAccessibilitySupportEnabled:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron IsAccessibilitySupportEnabled response:", resp.StatusCode, string(r))
	}
	return data.AccessEnabled
}

// CmdLineAppendSwitch (swtch, value string)
// switch String - A command-line switch
// value String (optional) - A value for the given switch
// Append a switch (with optional value) to Chromium’s command line.
// Note: This will not affect process.argv, and is mainly used by developers to control some low-level Chromium behaviors.
func (g *Golectron) CmdLineAppendSwitch(swtch, value string) {
	data := struct {
		Switch string `json:"switch"`
		Value  string `json:"value"`
	}{Switch: swtch}
	if value != "" {
		data.Value = value
	}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.CmdLineAppendSwitch:", err)
	}
	fmt.Println("marshalling Golectron.CmdLineAppendSwitch worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/cmdLineAppendSwitch", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call CmdLineAppendSwitch function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron CmdLineAppendSwitch response:", resp.StatusCode, string(r))
	}
}

// CmdLineAppendArgument (value string)
// value String - The argument to append to the command line
// Append an argument to Chromium’s command line. The argument will be quoted correctly.
// Note: This will not affect process.argv.
func (g *Golectron) CmdLineAppendArgument(value string) {
	data := struct {
		Value string `json:"value"`
	}{Value: value}

	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.CmdLineAppendArgument:", err)
	}
	fmt.Println("marshalling Golectron.CmdLineAppendArgument worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/cmdLineAppendArgument", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call CmdLineAppendArgument function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron CmdLineAppendArgument response:", resp.StatusCode, string(r))
	}
}

// DockBounce (kind string) macOS
// kind String - Can be 'critical' or 'informational'.
// When critical is passed, the dock icon will bounce until either the application becomes active or the request is canceled.
// When informational is passed, the dock icon will bounce for one second. However, the request remains active until either the application becomes active or the request is canceled.
// Returns an ID representing the request or -1 in case of an error
func (g *Golectron) DockBounce(kind string) (id int) {
	if kind != "critical" && kind != "informational" {
		return -1
	}
	data := struct {
		Kind string `json:"type"`
		ID   int    `json:"id"`
	}{Kind: kind}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.DockBounce:", err)
		return -1
	}
	fmt.Println("marshalling Golectron.DockBounce worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/dockBounce", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockBounce function:", err)
		return -1
	}
	r, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal DockBounce:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockBounce response:", resp.StatusCode, string(r))
		return -1
	}
	return id
}

// DockCancelBounce (id int) macOS
// Cancel the bounce of id.
func (g *Golectron) DockCancelBounce(id int) {
	data := struct {
		ID int `json:"id"`
	}{ID: id}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.DockCancelBounce:", err)
	}
	fmt.Println("marshalling Golectron.DockCancelBounce worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/dockCancelBounce", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockCancelBounce function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockCancelBounce response:", resp.StatusCode, string(r))
	}
}

// DockDownloadFinished (filePath string) macOS
// Bounces the Downloads stack if the filePath is inside the Downloads folder.
func (g *Golectron) DockDownloadFinished(filePath string) {
	data := struct {
		FilePath string `json:"filePath"`
	}{FilePath: filePath}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.DockDownloadFinished:", err)
	}
	fmt.Println("marshalling Golectron.DockDownloadFinished worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/dockDownloadFinished", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockDownloadFinished function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockDownloadFinished response:", resp.StatusCode, string(r))
	}
}

// DockSetBadge (text string) macOS
// Sets the string to be displayed in the dock’s badging area.
func (g *Golectron) DockSetBadge(text string) {
	data := struct {
		Text string `json:"text"`
	}{Text: text}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.DockSetBadge:", err)
	}
	fmt.Println("marshalling Golectron.DockSetBadge worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/dockSetBadge", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockSetBadge function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockSetBadge response:", resp.StatusCode, string(r))
	}
}

// DockGetBadge () badge string macOS
// Returns the badge string of the dock.
func (g *Golectron) DockGetBadge() (text string) {
	req, _ := http.NewRequest("GET", "http://localhost/App/dockGetBadge", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockGetBadge function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		Text string `json:"text"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal DockGetBadge:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockGetBadge response:", resp.StatusCode, string(r))
	}
	return data.Text
}

// DockHide () macOS
// Hides the dock icon.
func (g *Golectron) DockHide() {
	req, _ := http.NewRequest("GET", "http://localhost/App/dockHide", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockHide function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockHide response:", resp.StatusCode, string(r))
	}
}

// DockShow () macOS
// Shows the dock icon.
func (g *Golectron) DockShow() {
	req, _ := http.NewRequest("GET", "http://localhost/App/dockShow", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockShow function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockShow response:", resp.StatusCode, string(r))
	}
}

// DockIsVisible () macOS
// Returns whether the dock icon is visible. The app.dock.show() call is asynchronous so this method might not return true immediately after that call.
func (g *Golectron) DockIsVisible() (isVisible bool) {
	req, _ := http.NewRequest("GET", "http://localhost/App/dockIsVisible", nil)
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockIsVisible function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	data := struct {
		IsVisible bool `json:"isVisible"`
	}{}
	err = json.Unmarshal(r, &data)
	if err != nil {
		fmt.Println("ERROR Golectron failed to Unmarshal DockIsVisible:", err)
	}
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockIsVisible response:", resp.StatusCode, string(r))
	}
	return data.IsVisible
}

// DockSetMenu (menu Menu) macOS
// Sets the application’s dock menu.
func (g *Golectron) DockSetMenu(menu Menu) {
	data := struct {
		MenuID int `json:"menuID"`
	}{MenuID: menu.Id}
	jsData, err := json.Marshal(data)
	if err != nil {
		fmt.Println("ERROR marshalling Golectron.DockSetMenu:", err)
	}
	fmt.Println("marshalling Golectron.DockSetMenu worked:", string(jsData))
	req, _ := http.NewRequest("POST", "http://localhost/App/dockSetMenu", bytes.NewBuffer(jsData))
	req.Header.Set("Content-Type", "application/json")
	resp, err := g.elecClient.Do(req)
	if err != nil {
		fmt.Println("ERROR Golectron failed to call DockSetMenu function:", err)
	}
	r, _ := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("ERROR in Golectron DockSetMenu response:", resp.StatusCode, string(r))
	}
}

/*
app.dock.setIcon(image) macOS
image NativeImage
Sets the image associated with this dock icon.
*/
